<?php
use Illuminate\Database\Eloquent\Model as Model;

class Constants extends Model {
	
	public function getEntryPayin() {
		$item = Constants::where('name', '=', 'entry_amount') -> first();
		return ($item) ? (int)$item -> value : 0;
	}
	
// 	public function getDirectReferralBonus() {
// 		$item = Constants::where('name', '=', 'direct_referral_bonus') -> first();
// 		return $item -> value;
// 	}

// 	public function getExitBonus() {
// 		$item = Constants::where('name', '=', 'exit_bonus') -> first();
// 		return $item -> value;
// 	}

// 	public function getMisc() {
// 		$item = Constants::where('name', '=', 'misc') -> first();
// 		return $item -> value;
// 	}

// 	public function getConceptualization() {
// 		$item = Constants::where('name', '=', 'conceptualization_fee') -> first();
// 		return $item -> value;
// 	}

// 	public function getPresShare() {
// 		$item = Constants::where('name', '=', 'pres_share') -> first();
// 		return $item -> value;
// 	}

// 	public function getVPresShare() {
// 		$item = Constants::where('name', '=', 'vpres_share') -> first();
// 		return $item -> value;
// 	}

// 	public function getCompanyShare() {
// 		$item = Constants::where('name', '=', 'company_share') -> first();
// 		return $item -> value;
// 	}

}
