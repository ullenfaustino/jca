<?php
class CodesTransaction extends Model {
	protected $table = "codes_transaction";

	public function getIssuedCodes($id) {
		$arrIssuedCodes = array();
		$CT =  CodesTransaction::join('codes_transaction_has_codes as cthc','cthc.codes_transaction_id', '=', 'codes_transaction.id')
	       ->leftJoin('codes as c', 'c.id', '=', 'cthc.codes_id')
	       ->leftJoin('users as u','u.id', '=', 'codes_transaction.issued_to_id')
	       ->where('codes_transaction.issued_to_id', '=', $id)
	       ->select(['*', 'c.created_at as created_at', 'c.user_id as owner_id'])
		   ->get();
	       
	    foreach ($CT as $key => $code) {
			$user = Users::where('id', '=', $code->owner_id)->first();
			if($user == null) {
				$code->owner_name = "";
				$code->owner_fName = "";
				$code->owner_lName = "";
			} else {
				$code->owner_name = $user->username;
				$code->owner_fName = $user->first_name;
				$code->owner_lName = $user->last_name;
			}
			array_push($arrIssuedCodes, $code);
		}
		return $arrIssuedCodes;
	}
	
	
	// $from = date( 'Y-m-d', strtotime(Input::get('from')));
	// $to = date( 'Y-m-d', strtotime(Input::get('to')));
	// $parties = Party::whereBetween('date', array($from, $to))->get();
}
