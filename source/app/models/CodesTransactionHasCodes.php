<?php
class CodesTransactionHasCodes extends Model {
	public $timestamps = false;


	public function userTransactions() {
		return CodesTransactionHasCodes::join('codes_transaction as ct','ct.id', '=', 'codes_transaction_id')
		    			->leftJoin('users as u','u.id','=','ct.generated_by_id')
		    			->leftJoin('users as us','us.id','=','ct.issued_to_id')
		    			->leftJoin('codes as c','c.id','=','codes_transaction_has_codes.codes_id')
		    			->groupBy('codes_transaction_has_codes.codes_transaction_id')
		    			->select([new raw("sum(case when c.is_used = 0 then 1 else 0 end) unused"),new raw("sum(case when c.is_used = 1 then 1 else 0 end) used"),'c.id','ct.id as id',new raw('count(*) as code_count'),'codes_transaction_has_codes.codes_transaction_id','ct.created_at', 
							'u.first_name as generated_by_firstname','u.username as generated_by_username', 'u.last_name as generated_by_lastname','us.first_name as issued_to_firstname',
 							'us.last_name as issued_to_lastname', 'ct.generated_by_id as gID', 'ct.id as ct_id','ct.account_type as account_type' ]);
	}
}


//select count(*) as code_count,`codes_transaction_has_codes`.`codes_transaction_id`,`ct`.`created_at`, 
//`u`.`first_name` as `generated_by_firstname`, `u`.`last_name` as `generated_by_lastname`,`us`.`first_name` as `issued_to_firstname`,
// `us`.`last_name` as `issued_to_lastname` 
// from `codes_transaction_has_codes` left join `codes_transaction` as `ct` on `ct`.`id` = `codes_transaction_id` 
//left join `users` as `u` on `u`.`id` = `ct`.`generated_by_id` left join `users` as `us` on `us`.`id` = `ct`.`issued_to_id` group by `ct`.`id`
