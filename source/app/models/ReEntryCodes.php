<?php

use Illuminate\Database\Eloquent\Model as Model;

class ReEntryCodes extends Model {

	public function getReEntryCodesPerUser($id) {
		return ReEntryCodes::join('codes as C','C.id','=','re_entry_codes.code_id')
				->leftJoin('users as u','u.id','=','C.user_id')
				->where('re_entry_codes.user_id', '=', $id);
	}

	public function getReEntryCodes() {
		return ReEntryCodes::join('codes as C','C.id','=','re_entry_codes.code_id')
				->leftJoin('users as u','u.id','=','C.user_id')
				->select(["*", "re_entry_codes.created_at as created_at"]);
	}
	
}
