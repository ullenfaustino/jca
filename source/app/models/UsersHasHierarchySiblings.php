<?php
use Illuminate\Database\Eloquent\Model as Model;

class UsersHasHierarchySiblings extends Model {
	public function user($id) {
    	return UsersHasHierarchySiblings::where('users_has_hierarchy_siblings.user_id', '=', $id)
    				->select('*');

    }

    public function siblings($id, $month = null) {
        $siblings = UsersHasHierarchySiblings::leftJoin('hierarchy_siblings as h', 'h.id', "=", "users_has_hierarchy_siblings.hierarchy_sibling_id")
                    ->where('users_has_hierarchy_siblings.user_id', '=', $id)
                    ->select(array('recruitee_id','position'));
        
        if (!is_null($month)) {
        	$siblings = $siblings -> whereRaw(sprintf("MONTH(h.created_at) = '%s' AND YEAR(h.created_at) = '%s'", $month, date("Y")));
		}
		return $siblings;
    }
    
    public function siblingsPaginated($id, $offset, $limit) {
        return UsersHasHierarchySiblings::leftJoin('hierarchy_siblings as h', 'h.id', "=", "users_has_hierarchy_siblings.hierarchy_sibling_id")
                    ->where('users_has_hierarchy_siblings.user_id', '=', $id)
                   	->offset($offset)
                   	->take($limit)
                    ->select(array('recruitee_id','position'));
    }
    
    public function hasLeft($id) {
    	$siblings = UsersHasHierarchySiblings::siblings($id)->get();
    	foreach ($siblings as $key => $sib) {
			if ($sib -> position === 0) {
				return true;
			}
		}
		return false;
    }
    
    public function hasRight($id) {
    	$siblings = UsersHasHierarchySiblings::siblings($id)->get();
    	foreach ($siblings as $key => $sib) {
			if ($sib -> position === 1) {
				return true;
			}
		}
		return false;
    }
    
    public function hasEntry($userid) {
    	$heirarchy = UsersHasHierarchySiblings::leftJoin('hierarchy_siblings as HS', 'HS.id','=','users_has_hierarchy_siblings.hierarchy_sibling_id')
						-> where('HS.recruitee_id','=',$userid)
						-> select(array('*'))
						-> get();
    	if (count($heirarchy) > 0) {
    		$has_entry = true;
    	} else {
    		$has_entry = false;
    	}
    	return $has_entry;
    }
}

// SELECT * FROM `users_has_hierarchy_siblings` 
// JOIN `hierarchy_siblings` as `h` ON `h`.`id` = `users_has_hierarchy_siblings`.`hierarchy_sibling_id`  
// WHERE `users_has_hierarchy_siblings`.`user_id` = 15
