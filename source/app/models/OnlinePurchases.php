<?php
class OnlinePurchases extends Model {
	public function getPurchases($id) {
		return OnlinePurchases::join('status','status.id', '=', 'online_purchases.status_id')
					->where('online_purchases.user_id', '=', $id)
					->select(["*",'online_purchases.id as id']);
	}

	public function getPendingPurchases() {
		return OnlinePurchases::join('users','users.id', '=', 'online_purchases.user_id')
					->join('status','status.id', '=', 'online_purchases.status_id')
					->where('online_purchases.status_id', '=', 0)
					->select(["*",'online_purchases.id as id']);
	}

	public function getActionPurchases() {
		return OnlinePurchases::join('users as us','us.id', '=', 'online_purchases.user_id')
					->join('status','status.id', '=', 'online_purchases.status_id')
					->leftJoin('users as u','u.id', '=', 'online_purchases.admin_id')
					->where('online_purchases.status_id', '=', 1)
					->select(["*",'online_purchases.id as id','u.username as admin_username', 'us.username as requestee_username', 'online_purchases.created_at as created_at']);
	}

	public function getCancelledPurchases() {
		return OnlinePurchases::join('users as us','us.id', '=', 'online_purchases.user_id')
					->join('status','status.id', '=', 'online_purchases.status_id')
					->leftJoin('users as u','u.id', '=', 'online_purchases.admin_id')
					->where('online_purchases.status_id', '=', 2)
					->select(["*",'online_purchases.id as id','u.username as admin_username', 'us.username as requestee_username']);
	}
}