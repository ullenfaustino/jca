<?php
use \PhalconRest\Models\ApiVersion;
class Version {

	public static function getApiVersion() {
		$version = ApiVersion::findFirst("is_deleted=0");
		return $version -> name;
	}

}
?>