<?php

/**
 * Sample group routing with user check in middleware
 */
Route::group('/admin', function() {
	if (!Sentry::check()) {
		if (Request::isAjax()) {
			Response::headers() -> set('Content-Type', 'application/json');
			Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
			App::stop();
		} else {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	} else {
		$user = Sentry::getUser();
		if (!$user -> hasAnyAccess(array('admin'))) {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	}
}, function() use ($app) {
	/** sample namespaced controller */
	Route::get('/', 'Admin\AdminController:index') -> name('admin');
	foreach (Module::getModules() as $module) {
		$module -> registerAdminRoute();
	}
});

Route::group('/cashier', function() {
	if (!Sentry::check()) {
		if (Request::isAjax()) {
			Response::headers() -> set('Content-Type', 'application/json');
			Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
			App::stop();
		} else {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	} else {
		$user = Sentry::getUser();
		if ($user -> hasAnyAccess(array('admin', 'member'))) {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	}
}, function() use ($app) {
	/** sample namespaced controller */
	Route::get('/', 'Cashier\CashierController:index') -> name('cashier');
	foreach (Module::getModules() as $module) {
		$module -> registerCashierRoute();
	}
});

Route::group('/member', function() {
	if (!Sentry::check()) {
		if (Request::isAjax()) {
			Response::headers() -> set('Content-Type', 'application/json');
			Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
			App::stop();
		} else {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	} else {
		$user = Sentry::getUser();
		if ($user -> hasAnyAccess(array('admin', 'cashier'))) {
			$redirect = Request::getResourceUri();
			Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
		}
	}
}, function() use ($app) {
	/** sample namespaced controller */
	Route::get('/', 'Member\MemberController:index') -> name('member');
	Route::get('/set/password/:id', 'Member\MemberController:setMemberPassword');
	Route::get('/profile', 'Member\MemberController:memberProfiles');

	Route::post('/set/new/password', 'Member\MemberController:setMemberNewPassword') -> name("set_new_password");
	
	Route::get('/activation', 'Member\MemberController:accountActivation');
	Route::get('/resend/activation_code', 'Member\MemberController:resendActivationCode') -> name('resend_activationcode');
	Route::post('/account/activate', 'Member\MemberController:activatAccount') -> name("activate_account");
	Route::post('/verify/mobile/activation_code', 'Member\MemberController:verifyMobileActivationCode') -> name("verify_mobile_activation");
	
	Route::post('/profile/update', 'Member\MemberController:updateMemberProfile') -> name("update_member_profile");
	
	foreach (Module::getModules() as $module) {
		$module -> registerMemberRoute();
	}
});

/** API METHOD **/
Route::get('/api/geneology/:id', 'APIController:getGeneology');
Route::get('/api/geneology/detail/user/:user_id', 'APIController:getUserDetail');
Route::get('/api/validate/dr/:username', 'APIController:validateDR');
Route::get('/api/validate/dr/:username/:member_id', 'APIController:validateDRMember');
Route::get('/reports/generated_codes/:transaction_id', 'APIController:listGeneratedCodes');
Route::get('/reports/list/members', 'APIController:listMembers');
Route::get('/reports/list/pending/payouts', 'APIController:pendingPayouts');
Route::get('/reports/list/approved/payouts/:status', 'APIController:approvedPayouts');
Route::get('/graph/payins', 'APIController:landingPayinStats');
Route::get('/print/receipt/pdf/:code_id/:receipt_no', 'APIController:printReceiptPDF' );
Route::get('/print/receipt/payout/pdf/:trans_no/:receipt_no', 'APIController:printReceiptPayoutPDF' );
Route::get('/print/receipt/pos/pdf/:control_number/:receipt_no', 'APIController:printReceiptPOS' );

/** GET METHOD **/
Route::get('/', 'UserController:landingPage');
Route::get('/useful', 'UserController:usefulPage');
Route::get('/login', 'UserController:login') -> name('login');
Route::get('/logout', 'UserController:logout') -> name('logout');
Route::get('/users/all', 'UserController:getallUsers');
Route::get('/registration', 'RegistrationController:pre_registration_view') -> name('registration');
Route::get('/activation/position/:user_id', 'UserController:validateChild');
Route::get('/registration/mobile/verification', 'RegistrationController:mobileVerification');
Route::get('/profile/view/:user_id', 'UserController:viewUserProfile');
// Route::get('/registration/confirmation/:username/:activation_code', 'RegistrationController:confirmation');

/** POST METHOD **/
Route::post('/login', 'UserController:doLogin') -> name('dologin');
Route::post('/login/guest', 'UserController:loginAsGuest') -> name('login_as_guest');
Route::post('/register', 'RegistrationController:init_pre_registration') -> name('init_registration');
Route::post('/pre-registration', 'RegistrationController:pre_registration') -> name('pre_registration');
Route::post('/change/masterpassword', 'Admin\AdminController:changeMasterPassword') -> name('change_master');
Route::post('/change/adminpassword', 'Admin\AdminController:changePassword') -> name('change_admin_password');
Route::post('/toggleregistration', 'Admin\AdminController:toggleRegistration') -> name('toggle_registration');
Route::post('/togglepayout', 'Admin\AdminController:togglePayout') -> name('toggle_payout');

Route::post('/registration/add/team', 'RegistrationController:addNewTeam') -> name('add_team');

Route::get('/api/available/gc/:user_id', 'APIController:availableGC');
