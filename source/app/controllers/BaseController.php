<?php

class BaseController
{

    protected $app;
    protected $data;

    public function __construct()
    {
        $this->app = Slim\Slim::getInstance();
        $this->data = array();

        /** default title */
        $this->data['title'] = '';

        /** meta tag and information */
        $this->data['meta'] = array();

        /** queued css files */
        $this->data['css'] = array(
            'internal'  => array(),
            'external'  => array()
        );

        /** queued js files */
        $this->data['js'] = array(
            'internal'  => array(),
            'external'  => array()
        );

        /** prepared message info */
        $this->data['message'] = array(
            'error'    => array(),
            'info'    => array(),
            'debug'    => array(),
        );

        /** global javascript var */
        $this->data['global'] = array();

        /** base dir for asset file */
        $this->data['avatar_Url']  = AVATAR_PATH;
        $this->data['baseUrl']  = $this->baseUrl();
        $this->data['assetUrl'] = $this->data['baseUrl'].'assets/';
        $this -> data['request_uri'] = $_SERVER['REQUEST_URI'];
        
        $this->data['avatarUrl']  = FLAG_PATH;
        $this->data['flags'] = $this->getFlags();

        $this->loadBaseCss();
        $this->loadBaseJs();
		
		// setup general settings
		$login_attribute = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
		if ($login_attribute) {
			$this -> data['login_attribute'] = json_decode($login_attribute -> content);
		}
		
		// products
		$products = UnilevelProducts::all();
		$this -> data['products'] = $products;
    }

    /**
     * enqueue css asset to be loaded
     * @param  [string] $css     [css file to be loaded relative to base_asset_dir]
     * @param  [array]  $options [location=internal|external, position=first|last|after:file|before:file]
     */
    protected function loadCss($css, $options=array())
    {
        $location = (isset($options['location'])) ? $options['location']:'internal';

        //after:file, before:file, first, last
        $position = (isset($options['position'])) ? $options['position']:'last';

        if(!in_array($css,$this->data['css'][$location])){
            if($position=='first' || $position=='last'){
                $key = $position;
                $file='';
            }else{
                list($key,$file) =  explode(':',$position);
            }

            switch($key){
                case 'first':
                    array_unshift($this->data['css'][$location],$css);
                break;

                case 'last':
                    $this->data['css'][$location][]=$css;
                break;

                case 'before':
                case 'after':
                    $varkey = array_keys($this->data['css'][$location],$file);
                    if($varkey){
                        $nextkey = ($key=='after') ? $varkey[0]+1 : $varkey[0];
                        array_splice($this->data['css'][$location],$nextkey,0,$css);
                    }else{
                        $this->data['css'][$location][]=$css;
                    }
                break;
            }
        }
    }


    /**
     * enqueue js asset to be loaded
     * @param  [string] $js      [js file to be loaded relative to base_asset_dir]
     * @param  [array]  $options [location=internal|external, position=first|last|after:file|before:file]
     */
    protected function loadJs($js, $options=array())
    {
        $location = (isset($options['location'])) ? $options['location']:'internal';

        //after:file, before:file, first, last
        $position = (isset($options['position'])) ? $options['position']:'last';

        if(!in_array($js,$this->data['js'][$location])){
            if($position=='first' || $position=='last'){
                $key = $position;
                $file='';
            }else{
                list($key,$file) =  explode(':',$position);
            }

            switch($key){
                case 'first':
                    array_unshift($this->data['js'][$location],$js);
                break;

                case 'last':
                    $this->data['js'][$location][]=$js;
                break;

                case 'before':
                case 'after':
                    $varkey = array_keys($this->data['js'][$location],$file);
                    if($varkey){
                        $nextkey = ($key=='after') ? $varkey[0]+1 : $varkey[0];
                        array_splice($this->data['js'][$location],$nextkey,0,$js);
                    }else{
                        $this->data['js'][$location][]=$js;
                    }
                break;
            }
        }
    }

    /**
     * clear enqueued css asset
     */
    protected function resetCss()
    {
        $this->data['css']         = array(
            'internal'  => array(),
            'external'  => array()
        );
    }

    /**
     * clear enqueued js asset
     */
    protected function resetJs()
    {
        $this->data['js']         = array(
            'internal'  => array(),
            'external'  => array()
        );
    }

    /**
     * remove individual css file from queue list
     * @param  [string] $css [css file to be removed]
     */
    protected function removeCss($css)
    {
        $key=array_keys($this->data['css']['internal'],$css);
        if($key){
            array_splice($this->data['css']['internal'],$key[0],1);
        }

        $key=array_keys($this->data['css']['external'],$css);
        if($key){
            array_splice($this->data['css']['external'],$key[0],1);
        }
    }

    /**
     * remove individual js file from queue list
     * @param  [string] $js [js file to be removed]
     */
    protected function removeJs($js)
    {
        $key=array_keys($this->data['js']['internal'],$js);
        if($key){
            array_splice($this->data['js']['internal'],$key[0],1);
        }

        $key=array_keys($this->data['js']['external'],$js);
        if($key){
            array_splice($this->data['js']['external'],$key[0],1);
        }
    }

    /**
     * addMessage to be viewd in the view file
     */
    protected function message($message, $type='info')
    {
        $this->data['message'][$type] = $message;
    }

    /**
     * register global variable to be accessed via javascript
     */
    protected function publish($key,$val)
    {
        $this->data['global'][$key] =  $val;
    }

    /**
     * remove published variable from registry
     */
    protected function unpublish($key)
    {
        unset($this->data['global'][$key]);
    }

    /**
     * add custom meta tags to the page
     */
    protected function meta($name, $content)
    {
        $this->data['meta'][$name] = $content;
    }

    /**
     * load base css for the template
     */
    protected function loadBaseCss()
    {
        $this->loadCss("vendor/fontawesome/css/font-awesome.css");
        $this->loadCss("vendor/metisMenu/dist/metisMenu.css");
        $this->loadCss("vendor/animate.css/animate.css");
        $this->loadCss("vendor/bootstrap/dist/css/bootstrap.css");
        $this->loadCss("fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css");
        $this->loadCss("fonts/pe-icon-7-stroke/css/helper.css");
        $this->loadCss("styles/style.css");
        $this->loadCss("custom.css");
        $this->loadCss("select2.css");
        $this->loadCss("jquery.auto-complete.css");
        $this->loadCss("bootstrap-timepicker.min.css");
        $this->loadCss("plugins/dataTables/dataTables.bootstrap.css");
        $this->loadCss("plugins/bootstrap-switch-master/css/bootstrap3/bootstrap-switch.min.css");
    }

    /**
     * load base js for the template
     */
    protected function loadBaseJs()
    {
        $this->loadJs("vendor/jquery/dist/jquery.min.js");
        $this->loadJs("vendor/jquery-ui/jquery-ui.min.js");
        $this->loadJs("vendor/slimScroll/jquery.slimscroll.min.js");
        $this->loadJs("vendor/bootstrap/dist/js/bootstrap.min.js");
        $this->loadJs("vendor/metisMenu/dist/metisMenu.min.js");
        $this->loadJs("vendor/iCheck/icheck.min.js");
        $this->loadJs("vendor/sparkline/index.js");
        $this->loadJs('jquery.auto-complete.min.js');
        $this->loadJs('bootstrap-timepicker.min.js');
        $this->loadJs("scripts/homer.js");
        $this->loadJs("vendor/jquery-validation/jquery.validate.min.js");
        $this->loadJs("vendor/select2-3.5.2/select2.min.js");
        $this->loadJs('plugins/input-mask/jquery.inputmask.js');
		$this->loadJs('plugins/input-mask/jquery.inputmask.date.extensions.js');
		$this->loadJs('plugins/input-mask/jquery.inputmask.extensions.js');
		$this->loadJs("notify.min.js");
		$this->loadJs("app/xpressload360helper.js");
		$this->loadJs("plugins/dataTables/jquery.dataTables.js");
		$this->loadJs("plugins/dataTables/dataTables.bootstrap.js");
		$this->loadJs("plugins/bootstrap-switch-master/js/bootstrap-switch.min.js");
		$this->loadJs("jquery.confirm.min.js");
    }

    /**
     * generate base URL
     */
    protected function baseUrl()
    {
        $path       = dirname($_SERVER['SCRIPT_NAME']);
        $path       = trim($path, '/');
        $baseUrl    = Request::getUrl();
        $baseUrl    = trim($baseUrl, '/');
        return $baseUrl.'/'.$path.( $path ? '/' : '' );
    }

    /**
     * generate siteUrl
     */
    protected function siteUrl($path, $includeIndex = false)
    {
        $path = trim($path, '/');
        return $this->data['baseUrl'].$path;
    }
    
    private function getFlags() {
        $flags = array();
        foreach (new DirectoryIterator(FLAG_PATH) as $fileInfo) {
            if ($fileInfo -> isDot() || $fileInfo -> getFilename() === "blank.png")
                continue;
            $file = $fileInfo -> getFilename();
            $fileInfo = pathinfo(sprintf("%s%s", FLAG_PATH, $file));
            array_push($flags, $fileInfo);
        }
        return $flags;
    }
}
