<?php

namespace Admin;

use \App;
use \View;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;

use \AdminControl;
use \Users;
use \MembersProfiles;
use \DirectReferrals;
use \MasterPassword;
use \Constants;
use \Codes;
use \MemberPayoutRequests;
use \raw;
use \CodesTransactionHasCodes;

class AdminController extends BaseController {
		
	/**
	 * display the admin dashboard
	 */
	
	public function index() {
		Response::Redirect($this -> siteUrl('admin/dashboard'));
	} 
	 
}
