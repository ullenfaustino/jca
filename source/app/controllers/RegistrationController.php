<?php

class RegistrationController extends BaseController {
			
	public function pre_registration_view() {
		$this -> data['title'] = 'Member Pre-Registration';

		$toggleRegistration = AdminControl::where('name', '=', 'member_registration') -> first();
		$this -> data['toggle_registration'] = $toggleRegistration -> value;

		$teams = Teams::all();
		$this -> data['teams'] = $teams;
		
		// setup general settings
        $login_attribute = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
        if ($login_attribute) {
            $this -> data['login_attribute'] = json_decode($login_attribute -> content);
        }
		
		View::display('user/registration.twig', $this -> data);
	}	
	
	public function pre_registration() {
		$username = Input::post('username');
		$email = Input::post('email');
		$password = Input::post('password');
		$confirm_password = Input::post('confirm_password');
		$firstname = Input::post('first_name');
		$middlename = Input::post('middle_name');
		$lastname = Input::post('last_name');
		$mobile_number = Input::post('mobile_number');
		$country_code = Input::post("country_code");
		
		$firstIndex = substr($mobile_number, 0, 1);
		if ($firstIndex === "0") {
			$mobile_number = substr_replace($mobile_number, "", 0, 1);
		}
		
		if ($password !== $confirm_password) {
			App::flash('message_status', false);
			App::flash('message', "Password Does not matched.");
			Response::redirect($this -> siteUrl("/registration"));
			return;
		}
		
		$codeGen = new CodeGenerator();
		$ban = $this -> countBAN();
		$activation_code = $codeGen -> getToken(5);
		try {
			$credential = ['ban' => $ban, 
							'username' => trim($username), 
							'email' => trim($email), 
							'password' => $password, 
							'canonical_hash' => base64_encode($password), 
							'first_name' => trim($firstname), 
							'middle_name' => trim($middlename), 
							'last_name' => trim($lastname), 
							'mobile_number' => $mobile_number, 
							'country_code' => $country_code,
							'activated' => 1,
							'user_type' => 3, 
							'is_mobile_verified' => 0,
							'is_password_changed' => 1, 
							'permissions' => array('member' => 1)];

			$user = Sentry::register($credential, false);
			$user -> addGroup(Sentry::getGroupProvider() -> findByName('Member'));
			
			$user -> activation_code = $activation_code;
			$user -> save();
			
			// Sentry::login($user, false);
			
			App::flash('message_status', true);
			App::flash('message', "Account Successfully Created.");
			
			$this -> sendSMSVerificationCode($user, $activation_code);
			
	 		// $activationCode = $user -> getActivationCode();
            GenericHelper::sendAccountVerification($user, $activation_code, $password);
            Response::redirect($this -> siteUrl("/registration/mobile/verification"));
            return;
		} catch (\Exception $e ) {
			$msg = "[Error Message] " . $e -> getMessage();
			$msg .= "\n[Message] Please Contact your System Administrator.";
			
			App::flash('message_status', false);
			App::flash('message', $msg);
		}
		Response::redirect($this -> siteUrl("/login"));
	}
	
	private function sendSMSVerificationCode($user, $activation_code) {
		$mobile = sprintf("%s%s", $user -> country_code, $user -> mobile_number);
		echo "[SENDTO]|--> $mobile\n";
        $message = "[JCA International Corporation]\n";
        $message .= sprintf("Your account verification code is : %s\n", $activation_code);
        echo $message;
        // return GenericHelper::sendSMS($mobile, $message);
        
        $sms_cmd = sprintf("php %s/sms_notification.php %s %s &", WORKERS_PATH, $mobile, base64_encode($message));
        pclose(popen($sms_cmd, "w"));
	}
	
	private function countBAN() {
        $codeGen = new CodeGenerator();
        $uuid = $codeGen -> getToken(12);
        $refid = Users::where('ban', '=', $uuid) -> first();
        if (!$refid) {
            return $uuid;
        } else {
            return $this -> countBAN();
        }
    }
    
    public function mobileVerification() {
    	$this -> data['title'] = 'Mobile Verification';
		View::display('user/mobile_verification.twig', $this -> data);
    }
    
	public function confirmation($username, $activation_code) {
        $this -> data['title'] = 'JCA - Account Details';
        $user = Users::find($username);
        if ($user) {
            $this -> data['user'] = $user;

            try {
                // Find the user using the user id
                $user = Sentry::findUserById($user -> id);

                // Check if the user is activated or not
                if ($user -> isActivated()) {
                    Response::redirect($this -> siteUrl('/login'));
                    return;
                } else {
                    // Attempt to activate the user
                    if ($user -> attemptActivation($activation_code)) {
                        Sentry::login($user, false);
                    } else {
                        App::flash('message_status', false);
                        App::flash('message', 'Failed to activate User account.');
                    }
                    Response::redirect($this -> siteUrl('/login'));
                }
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                App::flash('message_status', false);
                App::flash('message', 'User was not found.');
            }
        } else {
            App::flash('message_status', false);
            App::flash('message', 'Username not Found.');
        }
        Response::redirect($this -> siteUrl('/login'));
    }

	public function addNewTeam() {
		$teamName = Input::post("team_name");

		$team = new Teams();
		$team -> name = $teamName;
		$team -> save();
		
		App::flash('message', "New Team Added");
		Response::redirect($this -> siteUrl("/registration"));
	}
	
	public function testMail() {
		GenericHelper:sendMail("ullen.d.faustino@gmail.com", "Test mail", "Testing content");
	}

}
