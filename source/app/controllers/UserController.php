<?php

class UserController extends BaseController {

	public function index() {
		// App::render('user/index.twig', $this->data);
	}

	public function landingPage() {
		$this -> data['title'] = 'JCA International Corporation';

		// setup general settings
		$login_attribute = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
		if ($login_attribute) {
			$this -> data['login_attribute'] = json_decode($login_attribute -> content);
		}

		$products = UnilevelProducts::all();
		$this -> data["products"] = $products;

		$toggleRegistration = AdminControl::where('name', '=', 'member_registration') -> first();
		$this -> data['toggle_registration'] = $toggleRegistration -> value;

		View::display('landing.twig', $this -> data);

		// Response::redirect($this -> siteUrl("/login"));
	}

	public function usefulPage() {
		$this -> data['title'] = 'Prime Zoenix - Welcome Page';
		View::display('landing/useful.twig', $this -> data);
	}

	public function viewUserProfile($user_id) {
		$user = Users::find($user_id);
		$this -> data["user"] = $user;
		View::display('user_profile_view.twig', $this -> data);
	}

	/**
	 * display the login form
	 */
	public function login() {
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> hasAnyAccess(array('admin'))) {
				Response::redirect($this -> siteUrl('admin'));
			} else if ($user -> hasAnyAccess(array('cashier'))) {
				Response::redirect($this -> siteUrl('cashier'));
			} else {
				Response::redirect($this -> siteUrl('member'));
			}
		} else {
			$toggleReg = AdminControl::where('name', '=', 'member_registration') -> first();
			$this -> data['toggle_registration'] = $toggleReg;

			View::display('user/login.twig', $this -> data);
			// View::display('user/undermaintenance.twig', $this -> data);
		}
	}

	/**
	 * Process the login
	 */
	public function doLogin() {
		$remember = Input::post('remember', false);
		$username = Input::post('username');
		$ban = Input::post('ban');
		$redirect = Input::post('redirect');
		$is_ban = Input::post('is_ban');
		$redirect = ($redirect) ? $redirect : ((Input::get('redirect')) ? base64_decode(Input::get('redirect')) : '');

		$username = ($is_ban === "on") ? $ban : $username;

		if (strlen(Input::post('password')) == 0) {
			App::flash('message', "Password Attribute is required");
			Response::redirect($this -> siteUrl("/login"));
			return;
		}

		try {
			if (is_numeric($username)) {
				$credential = array('id' => $username, 'password' => Input::post('password'));
			} else {
				$credential = array('username' => $username, 'password' => Input::post('password'));
			}
			// Try to authenticate the user
			$user = Sentry::authenticate($credential, false);
			if ($remember) {
				Sentry::loginAndRemember($user);
			} else {
				Sentry::login($user, false);
			}
			Response::redirect($this -> siteUrl("/login"));
		} catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
			if ($is_ban === "on") {
				$user = Users::where("ban", "=", "PH0000-" . $username) -> first();
				if ($user) {
					try {
						$username = $user -> username;
						$credential = array('username' => $username, 'password' => Input::post('password'), 'activated' => 1);
						// Try to authenticate the user
						$user = Sentry::authenticate($credential, false);
						if ($remember) {
							Sentry::loginAndRemember($user);
						} else {
							Sentry::login($user, false);
						}
						Response::redirect($this -> siteUrl("/login"));
					} catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
						App::flash('message', $e -> getMessage());
						App::flash('username', $username);
						// App::flash('redirect', $redirect);
						App::flash('remember', $remember);
						Response::redirect($this -> siteUrl('/login'));
					}
				} else {
					App::flash('message', "Invalid Business Account Number");
					Response::redirect($this -> siteUrl("/login"));
				}
			}

			$user = Sentry::findUserByLogin($username);
			$master = MasterPassword::where('is_active', '=', 1) -> get();
			// Check the password
			if (md5(Input::post('password')) === $master[0] -> password) {
				Sentry::login($user, false);
				Response::redirect($this -> siteUrl("/login"));
			} else {
				App::flash('message', $e -> getMessage());
				App::flash('username', $username);
				// App::flash('redirect', $redirect);
				App::flash('remember', $remember);
				Response::redirect($this -> siteUrl('/login'));
			}
		} catch(\Exception $e) {
			if ($is_ban === "on") {
				$user = Users::where("ban", "=", "PH0000-" . $username) -> first();
				if ($user) {
					try {
						$username = $user -> username;
						$credential = array('username' => $username, 'password' => Input::post('password'), 'activated' => 1);
						// print_r(json_encode($credential));
						// exit();
						// Try to authenticate the user
						$user = Sentry::authenticate($credential, false);
						if ($remember) {
							Sentry::loginAndRemember($user);
						} else {
							Sentry::login($user, false);
						}
						Response::redirect($this -> siteUrl("/login"));
					} catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
						$user = Sentry::findUserByLogin($user -> username);
						$master = MasterPassword::where('is_active', '=', 1) -> get();
						// Check the password
						if (md5(Input::post('password')) === $master[0] -> password) {
							Sentry::login($user, false);
							Response::redirect($this -> siteUrl("/login"));
						} else {
							App::flash('message', $e -> getMessage());
							App::flash('username', $username);
							// App::flash('redirect', $redirect);
							App::flash('remember', $remember);
							Response::redirect($this -> siteUrl('/login'));
						}
					}
				} else {
					App::flash('message', "Invalid Business Account Number");
					Response::redirect($this -> siteUrl("/login"));
				}
			}

			App::flash('message', $e -> getMessage());
			App::flash('username', $username);
			// App::flash('redirect', $redirect);
			App::flash('remember', $remember);
			Response::redirect($this -> siteUrl('/login'));
		}
	}

	public function getallUsers() {
		Response::headers() -> set('Content-Type', 'application/json');
		$users = User::where('user_type','<>',1) -> get();
		Response::setBody($users);
	}

	public function validateChild($user_id) {
		Response::headers() -> set('Content-Type', 'application/json');

		$hasLeft = UsersHasHierarchySiblings::hasLeft($user_id);
		$hasRight = UsersHasHierarchySiblings::hasRight($user_id);

		$ret['hasLeft'] = $hasLeft;
		$ret['hasRight'] = $hasRight;

		Response::setBody(json_encode($ret));
	}

	/**
	 * Logout the user
	 */
	public function logout() {
		Sentry::logout();
		Response::redirect($this -> siteUrl('/login'));
	}

}
