<?php

namespace Member;

use \MembersProfiles;
use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;
use \Users;
use \User;
use \Codes;
use \BonusManager;
use \HierarchySiblings;
use \UsersHasHierarchySiblings;
use \CrosslineValidator;
use \GenericHelper;

class MemberController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		if (Sentry::check()) {
			$this -> data['user'] = $user;
		}
	}

	public function accountActivation() {
		Menu::get('member_sidebar') -> setActiveMenu('activation');

		$user = Sentry::getUser();
		$this -> data['title'] = "Activate Your Account";
		
		if ($user -> is_mobile_verified == 0) {
			Response::redirect($this -> siteUrl("/registration/mobile/verification"));
		}
		
		if ($user -> is_registered == 1) {
			Response::Redirect($this -> siteUrl("member"));
		}

		View::display('member/activation.twig', $this -> data);
	}

	public function activatAccount() {
		$user = Sentry::getUser();
		$activationCode = Input::post('activation_code');
		$directReferral = Input::post('direct_referral');
		$placementHead = Input::post('head_ref_code');
		$placementPosition = Input::post('position');

		$_code = Codes::where('generated_code', '=', $activationCode) -> first();
		if ($_code) {
			if ($_code -> is_used == 0) {
				try {
					$placementHead_id = Users::where("ban", "=", $placementHead) -> first();
					if (!$placementHead_id) {
						App::flash('message_status', false);
						App::flash('message', "Invalid Geneology Head!");
						Response::redirect($this -> siteUrl("member/activation"));
						return;
					}

					$directReferral_id = Users::where("username", "=", $directReferral) -> first();
					if (!$directReferral_id) {
						App::flash('message_status', false);
						App::flash('message', "Direct Referral is not valid, or not found in the system.");
						Response::redirect($this -> siteUrl("member/activation"));
						return;
					}

					$head = Sentry::findUserById($placementHead_id -> id);
					$DR = Sentry::findUserById($directReferral_id -> id);

					if (!CrosslineValidator::isCrosslinedRegistration($DR -> id, $head -> id)) {
						App::flash('message_status', false);
						App::flash('message', "Crosslining is not permitted!");
						Response::redirect($this -> siteUrl("member/activation"));
						return;
					}

					$hierarchy = new HierarchySiblings();
					$hierarchy -> recruitee_id = $user -> id;
					$hierarchy -> position = (strcasecmp($placementPosition, "Left") == 0) ? 0 : 1;
					if ($hierarchy -> save()) {
						$hasSiblings = new UsersHasHierarchySiblings();
						$hasSiblings -> user_id = $head -> id;
						$hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
						$hasSiblings -> save();

						$_code -> is_used = 1;
						$_code -> user_id = $user -> id;
						$_code -> save();

						$_user = User::find($user -> id);
						$_user -> is_registered = 1;
						$_user -> save();

						BonusManager::updateDirectReferralBalance($DR -> id, $user -> id);
						BonusManager::processLogicTransaction($user -> id);

						App::flash('message_status', true);
						App::flash('message', "Member Successfully Saved!");
						
						Response::redirect($this -> siteUrl("member"));
						return;
					} else {
						App::flash('message_status', false);
						App::flash('message', "Unable to save member, please try again..");
					}
				} catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
					App::flash('message_status', false);
					App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
				} catch ( \Exception $e ) {
					App::flash('message_status', false);
					App::flash('message', $e -> getMessage());
				}
			} else {
				App::flash('act_code', $activationCode);
				App::flash('message_status', false);
				App::flash('message', "Activation code is already in used!");
			}
		} else {
			App::flash('act_code', $activationCode);
			App::flash('message_status', false);
			App::flash('message', "Activation code is invalid!");
		}
		Response::redirect($this -> siteUrl("member/activation"));
	}
	
	public function resendActivationCode() {
		try {
			$user = Sentry::getUser();
			
			$mobile = sprintf("%s%s", $user -> country_code, $user -> mobile_number);
			echo "[SENDTO]|--> $mobile\n";
	        $message = "[JCA International Corporation]\n";
	        $message .= sprintf("Your account verification code is : %s\n", $user -> activation_code);
	        echo $message;
	        
	        $sms_cmd = sprintf("php %s/sms_notification.php %s %s &", WORKERS_PATH, $mobile, base64_encode($message));
        	pclose(popen($sms_cmd, "w"));
	        
	        // if (GenericHelper::sendSMS($mobile, $message)) {
	        	App::flash('message_status', true);
				App::flash('message', "Activation Code successfully sent.");
	        // } else {
	        	// App::flash('message_status', false);
				// App::flash('message', "Message Sending failed.");
	        // }
        } catch (\Exception $e ) {
			$msg = "[Error Message] " . $e -> getMessage();
			$msg .= "\n[Message] Please Contact your System Administrator.";
			
			App::flash('message_status', false);
			App::flash('message', $msg);
		}
		Response::redirect($this -> siteUrl("/registration/mobile/verification"));
	}
	
	public function verifyMobileActivationCode() {
		try {
			$user = Sentry::getUser();
			$activation_code = Input::post("activation_code");
			
			$validate_code = Users::where("activation_code", "=", $activation_code) -> first();
			if ($validate_code) {
				if ($user -> id == $validate_code -> id) {
					$validate_code -> activation_code = "";
					$validate_code -> is_mobile_verified = 1;
					$validate_code -> save();
					
					App::flash('message_status', true);
					App::flash('message', "Activation Code accepted.");
					Response::redirect($this -> siteUrl("/login"));
					return;
				}
			}
			App::flash('message_status', false);
			App::flash('message', "Invalid Activation Code");
        } catch (\Exception $e ) {
			$msg = "[Error Message] " . $e -> getMessage();
			$msg .= "\n[Message] Please Contact your System Administrator.";
			
			App::flash('message_status', false);
			App::flash('message', $msg);
		}
		Response::redirect($this -> siteUrl("/registration/mobile/verification"));
	}
	
	public function setMemberPassword($id) {
		$id = base64_decode($id);
		$user = Users::find($id);
		$this -> data['user'] = $user;
		$this -> data['title'] = 'Set Your New Password';
		
		if ($user -> is_password_changed == 1) {
			Response::Redirect($this -> siteUrl("member"));
		}
		
		View::display('member/setpassword.twig', $this -> data);
	}

	public function setMemberNewPassword() {
		$password = Input::post("password");
		$confirm = Input::post("confirm");
		$user_id = Input::post("user_id");

		if ($confirm !== $password) {
			App::flash('message_status', false);
			App::flash('message', "Password does not matched.");
			Response::redirect($this -> siteUrl("member/set/password/" . base64_encode($user_id)));
			return;
		}

		$user = Sentry::findUserById($user_id);
		if ($user) {
			$user -> password = $password;
			$user -> is_password_changed = 1;
			$user -> save();

			App::flash('message_status', true);
			App::flash('message', "Your new password has been successfully updated.");
			Response::Redirect($this -> siteUrl('member'));
		} else {
			App::flash('message_status', false);
			App::flash('message', "User Not Found.");
			Response::redirect($this -> siteUrl("member/set/password/" . base64_encode($user_id)));
		}
	}
	
	public function memberProfiles() {
		$user = Sentry::getUser();
		$this -> data['user'] = $user;
		View::display('member/user_profile.twig', $this -> data);
	}
	
	public function updateMemberProfile() {
		$user = Sentry::getUser();
		
		$avatar = Input::file("avatar");
		$first_name = Input::post("first_name");
		$middle_name = Input::post("middle_name");
		$last_name = Input::post("last_name");
		$gender = Input::post("gender");
		$mobile_number = Input::post('mobile_number');
		$country_code = Input::post("country_code");
		$address = Input::post("address");
		$birthday = Input::post("birthday");
		$birthday = strtr($birthday, '/', '-');
		
		$firstIndex = substr($mobile_number, 0, 1);
		if ($firstIndex === "0") {
			$mobile_number = substr_replace($mobile_number, "", 0, 1);
		}
		
		$avatar_name = null;
		$tempName = $avatar["tmp_name"];
		$filename = $avatar["name"];
		if (!empty($avatar)) {
			$allowed = ' ,png,jpg,JPG,PNG';
			$extension_allowed = explode(',', $allowed);
			$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
			if (!array_search($file_extension, $extension_allowed)) {
				App::flash('message', "Invalid Image File Type!" . $filename);
				App::flash('message_status', false);
				Response::redirect($this -> siteUrl('member/profile'));
				return;
			}

			if (!file_exists(AVATAR_PATH)) {
				mkdir(AVATAR_PATH, 0777, TRUE);
			}

			$type = pathinfo($filename, PATHINFO_EXTENSION);
			$img = AVATAR_PATH . '/' . $filename;

			if (move_uploaded_file($tempName, $img)) {
				$avatar_name = $filename;
			}
		}
		
		$user -> first_name = $first_name;
		$user -> middle_name = $middle_name;
		$user -> last_name = $last_name;
		$user -> gender = $gender;
		if (!is_null($avatar_name)) {
			$user -> img_file_name = $avatar_name;
		}
		$user -> address = $address;
		$user -> mobile_number = $mobile_number;
		$user -> country_code = $country_code;
		$user -> birthday = date("Y-m-d", strtotime($birthday));
		$user -> save();
		
		App::flash('message_status', true);
		App::flash('message', "Profile Successfully Updated.");
		Response::redirect($this -> siteUrl("member/profile"));
	}
}
