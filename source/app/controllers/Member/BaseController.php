<?php

namespace Member;

use \App;
use \Menu;
use \Module;
use \Sentry;
use \Response;
use \Request;

class BaseController extends \BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

		$adminMenu = Menu::create('member_sidebar');

		$user = Sentry::getUser();
		if ($user -> is_registered == 1) {
			foreach (Module::getModules () as $module) {
				$module -> registerMemberMenu();
			}
		} else {
			$dashboard = $adminMenu -> createItem('activation', array('label' => 'Activation', 'icon' => 'dashboard', 'url' => 'member/activation'));

			$adminMenu -> addItem('activation', $dashboard);
			$adminMenu -> setActiveMenu('activation');
		}

		$this -> verifyAccess();
	}
	
	private function verifyAccess() {
		$user = Sentry::getUser();
		$current_url = Request::getPath();
		if ($user -> is_mobile_verified == 1) {
			if ($user -> is_registered == 1) {
				if ($user -> is_password_changed == 0) {
					if ($current_url !== "/member/set/password/" . base64_encode($user -> id)) {
						Response::Redirect($this -> siteUrl('member/set/password/' . base64_encode($user -> id)));
					}
					return;
				}
				if ($current_url === "/member") {
                    Response::Redirect($this -> siteUrl('member/geneology'));
                }
			} else {
				if ($current_url !== "/member/activation") {
					Response::Redirect($this -> siteUrl('member/activation'));
				}
			}
		} else {
			if ($current_url !== "/registration/mobile/verification") {
				Response::redirect($this -> siteUrl("/registration/mobile/verification"));
			}
		}
	}

}
