<?php

namespace Cashier;

use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;

class CashierController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		if (Sentry::check()) {
			$this -> data['user'] = Sentry::getUser();
		}

		Response::Redirect($this -> siteUrl('cashier/check_generation'));
	}

}
