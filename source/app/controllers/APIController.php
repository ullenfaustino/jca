<?php

class APIController extends BaseController {
		
	public function availableGC($user_id) {
		Response::headers() -> set('Content-Type', 'application/json');
		Response::setBody(BonusManager::getCurrentTotalGC($user_id));
	}
	
	public function getGeneology($id) {
		Response::headers() -> set('Content-Type', 'application/json');

		$siblings = UsersHasHierarchySiblings::siblings($id) -> get() -> toArray();
		$data['parent'] = Sentry::findUserById($id);
		// load 1st gen child
		foreach ($siblings as $key => $child) {
			if ($child['position'] == 0) {
				$data['child_left'] = Sentry::findUserById($child['recruitee_id']);
				$siblings0 = UsersHasHierarchySiblings::siblings($child['recruitee_id']) -> get() -> toArray();

				// 2nd gen
				foreach ($siblings0 as $key => $child0) {
					if ($child0['position'] == 0) {
						$data['child_left00'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings000 = UsersHasHierarchySiblings::siblings($child0['recruitee_id']) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings000 as $key => $child00) {
							if ($child00['position'] == 0) {
								$data['child_left000'] = Sentry::findUserById($child00['recruitee_id']);
							} else if ($child00['position'] == 1) {
								$data['child_left001'] = Sentry::findUserById($child00['recruitee_id']);
							}
						}
					} else if ($child0['position'] == 1) {
						$data['child_left01'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings001 = UsersHasHierarchySiblings::siblings($child0['recruitee_id']) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings001 as $key => $child01) {
							if ($child01['position'] == 0) {
								$data['child_left010'] = Sentry::findUserById($child01['recruitee_id']);
							} else if ($child01['position'] == 1) {
								$data['child_left011'] = Sentry::findUserById($child01['recruitee_id']);
							}
						}
					}
				}
			} else if ($child['position'] == 1) {
				$data['child_right'] = Sentry::findUserById($child['recruitee_id']);
				$siblings0 = UsersHasHierarchySiblings::siblings($child['recruitee_id']) -> get() -> toArray();

				// 2nd gen
				foreach ($siblings0 as $key => $child0) {
					if ($child0['position'] == 0) {
						$data['child_right00'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings000 = UsersHasHierarchySiblings::siblings($child0['recruitee_id']) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings000 as $key => $child00) {
							if ($child00['position'] == 0) {
								$data['child_right000'] = Sentry::findUserById($child00['recruitee_id']);
							} else if ($child00['position'] == 1) {
								$data['child_right001'] = Sentry::findUserById($child00['recruitee_id']);
							}
						}
					} else if ($child0['position'] == 1) {
						$data['child_right01'] = Sentry::findUserById($child0['recruitee_id']);

						$siblings001 = UsersHasHierarchySiblings::siblings($child0['recruitee_id']) -> get() -> toArray();
						// 3rd gen
						foreach ($siblings001 as $key => $child01) {
							if ($child01['position'] == 0) {
								$data['child_right010'] = Sentry::findUserById($child01['recruitee_id']);
							} else if ($child01['position'] == 1) {
								$data['child_right011'] = Sentry::findUserById($child01['recruitee_id']);
							}
						}
					}
				}
			}
		}

		$hierarchy = BonusManager::getHierarchyCount($id);
		$data['heirarchy'] = $hierarchy;
		
		$currentDrBonus = BonusManager::getCurrentDRBonus($id);
		$data['current_DR_bonus'] = $currentDrBonus;
		
		$currentDrGCBonus = BonusManager::getCurrentDR_GC_Bonus($id);
		$data['current_DR_GC_bonus'] = $currentDrGCBonus;
		
		$currentPairBonus = BonusManager::getCurrentEarnings($id);
		$data['current_Pair_bonus'] = $currentPairBonus;
		
		$currentPairGCBonus = BonusManager::getCurrent_GC_Earnings($id);
		$data['current_Pair_GC_bonus'] = $currentPairGCBonus;
		
		$pairingCount = BonusManager::getTotalPairingCount($id);
		$data['total_pair_count'] = $pairingCount;
		
		$isPairingValid = BonusManager::isPairingValid($id);
		$data['is_pairing_valid'] = $isPairingValid;
		
		$data['pairing_stats'] = $this -> getIncome($id);
		
		$pv_points = BonusManager::getPV_Points($id);
		$data['pv_points'] = $pv_points;
		
		Response::setBody(json_encode($data));
	}
	
	private function getIncome($id) {
		$pairing_stats = GenericHelper::monthList();
		foreach ($pairing_stats as $key => $month) {
			$total_income = PairBonus::where("user_id", "=", $id) 
										-> where("is_paired", "=", 1)
										-> where("is_gc", "=", 0)
										-> where("payout_status", "=", 0)
										-> whereRaw(sprintf("DATE(`created_at`) = '%s'", date("Y-m-d")))
										-> sum('amount');
										
			$total_income_gc = PairBonus::where("user_id", "=", $id) 
										-> where("is_paired", "=", 1)
										-> where("is_gc", "=", 1)
										-> where("payout_status", "=", 0)
										-> whereRaw(sprintf("DATE(`created_at`) = '%s'", date("Y-m-d")))
										-> sum('amount');
										
			$total_dr = DirectReferrals::where("recruiter_id", "=", $id) 
										-> where("payout_status", "=", 0)
										-> whereRaw(sprintf("DATE(`created_at`) = '%s'", date("Y-m-d")))
										-> sum('amount');
										
			$total_dr_gc = DirectReferrals::where("recruiter_id", "=", $id) 
										-> where("payout_status", "=", 0)
										-> whereRaw(sprintf("DATE(`created_at`) = '%s'", date("Y-m-d"))) 
										-> sum('gc');
			
			$month -> total_income = $total_income;
			$month -> total_income_gc = $total_income_gc;
			$month -> total_dr = $total_dr;
			$month -> total_dr_gc = $total_dr_gc;
		}
		return $pairing_stats;
	}
	
	public function validateDR($username) {
		Response::headers() -> set('Content-Type', 'application/json');

		$user = Users::where("username", "=", $username) -> first();
		if ($user) {
			$response['status'] = "success";
			$response['user'] = $user;
		} else {
			$response['status'] = "error";
		}

		Response::setBody(json_encode($response));
	}
	
	public function validateDRMember($username, $parent_id) {
		Response::headers() -> set('Content-Type', 'application/json');

		$user = Users::where("username", "=", $username) -> first();
		if ($user) {
			$heirarchy = BonusManager::getHierarchyMembers($parent_id);
			$leftMembers = $heirarchy['LEFT']['members'];
			$rightMembers = $heirarchy['RIGHT']['members'];
			$is_member = false;
			if (in_array($user -> id, $leftMembers) || in_array($user -> id, $rightMembers)) {
				$is_member = true;
			}
			
			if ($is_member) {
				$response['status'] = "success";
				$response['user'] = $user;
			} else {
				$response['status'] = "error";
			}
		} else {
			$response['status'] = "error";
		}

		Response::setBody(json_encode($response));
	}
	
	public function getUserDetail($user_id) {
		Response::headers() -> set('Content-Type', 'application/json');

		$user = Users::find($user_id);
		$color = "geneology/silver-gene.png";
		if ($user) {
			$color = "geneology/black-gene.png";
		}
		$response['color'] = $color;

		$direct = DirectReferrals::getDirect($user_id) -> first();
		$response['dr'] = null;
		if ($direct) {
			$response['dr'] = $direct;
		}

		Response::setBody(json_encode($response));
	}
	
	public function listGeneratedCodes($transaction_id) {
		$this -> data['generated_codes'] = CodesTransactionHasCodes::leftJoin('codes_transaction as CT', 'CT.id', '=', 'codes_transaction_has_codes.codes_transaction_id') 
																-> leftJoin('codes as C', 'C.id', '=', 'codes_transaction_has_codes.codes_id') 
																-> leftJoin('users as User', 'User.id', '=', 'C.user_id') 
																-> where('codes_transaction_has_codes.codes_transaction_id', '=', $transaction_id) 
																-> select(['C.id as id', 
																			'C.generated_code as code', 
																			'User.username as user_username', 
																			'User.first_name', 
																			'User.middle_name',
																			'User.last_name',
																			'CT.account_type as account_type',
																			'C.is_used']) 
																-> get();
		
		View::display('reports/generated_codes.twig', $this -> data);
	}
	
	public function listMembers() {
        $this -> data['users'] = Users::where('id', '<>', 4) 
                                    -> where("user_type","=", 3)
                                    -> get();
        View::display('reports/member_list.twig', $this -> data);
    }
    
    public function pendingPayouts() {
        $payout_requests = MemberPayoutRequests::where("status", "=", 0) -> get();
        foreach ($payout_requests as $key => $payout_request) {
            $strRange = explode("|", $payout_request -> week_range);
            $from_Date = new \DateTime($strRange[0]);
            $to_Date = new \DateTime($strRange[1]);
            
            $payout_request -> dt_range = $from_Date -> format("F j, Y") . " to " . $to_Date -> format("F j, Y");
        }
        $this -> data["payout_requests"] = $payout_requests;
        View::display('reports/pending_payouts_list.twig', $this -> data);
    }
    
    public function approvedPayouts($status) {
        $approved_payout_requests = MemberPayoutRequests::where("status", "=", 1);
        
        if ($status === 'daily') {
            $approved_payout_requests = $approved_payout_requests 
                                        -> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", date('Y-m-d'), date('Y-m-d')));
        } elseif ($status === 'weekly') {
            $lastSunday = strtotime('last sunday');
            $startDay = date('Y-m-d', $lastSunday);
            $endDay = date('Y-m-d', strtotime('+6 days', $lastSunday));
            
            $approved_payout_requests = $approved_payout_requests 
                                        -> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
        } elseif ($status === 'monthly') {
            $StartDate = new \DateTime('now');
            $StartDate -> modify('first day of this month');
            $startDay = $StartDate -> format('Y-m-d');
    
            $EndDate = new \DateTime('now');
            $EndDate -> modify('last day of this month');
            $endDay = $EndDate -> format('Y-m-d');
            
            $approved_payout_requests = $approved_payout_requests 
                                        -> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
        }
        
        $approved_payout_requests = $approved_payout_requests -> get();
        foreach ($approved_payout_requests as $key => $approved_payout_request) {
            $strRange = explode("|", $approved_payout_request -> week_range);
            $from_Date = new \DateTime($strRange[0]);
            $to_Date = new \DateTime($strRange[1]);
            
            $approved_payout_request -> dt_range = $from_Date -> format("F j, Y") . " to " . $to_Date -> format("F j, Y");
        }
        $this -> data["approved_payout_requests"] = $approved_payout_requests;
        View::display('reports/approved_payouts_list.twig', $this -> data);
    }
	
	public function landingPayinStats() {
		$payin = Constants::where('name', '=', 'entry_amount') -> first();
		$payin_amount = 0;
		if ($payin) {
			$payin_amount = $payin -> value;
		}
		
		$months = GenericHelper::monthList();
		foreach ($months as $key => $month) {
			// total payins
			$payins = Codes::where("is_used","=",1)
									-> whereRaw(sprintf("MONTH(updated_at) = '%s' and YEAR(updated_at)", $month -> month, date("Y")))
									-> count();
			$month -> total_payins = $payins;
		}
		Response::setBody(json_encode($months));
	}
	
	public function printReceiptPDF($code_id, $receipt_no) {
        $codeGen = new CodeGenerator();
        
        $user = Sentry::getUser();
        $this -> data['title'] = 'Generate Codes';
        $this -> data['user'] = $user;
        
        $code = Codes::find($code_id);
        
        $payin_amount = Constants::where("name","=","entry_amount")->first();
        $this -> data['code_amount'] = $payin_amount -> value;
        
        $receipt = Receipt::where("or_num","=",$receipt_no) 
                        -> where("module_type","=","payin")
                        -> first();
        if (!$receipt) {
            $receipt = new Receipt();
            $receipt -> trans_code = $codeGen -> generateControlNumber(4);
            $receipt -> or_num = $receipt_no;
            $receipt -> ref_id = $code -> id;
            $receipt -> module_type = "payin";
            $receipt -> total_amount = $payin_amount -> value;
            $receipt -> created_by = $user -> id;
            $receipt -> issued_to = $code -> user_id;
            $receipt -> is_issued = 1;
            $receipt -> save();
        }
        
        // receipt data content
        $receipt_data["date_today"] = date("F j, Y");
        $receipt_data["customer_id"] = $code -> user_id;
        $receipt_data["receipt_no"] = $receipt -> or_num;
        $receipt_data["reference_code"] = $receipt -> trans_code;
        $receipt_data["sub_total"] = 0;
        $receipt_data["total"] = $payin_amount -> value;
        $receipt_data["amount_paid"] = $payin_amount -> value;
        $receipt_data["particulars"][] = array("item" => "Activation Code",
                                                "description" => $code -> generated_code,
                                                "unit_cost" => $payin_amount -> value,
                                                "quantity" => 1,
                                                "amount" => $payin_amount -> value);
        $this -> data['receipt_data'] = $receipt_data;
        
        /** render the template */
        View::display('reports/receipt_pdf.twig', $this -> data);
    }
    
    public function printReceiptPayoutPDF($trans_no, $receipt_no) {
        $transaction = MemberPayoutRequests::where("transaction_number", "=", $trans_no) -> first();
        
        $codeGen = new CodeGenerator();
        
        $user = Sentry::getUser();
        $this -> data['title'] = 'Print Receipt';
        $this -> data['user'] = $user;
        
        $receipt = Receipt::where("or_num","=",$receipt_no) 
                        -> where("module_type","=","payout")
                        -> first();
        if (!$receipt) {
            $receipt = new Receipt();
            $receipt -> trans_code = $codeGen -> generateControlNumber(4);
            $receipt -> or_num = $receipt_no;
            $receipt -> ref_id = $transaction -> id;
            $receipt -> module_type = "payout";
            $receipt -> total_amount = $transaction -> amount_encash;
            $receipt -> created_by = $user -> id;
            $receipt -> issued_to = $transaction -> user_id;
            $receipt -> is_issued = 1;
            $receipt -> save();
        }
        
        // receipt data content
        $receipt_data["date_today"] = date("F j, Y");
        $receipt_data["customer_id"] = $transaction -> user_id;
        $receipt_data["receipt_no"] = $receipt -> or_num;
        $receipt_data["reference_code"] = $receipt -> trans_code;
        $receipt_data["sub_total"] = 0;
        $receipt_data["total"] = $transaction -> amount_encash;
        $receipt_data["amount_paid"] = $transaction -> amount_encash;
        $receipt_data["particulars"][] = array("item" => "Payout",
                                                "description" => "Bonus Encashment",
                                                "unit_cost" => $transaction -> amount_encash,
                                                "quantity" => 1,
                                                "amount" => $transaction -> amount_encash);
        $this -> data['receipt_data'] = $receipt_data;
        
        /** render the template */
        View::display('reports/receipt_pdf.twig', $this -> data);
    }
    
    public function printReceiptPOS($control_number, $receipt_no) {
        $transaction = UnilevelPurchaseLogs::where("control_number", "=", $control_number) -> first();
        
        $codeGen = new CodeGenerator();
        
        $user = Sentry::getUser();
        $this -> data['title'] = 'Print Receipt';
        $this -> data['user'] = $user;
        $this -> data['control_number'] = $control_number;
        
        $receipt = Receipt::where("or_num","=",$receipt_no) 
                        -> where("module_type","=","pos")
                        -> first();
        if (!$receipt) {
            $receipt = new Receipt();
            $receipt -> trans_code = $codeGen -> generateControlNumber(4);
            $receipt -> or_num = $receipt_no;
            $receipt -> ref_id = $transaction -> id;
            $receipt -> module_type = "pos";
            $receipt -> total_amount = $transaction -> total_amount;
            $receipt -> created_by = $user -> id;
            $receipt -> issued_to = $transaction -> member_id;
            $receipt -> is_issued = 1;
            $receipt -> save();
        }
        
        $product = UnilevelProducts::where("product_code", "=", $transaction -> product_code) -> first();
        $item_amount = ($transaction -> payment_type == 3) ? $product -> srp : $product -> price;
        
        // receipt data content
        $receipt_data["date_today"] = date("F j, Y");
        $receipt_data["customer_id"] = $transaction -> member_id;
        $receipt_data["receipt_no"] = $receipt -> or_num;
        $receipt_data["reference_code"] = $receipt -> trans_code;
        $receipt_data["sub_total"] = 0;
        $receipt_data["total"] = $transaction -> total_amount;
        $receipt_data["amount_paid"] = $transaction -> total_amount;
        $receipt_data["particulars"][] = array("item" => $product -> product_name,
                                                "description" => $control_number,
                                                "unit_cost" => $item_amount,
                                                "quantity" => $transaction -> quantity,
                                                "amount" => $transaction -> total_amount);
        $this -> data['receipt_data'] = $receipt_data;
        
        /** render the template */
        View::display('reports/receipt_pdf.twig', $this -> data);
    }
}
