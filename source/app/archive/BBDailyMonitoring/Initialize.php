<?php

namespace BBDailyMonitoring;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'BBDailyMonitoring';
    }

    public function getModuleAccessor(){
        return 'dailymonitoring';
    }
    
    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $member = $adminMenu->createItem('dailymonitoring', array(
            'label' => 'Daily Monitoring',
            'icon'  => 'bar-chart-o',
            'url'   => 'admin/monitoring'
        ));

        // $userGroup->addChildren($userMenu);
        // $userGroup->addChildren($groupMenu);
		$adminMenu->addItem('dailymonitoring', $member);
    }

    public function registerAdminRoute(){
        Route::resource('/monitoring', 'BBDailyMonitoring\Controllers\MonitoringController');
        Route::post('/accounts/encoded', 'BBDailyMonitoring\Controllers\EncodedAccountController:encodedAccounts')->name('encoded_accounts');
    }
}
