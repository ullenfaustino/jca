<?php

namespace BBDailymonitoring\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class MonitoringController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        Menu::get('admin_sidebar')->setActiveMenu('dailymonitoring');
    }

    /**
     * display list of resource
     */
    public function index($page = 1)
    {
        $user = Sentry::getUser();
        $this->data['title'] = 'Daily Monitoring';
        $this->data['users'] = User::groupByCreatedAt($user -> id)
                               ->get()
                               ->toArray();
       // var_dump($this->data['users']);
        $this -> data['user'] = $user;


        /** load the user.js app */
        $this->loadJs('app/user.js');

        /** publish necessary js  variable */
        $this->publish('baseUrl', $this->data['baseUrl']);

        /** render the template */
        View::display('@dailymonitoring/index.twig', $this->data);
    }
}