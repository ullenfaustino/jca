<?php

namespace BBDailymonitoring\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class EncodedAccountController extends BaseController {

	public function index() {
		Response::redirect($this -> siteUrl('admin/monitoring'));
	}

	public function encodedAccounts() {
		$date = Input::post('date');

		$user = Sentry::getUser();
		$this -> data['title'] = 'Daily Entries for ';
		$this -> data['_date'] = $date;
		$this -> data['user'] = $user;
		$this -> data['users'] = User::getUsersByDate($date);

		/** load the user.js app */
		$this -> loadJs('app/user.js');

		/** publish necessary js  variable */
		$this -> publish('baseUrl', $this -> data['baseUrl']);

		/** render the template */
		View::display('@dailymonitoring/encoded_account.twig', $this -> data);
	}

}
