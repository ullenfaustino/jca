<?php

namespace CashierRegistration;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'CashierRegistration';
	}
	
	public function getModuleAccessor() {
		return 'cashierregistration';
	}
	
	public function registerCashierMenu() {
		$cashierMenu = Menu::get ( 'cashier_sidebar' );
		
		$cashier = $cashierMenu->createItem ( 'cashier_registration', array (
				'label' => 'Registration',
				'icon' => 'database',
				'url' => 'cashier/registration' 
		));
		$cashier->setAttribute('class', 'nav nav-second-level');
		
		$registrationMenu = $cashierMenu->createItem('registration', array(
            'label' => 'Registration',
            'icon'  => 'user-plus',
            'url'   => 'cashier/registration'
        ));
		
		$preRegisteredMenu = $cashierMenu->createItem('pre-registered', array(
            'label' => 'Pre-Registered',
            'icon'  => 'users',
            'url'   => 'cashier/pre-registered'
        ));
        
        $RegisteredMenu = $cashierMenu->createItem('registered', array(
            'label' => 'Registered',
            'icon'  => 'users',
            'url'   => 'cashier/registered'
        ));
		
		$cashier->addChildren($registrationMenu);
		$cashier->addChildren($preRegisteredMenu);
		$cashier->addChildren($RegisteredMenu);
		
		$cashierMenu->addItem ( 'cashier_registration', $cashier);
	}
	
	public function registerCashierRoute() {
		Route::resource ( '/registration', 'CashierRegistration\Controllers\CashierRegistrationController' );
		
		Route::get('/pre-registered', 'CashierRegistration\Controllers\CashierRegistrationController:pre_registered_view' );
		Route::get('/registered', 'CashierRegistration\Controllers\CashierRegistrationController:registered_view' );
	}
}
