<?php

namespace CashierRegistration\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Cashier\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \AdminControl;
use \Users;

class CashierRegistrationController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('cashier_sidebar') -> setActiveMenu('cashier_registration');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Registration';
		$this -> data['user'] = $user;
		
		$toggleRegistration = AdminControl::where('name','=','member_registration') -> first();
		$this -> data['toggle_registration'] = $toggleRegistration -> value;
		
		/** render the template */
		View::display('@cashierregistration/index.twig', $this -> data);
	}

	public function pre_registered_view() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Pre-Registered List';
		$this -> data['user'] = $user;

		$this -> data['users'] = Users::where('user_type', '=', 3) -> where('username', '<>', 'HEAD') -> where('is_registered', '=', 0) -> get();

		/** render the template */
		View::display('@cashierregistration/pre_registered.twig', $this -> data);
	}
	
	public function registered_view() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Registered List';
		$this -> data['user'] = $user;

		$this -> data['users'] = Users::where('user_type', '=', 3) -> where('username', '<>', 'HEAD') -> where('is_registered', '=', 1) -> get();

		/** render the template */
		View::display('@cashierregistration/registered.twig', $this -> data);
	}
}
