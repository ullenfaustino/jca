<?php

namespace AdminBonusHistory;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'AdminBonusHistory';
	}
	
	public function getModuleAccessor() {
		return 'adminbonushistory';
	}
	
	public function registerAdminMenu() {
		$adminMenu = Menu::get ( 'admin_sidebar' );
		
		$member = $adminMenu->createItem ( 'bonushistory', array (
				'label' => 'Bonus History',
				'icon' => 'history',
				'url' => 'admin/bonushistory' 
		) );
		
		$adminMenu->addItem ( 'bonushistory', $member );
	}
	
	public function registerAdminRoute() {
		Route::resource ( '/bonushistory', 'AdminBonusHistory\Controllers\AdminBonusHistoryController' );
		
		Route::get ( '/bonushistory/pair/all/:week', 'AdminBonusHistory\Controllers\AdminBonusHistoryController:ViewPairHistory' );
		Route::get ( '/bonushistory/dr/all/:week', 'AdminBonusHistory\Controllers\AdminBonusHistoryController:ViewDrHistory' );
	}
}
