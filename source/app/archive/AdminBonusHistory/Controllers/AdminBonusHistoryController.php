<?php

namespace AdminBonusHistory\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \PairBonus;
use \DirectReferrals;
use \raw;

class AdminBonusHistoryController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('admin_sidebar') -> setActiveMenu('bonushistory');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Bonus History';
		$this -> data['user'] = $user;
		
		$pairGroups = $this -> getPairBonusGroups();
		$this -> data['pairGroups'] = $pairGroups;
		
		$drGroups = $this -> getDirectReferralBonusGroups();
		$this -> data['directReferrals'] = $drGroups;

		/** render the template */
		View::display('@adminbonushistory/index.twig', $this -> data);
	}
	
	private function getPairBonusGroups() {
		return PairBonus::select (array(
				new raw ( "count(user_id) AS totalCount" ),
				new raw ( " CONCAT(IF(created_at - INTERVAL 6 day < 0, 
                0, 
                IF(WEEKDAY(date(created_at) - INTERVAL 6 DAY) = 0, 
                    date(created_at) - INTERVAL 6 DAY, 
                    date(created_at) - INTERVAL ((WEEKDAY(date(created_at)) - 0)) DAY)),
            ' ~ ', date(DATE_ADD(created_at, INTERVAL (8 - IF(DAYOFWEEK(created_at)=1, 8, DAYOFWEEK(created_at))) DAY))) AS week" ),
				new raw ( " IF((WEEKDAY(date(created_at)) - 0) >= 0, 
                TO_DAYS(created_at) - (WEEKDAY(created_at) - 0), 
                TO_DAYS(created_at) - (7 - (0 - WEEKDAY(created_at)))) AS sortDay " ) 
		))
		->groupBy ( "sortDay" ) -> get();
	}
	
	private function getDirectReferralBonusGroups() {
		return DirectReferrals::select (array(
				new raw ( "count(recruitee_id) AS totalCount" ),
				new raw ( " CONCAT(IF(created_at - INTERVAL 6 day < 0, 
                0, 
                IF(WEEKDAY(date(created_at) - INTERVAL 6 DAY) = 0, 
                    date(created_at) - INTERVAL 6 DAY, 
                    date(created_at) - INTERVAL ((WEEKDAY(date(created_at)) - 0)) DAY)),
            ' ~ ', date(DATE_ADD(created_at, INTERVAL (8 - IF(DAYOFWEEK(created_at)=1, 8, DAYOFWEEK(created_at))) DAY))) AS week" ),
				new raw ( " IF((WEEKDAY(date(created_at)) - 0) >= 0, 
                TO_DAYS(created_at) - (WEEKDAY(created_at) - 0), 
                TO_DAYS(created_at) - (7 - (0 - WEEKDAY(created_at)))) AS sortDay " ) 
		))->groupBy ( "sortDay" ) -> get();
	}
	
	public function ViewPairHistory($week) {
		$weekRange = json_decode(base64_decode($week), TRUE);
		$startDay = ltrim(rtrim($weekRange[0]));
		$endDay = ltrim(rtrim($weekRange[1]));
		
		$user = Sentry::getUser();
		$this -> data['user'] = $user;
		$this -> data['title'] = sprintf("As of %s to %s", $startDay, $endDay);
		
		$histories = DB::select(sprintf("SELECT * FROM pair_bonus WHERE (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
		$this -> data['histories'] = $histories;
		
		View::display('@adminbonushistory/pairbonushistory.twig', $this -> data);
	}
	
	public function ViewDrHistory($week) {
		$weekRange = json_decode(base64_decode($week), TRUE);
		$startDay = ltrim(rtrim($weekRange[0]));
		$endDay = ltrim(rtrim($weekRange[1]));
		
		$user = Sentry::getUser();
		$this -> data['user'] = $user;
		$this -> data['title'] = sprintf("As of %s to %s", $startDay, $endDay);
		
		$histories = DB::select(sprintf("SELECT * FROM direct_referrals WHERE (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
		$this -> data['histories'] = $histories;
		
		View::display('@adminbonushistory/drbonushistory.twig', $this -> data);
	}
}
