<?php

namespace CashierUnilevel\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \raw;
use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Cashier\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \UnilevelPurchaseLogs;

class CashierUnilevelSummarylogsController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('cashier_sidebar') -> setActiveMenu('unilevel_logs');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Summary Logs';
		$this -> data['user'] = $user;

		$this -> data['summaryLogs'] = $this -> getSummaryLogs();

		/** render the template */
		View::display('@cashierunilevel/summary_logs/index.twig', $this -> data);
	}
	
	public function ViewPurchaseLogsHistory($monthly) {
		$monthRange = json_decode(base64_decode($monthly), TRUE);
		$startDay = ltrim(rtrim($monthRange[0]));
		$endDay = ltrim(rtrim($monthRange[1]));
		
		$user = Sentry::getUser();
		$this -> data['user'] = $user;
		$this -> data['title'] = sprintf("As of %s to %s", $startDay, $endDay);
		
		$purchaseLogs = DB::select(sprintf("SELECT * FROM unilevel_purchase_logs WHERE (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
		$this -> data['purchaseLogs'] = $purchaseLogs;
		
		View::display('@cashierunilevel/summary_logs/purchaselogshistory.twig', $this -> data);
	}
	
	private function getSummaryLogs() {
		$groupDates = DB::select("SELECT created_at as date from unilevel_purchase_logs GROUP BY MONTH(created_at)");
		$dateRange = array();
		foreach ($groupDates as $key => $date) {
			$StartDate = new \DateTime($date['date']);
			$StartDate -> modify('first day of this month');
			$startDay = $StartDate -> format('Y-m-d');

			$EndDate = new \DateTime($date['date']);
			$EndDate -> modify('last day of this month');
			$endDay = $EndDate -> format('Y-m-d');
			
			$dateRange[] = sprintf("%s ~ %s", $startDay, $endDay);
		}
		return $dateRange;
	}

}
