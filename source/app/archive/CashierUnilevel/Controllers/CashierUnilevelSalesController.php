<?php

namespace CashierUnilevel\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Cashier\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \CodeGenerator;

use \Users;
use \UnilevelProducts;
use \UnilevelPurchaseLogs;
use \DirectReferrals;
use \BonusManager;

class CashierUnilevelSalesController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('cashier_sidebar') -> setActiveMenu('unilevel_sales');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$randomCode = new CodeGenerator();

		$this -> data['title'] = 'Sales';
		$this -> data['user'] = $user;
		
		$allUsers = User::where('user_type', '<>', 1) 
						-> where('user_type', '<>', 2) 
						-> where('is_registered', '=', 1) 
						-> where('id', '<>', $user -> id) 
						-> where('id', '<>', 3) 
						-> get();
		$this -> data['all_users'] = $allUsers;

		$this -> data['purchaseLogs'] = $this -> getCurrentMonthPurchaseLogs();
		$this -> data['products'] = UnilevelProducts::all();
		$this -> data['control_number'] = $randomCode -> generateControlNumber(6);

		/** render the template */
		View::display('@cashierunilevel/sales/index.twig', $this -> data);
	}

	public function getCurrentMonthPurchaseLogs() {
		$StartDate = new \DateTime('now');
		$StartDate -> modify('first day of this month');
		$startDay = $StartDate -> format('Y-m-d');

		$EndDate = new \DateTime('now');
		$EndDate -> modify('last day of this month');
		$endDay = $EndDate -> format('Y-m-d');

		return DB::select(sprintf("SELECT * FROM unilevel_purchase_logs WHERE (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
	}

	public function purchaseProduct() {
		$user = Sentry::getUser();

		$CN = Input::post('control_number');
		$user_id = Input::post('user_id');
		$product_id = Input::post('product_id');
		$quantity = Input::post('quantity');
		$paymentType = Input::post('payment_type');

		$product = UnilevelProducts::find($product_id);
		$member = Users::find($user_id);

		if (!$member) {
			App::flash('message_status', false);
			App::flash('message', "Member Not Defined");
			Response::redirect($this -> siteUrl("cashier/unilevel/sales"));
			return;
		}

		if (!$product) {
			App::flash('message_status', false);
			App::flash('message', "No Selected Product");
			Response::redirect($this -> siteUrl("cashier/unilevel/sales"));
			return;
		}

		$totalPV = $product -> pv * $quantity;
		$totalUPV = $product -> upv * $quantity;
		$totalAmount = $product -> srp * $quantity;
		
		if ($paymentType == 2) {
			$GC_balance = BonusManager::getCurrentTotalGC($member -> id);
			if ($totalAmount > $GC_balance) {
				App::flash('message_status', false);
				App::flash('message', "Insufficient Amount of GC");
				Response::redirect($this -> siteUrl("cashier/unilevel/sales"));
				return;
			}
		}

		$purchase = new UnilevelPurchaseLogs();
		$purchase -> member_id = $member -> id;
		$purchase -> control_number = $CN;
		$purchase -> product_code = $product -> product_code;
		$purchase -> quantity = $quantity;
		$purchase -> total_amount = $totalAmount;
		$purchase -> total_pv = $totalPV;
		$purchase -> total_upv = $totalUPV;
		$purchase -> payment_type = $paymentType;
		$purchase -> created_by = $user -> id;
		$purchase -> save();

		if ($paymentType == 1) {
			$this -> processRedundantBinaryAndUnilevel($member -> id, $purchase);
	
			if (BonusManager::hasPVMaintenance($member -> id)) {
				BonusManager::enableDMBIncome($member -> id);
			}
		}

		App::flash('message_status', true);
		App::flash('message', "Success");
		Response::redirect($this -> siteUrl("cashier/unilevel/sales"));
	}

	private function processRedundantBinaryAndUnilevel($user_id, $purchase) {
		$unilevels = $this -> unilevels($user_id);

		foreach ($unilevels as $key => $direct_id) {
			$level = $key + 1;

			$earning = 0;
			if ($level == 1) {
				$earning = $purchase -> total_pv * 0.3;
			} elseif ($level == 2) {
				$earning = $purchase -> total_pv * 0.15;
			} elseif ($level == 3 || $level == 4 || $level == 5 || $level == 6 || $level == 7) {
				$earning = $purchase -> total_pv * 0.05;
			} elseif ($level == 8 || $level == 9 || $level == 10) {
				$earning = $purchase -> total_pv * 0.1;
			}
			if (BonusManager::hasPVMaintenance($direct_id)) {
				BonusManager::add_UPV_PPV_Points($direct_id, $user_id, $earning, true);
			}
		}
		
		foreach ($unilevels as $key => $direct_id) {
			$level = $key + 1;

			$earning = 0;
			if ($level == 1) {
				$earning = $purchase -> total_upv * 0.3;
			} elseif ($level == 2) {
				$earning = $purchase -> total_upv * 0.15;
			} elseif ($level == 3 || $level == 4 || $level == 5 || $level == 6 || $level == 7) {
				$earning = $purchase -> total_upv * 0.05;
			} elseif ($level == 8 || $level == 9 || $level == 10) {
				$earning = $purchase -> total_upv * 0.1;
			}
			if (BonusManager::hasPVMaintenance($direct_id)) {
				BonusManager::add_UPV_PPV_Points($direct_id, $user_id, $earning);
			}
		}
	}

	private function unilevels($member_id) {
		$unilevel = array();
		// 1st level
		$first_level = DirectReferrals::where('recruitee_id', '=', $member_id) -> first();
		if ($first_level) {
			array_push($unilevel, $first_level -> recruiter_id);

			// 2nd level
			$second_level = DirectReferrals::where('recruitee_id', '=', $first_level -> recruiter_id) -> first();
			if ($second_level) {
				array_push($unilevel, $second_level -> recruiter_id);

				// 3rd level
				$third_level = DirectReferrals::where('recruitee_id', '=', $second_level -> recruiter_id) -> first();
				if ($third_level) {
					array_push($unilevel, $third_level -> recruiter_id);

					// 4th level
					$fourth_level = DirectReferrals::where('recruitee_id', '=', $third_level -> recruiter_id) -> first();
					if ($fourth_level) {
						array_push($unilevel, $fourth_level -> recruiter_id);

						// 5th level
						$fifth_level = DirectReferrals::where('recruitee_id', '=', $fourth_level -> recruiter_id) -> first();
						if ($fifth_level) {
							array_push($unilevel, $fifth_level -> recruiter_id);

							// 6th level
							$sixtth_level = DirectReferrals::where('recruitee_id', '=', $fifth_level -> recruiter_id) -> first();
							if ($sixtth_level) {
								array_push($unilevel, $sixtth_level -> recruiter_id);

								// 7th level
								$seventh_level = DirectReferrals::where('recruitee_id', '=', $sixtth_level -> recruiter_id) -> first();
								if ($seventh_level) {
									array_push($unilevel, $seventh_level -> recruiter_id);

									// 8th level
									$eigth_level = DirectReferrals::where('recruitee_id', '=', $seventh_level -> recruiter_id) -> first();
									if ($eigth_level) {
										array_push($unilevel, $eigth_level -> recruiter_id);

										// 9th level
										$nineth_level = DirectReferrals::where('recruitee_id', '=', $eigth_level -> recruiter_id) -> first();
										if ($nineth_level) {
											array_push($unilevel, $nineth_level -> recruiter_id);

											// 10th level
											$tenth_level = DirectReferrals::where('recruitee_id', '=', $nineth_level -> recruiter_id) -> first();
											if ($tenth_level) {
												array_push($unilevel, $tenth_level -> recruiter_id);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $unilevel;
	}

}
