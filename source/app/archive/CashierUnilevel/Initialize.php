<?php

namespace CashierUnilevel;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'CashierUnilevel';
	}
	
	public function getModuleAccessor() {
		return 'cashierunilevel';
	}
	
	public function registerCashierMenu() {
		$cashierMenu = Menu::get ( 'cashier_sidebar' );
		
		$unilevelGroup = $cashierMenu->createItem ( 'unilevelgroup', array (
				'label' => 'Unilevel',
				'icon' => 'cogs',
				'url' => '#' 
		) );
		$unilevelGroup->setAttribute('class', 'nav nav-second-level');
		
		//unilevel products
		$productMenu = $cashierMenu->createItem('unilevel_products', array(
            'label' => 'Products',
            'icon'  => 'cubes',
            'url'   => 'cashier/unilevel/products'
        ));
        
        //unilevel sales
		$salesMenu = $cashierMenu->createItem('unilevel_sales', array(
            'label' => 'Sales',
            'icon'  => 'credit-card',
            'url'   => 'cashier/unilevel/sales'
        ));
        
        //unilevel summary logs
		$summaryLogsMenu = $cashierMenu->createItem('unilevel_logs', array(
            'label' => 'Summary Logs',
            'icon'  => 'list',
            'url'   => 'cashier/unilevel/summarylogs'
        ));
		
		// add to group
		// $unilevelGroup->addChildren($productMenu);
		$unilevelGroup->addChildren($salesMenu);
		$unilevelGroup->addChildren($summaryLogsMenu);
		
		$cashierMenu->addItem ( 'unilevelgroup', $unilevelGroup );
	}
	
	public function registerCashierRoute() {
		$this -> setProductRoutes();
		$this -> setSalesRoutes();
		$this -> setSummaryLogs();
	}
	
	private function setSalesRoutes() {
		Route::resource ('/unilevel/sales', 'CashierUnilevel\Controllers\CashierUnilevelSalesController');
		
		Route::post ('/unilevel/sales/purchase', 'CashierUnilevel\Controllers\CashierUnilevelSalesController:purchaseProduct') -> name('cashier_purchaseProduct');
	}
	
	private function setProductRoutes() {
		Route::resource ('/unilevel/products', 'CashierUnilevel\Controllers\CashierUnilevelProductsController');
		
		Route::post ('/unilevel/products/add', 'CashierUnilevel\Controllers\CashierUnilevelProductsController:addProduct') -> name('cashier_addProduct');
		Route::post ('/unilevel/products/edit', 'CashierUnilevel\Controllers\CashierUnilevelProductsController:editProduct') -> name('cashier_editProduct');
		Route::post ('/unilevel/products/delete', 'CashierUnilevel\Controllers\CashierUnilevelProductsController:deleteProduct') -> name('cashier_deleteProduct');
	}
	
	private function setSummaryLogs() {
		Route::resource ('/unilevel/summarylogs', 'CashierUnilevel\Controllers\CashierUnilevelSummarylogsController');
		
		Route::get ('/unilevel/summarylogs/view/:monthly', 'CashierUnilevel\Controllers\CashierUnilevelSummarylogsController:ViewPurchaseLogsHistory');
	}
}
