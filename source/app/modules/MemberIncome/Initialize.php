<?php

namespace MemberIncome;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'MemberIncome';
	}

	public function getModuleAccessor() {
		return 'memberincome';
	}

	// public function registerMemberMenu() {
	// $user = Sentry::getUser();
	// $adminMenu = Menu::get ( 'member_sidebar' );
	//
	// // Create Account Details Menu
	// $incomeMenu = $adminMenu->createItem ( 'memberincome', array (
	// 'label' => 'Income Report',
	// 'icon' => 'money',
	// 'url' => 'member/income'
	// ) );
	// $adminMenu->addItem ( 'memberincome', $incomeMenu );
	// }
	public function registerMemberRoute() {
		Route::resource('/income', 'MemberIncome\Controllers\MemberIncomeController');
	}

}
