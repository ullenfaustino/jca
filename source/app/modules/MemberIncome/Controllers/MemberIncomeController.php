<?php

namespace MemberIncome\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \BonusManager;
use \PairBonus;
use \DirectReferrals;
use \DirectMatchingBonus;
use \Points;
use \Commissions;
use \MemberPayoutRequests;

class MemberIncomeController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = "member_income";
	}

	/**
	 * display list of resource
	 */
	public function index($page = 1) {
		$user = Sentry::getUser();

		$this -> data['title'] = 'My Income';
		$this -> data['user'] = $user;
		
		// PERSONAL MATCHING BONUS
		$pairings = PairBonus::where('user_id', '=', $user -> id)
							-> where('is_paired', '=', 1)
							-> get();
		$this -> data['pairings'] = $pairings;
		
		$direct_referrals = DirectReferrals::where("recruiter_id", "=", $user -> id) -> get();
		$this -> data['direct_referrals'] = $direct_referrals;
		
		$payoutRequests = MemberPayoutRequests::where("user_id", "=", $user->id) 
       										-> where("status", "=", 1)
       										-> get();
       	$this -> data['payout_requests'] = $payoutRequests;
				
		View::display('@memberincome/index.twig', $this -> data);
	}
}
