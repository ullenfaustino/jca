<?php

namespace FFAdminPayout;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'FFAdminPayout';
    }

    public function getModuleAccessor(){
        return 'adminpayout';
    }

    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $member = $adminMenu->createItem('adminpayout', array(
            'label' => 'Payout Request',
            'icon'  => 'money',
            'url'   => 'admin/adminpayout'
        ));

		$adminMenu->addItem('adminpayout', $member);
    }

    public function registerAdminRoute(){
        Route::resource('/adminpayout', 'FFAdminPayout\Controllers\AdminPayoutController');
        Route::post('/adminpayout/approve', 'FFAdminPayout\Controllers\AdminPayoutController:approvePayout') -> name("admin_approve_payout");
    
        Route::get('/adminpayout/download/pdf/pending', 'FFAdminPayout\Controllers\AdminPayoutController:downloadPendingPayoutPDF');
        Route::get('/adminpayout/download/pdf/approved/:status', 'FFAdminPayout\Controllers\AdminPayoutController:downloadApprovedPayoutPDF');
    }
}
