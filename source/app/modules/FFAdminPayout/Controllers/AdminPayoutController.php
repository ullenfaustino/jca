<?php

namespace FFAdminPayout\Controllers;

use \raw;
use \App;
use \View;
use \Menu;
use \User;
use \Users;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \MemberPayoutRequests;
use \PairBonus;
use \DirectReferrals;
use \PMLogs;
use \CodeGenerator;
use \GenericHelper;

class AdminPayoutController extends BaseController {
	
	public function __construct() {
		parent::__construct();
		$this -> data["active_menu"] = "admin_payout";
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['user'] = $user;
		
		$payout_requests = MemberPayoutRequests::where("status", "=", 0) -> get();
		foreach ($payout_requests as $key => $payout_request) {
			$strRange = explode("|", $payout_request -> week_range);
            $from_Date = new \DateTime($strRange[0]);
            $to_Date = new \DateTime($strRange[1]);
            
            $payout_request -> dt_range = $from_Date -> format("F j, Y") . " to " . $to_Date -> format("F j, Y");
		}
		$this -> data["payout_requests"] = $payout_requests;
		
		$approved_payout_requests = MemberPayoutRequests::where("status", "=", 1) -> get();
		foreach ($approved_payout_requests as $key => $approved_payout_request) {
            $strRange = explode("|", $approved_payout_request -> week_range);
            $from_Date = new \DateTime($strRange[0]);
            $to_Date = new \DateTime($strRange[1]);
            
            $approved_payout_request -> dt_range = $from_Date -> format("F j, Y") . " to " . $to_Date -> format("F j, Y");
        }
		$this -> data["approved_payout_requests"] = $approved_payout_requests;
		
		View::display('@adminpayout/index.twig', $this -> data);
	}
	
	public function approvePayout() {
		$admin = Sentry::getUser();
		$trans_code = Input::post("trans_code");
		
		$codeGen = new CodeGenerator();
		
		$request = MemberPayoutRequests::where("transaction_number", "=", $trans_code) -> first();
		if ($request) {
			$request -> status = 1;
			$request -> approved_by = $admin -> id;
			if ($request -> save()) {
				//Pair Bonus
				$pair_bonuses = PairBonus::where('user_id', '=', $request -> user_id)
	            						-> where('is_gc', '=', 0)
								        -> where('is_paired', '=', 1)
								        -> where('is_valid', '=', 1)
								        -> where('payout_status', '=', 1)
	            						-> get();
	            foreach ($pair_bonuses as $key => $pair_bonus) {
	                $pair_bonus -> payout_status = 2;
	                $pair_bonus -> save();
	            }
	            
	            // DR Bonus
	            $dr_bonuses = DirectReferrals::where("recruiter_id","=", $request -> user_id)
	            								-> where('payout_status', '=', 1)
	            								-> get();
	           	foreach ($dr_bonuses as $key => $dr_bonus) {
					$dr_bonus -> payout_status = 2;
					$dr_bonus -> save();
			   	}
	            
			   	if ($request -> is_pm_deducted == 1) {
			   		// Add to PM Logs
					$pm_log = new PMLogs();
					$pm_log -> trans_code = $request -> transaction_number;
					$pm_log -> verification_code = $codeGen -> getToken(10);
					$pm_log -> user_id = $request -> user_id;
					$pm_log -> amount = 1500;
					$pm_log -> save();
			   	}
			   	
			   	// sms notification
			   	$user = Users::find($request -> user_id);
			   	$mobile = sprintf("%s%s", $user -> country_code, $user -> mobile_number);
		        $message = "[JCA International Corporation]\n";
		        $message .= sprintf("Hi %s, we are pleased to announce that your Commission Pay-Out Cheque is already available. Kindy bring 1 valid ID for verification and claim this in our office in Unit 810, Raffles Corporate Center, Emerald Ave. Ortigas Center on Friday 2 PM to 10 PM. Thank you!\n", $user -> username);
		        
		        $sms_cmd = sprintf("php %s/sms_notification.php %s %s &", WORKERS_PATH, $mobile, base64_encode($message));
		        pclose(popen($sms_cmd, "w"));
			}
			App::flash('message_status', true);
        	App::flash('message', "Transaction Completed.");
		} else {
			App::flash('message_status', false);
        	App::flash('message', "Invalid Transaction Number.");
		}
		Response::redirect($this -> siteUrl("admin/adminpayout"));
	}
	
	public function downloadPendingPayoutPDF() {
        $source_url = sprintf("%sreports/list/pending/payouts", GenericHelper::baseUrl());
        
        $file_path = __DIR__ . "/../../../../public/assets/downloads/";
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777, TRUE);
        }

        $pdfFile = sprintf("%s%s.pdf", $file_path, str_replace(" ", "_", date("F j Y")) . "_pending_payouts");

        $wkhtml_path = __DIR__ . "/../../../lib/wkhtml/bin/";
        $cmd = sprintf("%swkhtmltopdf %s %s", $wkhtml_path, $source_url, $pdfFile);
        echo "wkHTMLtoPDF_COMMAND-> $cmd \n";
        exec($cmd);

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($pdfFile) . "\"");

        readfile($pdfFile);

        unlink($pdfFile);
    }
    
    public function downloadApprovedPayoutPDF($status) {
        $source_url = sprintf("%sreports/list/approved/payouts/%s", GenericHelper::baseUrl(), $status);
        
        $file_path = __DIR__ . "/../../../../public/assets/downloads/";
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777, TRUE);
        }

        $pdfFile = sprintf("%s%s.pdf", $file_path, str_replace(" ", "_", date("F j Y")) . "_approved_payouts");

        $wkhtml_path = __DIR__ . "/../../../lib/wkhtml/bin/";
        $cmd = sprintf("%swkhtmltopdf %s %s", $wkhtml_path, $source_url, $pdfFile);
        echo "wkHTMLtoPDF_COMMAND-> $cmd \n";
        exec($cmd);

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($pdfFile) . "\"");

        readfile($pdfFile);

        unlink($pdfFile);
    }
}
