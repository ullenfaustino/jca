<?php

namespace IIMemberPayout\Controllers;

use \raw;
use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \MemberPayoutRequests;
use \CodeGenerator;
use \AdminControl;
use \PairBonus;
use \BonusManager;
use \DirectReferrals;
use \PMlogs;
use \Users;

class MemberPayoutController extends BaseController {

	public function __construct() {
		parent::__construct();
		// Menu::get('member_sidebar') -> setActiveMenu('payout');
		$this -> data['active_menu'] = "member_payout";
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Payout Transaction';
		$this -> data['user'] = $user;

		$togglePay = AdminControl::where('name', '=', 'member_payout') -> first();
		$this -> data['disabled'] = ($togglePay -> value == 1) ? "" : "disabled";
        
        $weeklyIncome = $this -> getWeeklyPairingIncome($user -> id);
        $this -> data['weekly_income'] = $weeklyIncome;
        
        $weeklyIncomeDR = $this -> getWeeklyDRIncome($user -> id);
        $this -> data['weekly_income_dr'] = $weeklyIncomeDR;
        // print_r(json_encode($weeklyIncomeDR));exit;
        
        $hasActivePayoutRequest = MemberPayoutRequests::where("user_id", "=", $user->id)
        											-> where("status", "=", 0)
        											-> first();
       	$this -> data['has_active_payout_request'] = ($hasActivePayoutRequest) ? TRUE : FALSE;
       	
       	$payoutRequests = MemberPayoutRequests::where("user_id", "=", $user->id) 
       										-> get();
        foreach ($payoutRequests as $key => $payoutRequest) {
            $strRange = explode("|", $payoutRequest -> week_range);
            $from_Date = new \DateTime($strRange[0]);
            $to_Date = new \DateTime($strRange[1]);
            
            $payoutRequest -> dt_range = $from_Date -> format("F j, Y") . " to " . $to_Date -> format("F j, Y");
            
            $check_issued = Users::find($payoutRequest -> check_issued_by);
            if ($check_issued) {
                $payoutRequest -> check_issued_by_name = $check_issued -> username;
            }
        }
        $this -> data['payout_requests'] = $payoutRequests;
       	
       	$this -> data['has_current_maintenance'] = BonusManager::hasCurrentMonthPM($user -> id);
        
		/** render the template */
		App::render('@memberpayout/index.twig', $this -> data);
	}
    
    private function getWeeklyPairingIncome($user_id) {
        $weekly = PairBonus::select ([ 
                // new raw ( "SUM(amount) AS total" ),
                new raw ( " CONCAT(IF(created_at - INTERVAL 6 day < 0, 
                0, 
                IF(WEEKDAY(date(created_at) - INTERVAL 6 DAY) = 0, 
                    date(created_at) - INTERVAL 6 DAY, 
                    date(created_at) - INTERVAL ((WEEKDAY(date(created_at)) - 0)) DAY)),
            ' to ', date(DATE_ADD(created_at, INTERVAL (8 - IF(DAYOFWEEK(created_at)=1, 8, DAYOFWEEK(created_at))) DAY))) AS week" ),
                new raw ( " IF((WEEKDAY(date(created_at)) - 0) >= 0, 
                TO_DAYS(created_at) - (WEEKDAY(created_at) - 0), 
                TO_DAYS(created_at) - (7 - (0 - WEEKDAY(created_at)))) AS sortDay " ) ,
                new raw ( " IF(WEEKDAY(date(created_at) - INTERVAL 6 DAY) = 0, 
                    date(created_at) - INTERVAL 6 DAY, 
                    date(created_at) - INTERVAL ((WEEKDAY(date(created_at)) - 0)) DAY) as start_day" ) ,
                new raw ( "date(DATE_ADD(created_at, INTERVAL (8 - IF(DAYOFWEEK(created_at)=1, 8, DAYOFWEEK(created_at))) DAY)) as end_day" )
         ])
        ->where("user_id","=",$user_id)
        ->groupBy("sortDay")
        -> get();
        
        foreach ($weekly as $key => $week) {
            $pair_Bonuses = PairBonus::where('user_id', '=', $user_id)
            						-> where('is_gc', '=', 0)
							        -> where('is_paired', '=', 1)
							        -> where('is_valid', '=', 1)
            						-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $week -> start_day, $week -> end_day)) 
            						-> sum("amount");
            
            // Approved Pair Bonus
            $approved_pair_bonus = PairBonus::where('user_id', '=', $user_id)
            						-> where('is_gc', '=', 0)
							        -> where('is_paired', '=', 1)
							        -> where('is_valid', '=', 1)
							        -> where('payout_status', '=', 2)
            						-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $week -> start_day, $week -> end_day)) 
            						-> sum("amount");
            
           	// Total Approved Bonus
           	$week -> total_approved_bonus = $approved_pair_bonus;
           	$week -> total_available_payout = $pair_Bonuses - $approved_pair_bonus;
        }
        return $weekly;
    }
    
    private function getWeeklyDRIncome($user_id) {
        $weekly = DirectReferrals::select ([ 
                // new raw ( "SUM(amount) AS total" ),
                new raw ( " CONCAT(IF(created_at - INTERVAL 6 day < 0, 
                0, 
                IF(WEEKDAY(date(created_at) - INTERVAL 6 DAY) = 0, 
                    date(created_at) - INTERVAL 6 DAY, 
                    date(created_at) - INTERVAL ((WEEKDAY(date(created_at)) - 0)) DAY)),
            ' to ', date(DATE_ADD(created_at, INTERVAL (8 - IF(DAYOFWEEK(created_at)=1, 8, DAYOFWEEK(created_at))) DAY))) AS week" ),
                new raw ( " IF((WEEKDAY(date(created_at)) - 0) >= 0, 
                TO_DAYS(created_at) - (WEEKDAY(created_at) - 0), 
                TO_DAYS(created_at) - (7 - (0 - WEEKDAY(created_at)))) AS sortDay " ) ,
                new raw ( " IF(WEEKDAY(date(created_at) - INTERVAL 6 DAY) = 0, 
                    date(created_at) - INTERVAL 6 DAY, 
                    date(created_at) - INTERVAL ((WEEKDAY(date(created_at)) - 0)) DAY) as start_day" ) ,
                new raw ( "date(DATE_ADD(created_at, INTERVAL (8 - IF(DAYOFWEEK(created_at)=1, 8, DAYOFWEEK(created_at))) DAY)) as end_day" )
         ])
        ->where("recruiter_id","=",$user_id)
        ->groupBy("sortDay")
        -> get();
        
        foreach ($weekly as $key => $week) {
            $dr_bonus = DirectReferrals::where("recruiter_id","=", $user_id)
        								-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $week -> start_day, $week -> end_day)) 
        								-> sum("amount");
        								
        	$approved_dr_bonus = DirectReferrals::where("recruiter_id","=", $user_id)
        								-> where("payout_status", "=", 2)
        								-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $week -> start_day, $week -> end_day)) 
        								-> sum("amount");							
            
           	$week -> total_approved_bonus = $approved_dr_bonus;
           	$week -> total_available_payout = $dr_bonus - $approved_dr_bonus;
        }
        return $weekly;
    }
    
    public function requestPayout() {
    	$codeGen = new CodeGenerator();
        $user = Sentry::getUser();
        
        $trans_code = $codeGen -> getToken(12);
		
		$total_available_payout = 0;
		$bonuses = $this -> getWeeklyPairingIncome($user -> id);
		foreach ($bonuses as $key => $bonus) {
			$total_available_payout += $bonus -> total_available_payout;
		}
		
		$total_available_payout_dr = 0;
		$bonuses_dr = $this -> getWeeklyDRIncome($user -> id);
		foreach ($bonuses_dr as $key => $bonus_dr) {
			$total_available_payout_dr += $bonus_dr -> total_available_payout;
		}
		
		$start_day = "n/a";
		foreach ($bonuses as $key => $bonus) {
			$pair_Bonuses = PairBonus::where('user_id', '=', $user -> id)
            						-> where('is_gc', '=', 0)
							        -> where('is_paired', '=', 1)
							        -> where('is_valid', '=', 1)
							        -> where("payout_status", "=", 0)
            						-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $bonus -> start_day, $bonus -> end_day)) 
            						-> sum("amount");
            if ($pair_Bonuses > 0) {
            	$start_day = $bonus -> start_day;
            	break;
            }
		}
		
		$end_day = "n/a";
		foreach ($bonuses as $key => $bonus) {
			$pair_Bonuses = PairBonus::where('user_id', '=', $user -> id)
            						-> where('is_gc', '=', 0)
							        -> where('is_paired', '=', 1)
							        -> where('is_valid', '=', 1)
							        -> where("payout_status", "=", 0)
            						-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $bonus -> start_day, $bonus -> end_day)) 
            						-> sum("amount");
            if ($pair_Bonuses > 0) {
            	$end_day = $bonus -> end_day;
            }
		}
		
		$week_range = sprintf("%s | %s", $start_day, $end_day);
		
		$total_payout_amount = $total_available_payout + $total_available_payout_dr;
		$processing_fee = 100;
		$tax = $total_payout_amount * 0.10;
		$deducted_payout_amount = $total_payout_amount - ($processing_fee + $tax);
		
		$is_pm_deducted = false;
		if ((!BonusManager::hasCurrentMonthPM($user -> id)) && ($total_payout_amount > 5000)) {
			if ($deducted_payout_amount > 1500) {
				$deducted_payout_amount -= 1500;
				$is_pm_deducted = true;
			} else {
				App::flash('message_status', false);
	        	App::flash('message', "Insufficient Amount to pay for the 10% processing fee and product maintenance.");
	        	Response::redirect($this -> siteUrl("member/payout"));
	        	return;
			}
		}
		
		if ($deducted_payout_amount <= 0) {
			App::flash('message_status', false);
        	App::flash('message', "Insufficient Amount to encash.");
        	Response::redirect($this -> siteUrl("member/payout"));
        	return;
		}
		
		$payout_request = new MemberPayoutRequests();
		$payout_request -> transaction_number = $trans_code;
		$payout_request -> user_id = $user -> id;
		$payout_request -> amount_encash = $deducted_payout_amount;
		$payout_request -> total_amount = $total_payout_amount;
		$payout_request -> is_pm_deducted = ($is_pm_deducted) ? 1 : 0;
		$payout_request -> week_range = $week_range;
		
		if ($payout_request -> save()) {
			// Updates Bonuses payout Status
			foreach ($bonuses as $key => $bonus) {
				//Pair Bonus
				$pair_bonuses = PairBonus::where('user_id', '=', $user -> id)
	            						-> where('is_gc', '=', 0)
								        -> where('is_paired', '=', 1)
								        -> where('is_valid', '=', 1)
								        -> where('payout_status', '=', 0)
	            						-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $bonus -> start_day, $bonus -> end_day)) 
	            						-> get();
	            foreach ($pair_bonuses as $key => $pair_bonus) {
	                $pair_bonus -> payout_status = 1;
	                $pair_bonus -> save();
	            }
			}
			
			foreach ($bonuses_dr as $key => $bonus_dr) {
	            // DR Bonus
	            $dr_bonuses = DirectReferrals::where("recruiter_id","=", $user -> id)
	            								-> where('payout_status', '=', 0)
	            								-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $bonus_dr -> start_day, $bonus_dr -> end_day)) 
	            								-> get();
	           	foreach ($dr_bonuses as $key => $dr_bonus) {
					$dr_bonus -> payout_status = 1;
					$dr_bonus -> save();
			   	}
			}
		}
		
        App::flash('message_status', true);
        App::flash('message', "Transaction successfully submitted!");
        Response::redirect($this -> siteUrl("member/payout"));
    }
    
}
