<?php

namespace IIMemberPayout;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'IIMemberPayout';
	}
	public function getModuleAccessor() {
		return 'memberpayout';
	}
	// public function registerMemberMenu() {
		// $adminMenu = Menu::get('member_sidebar');
        // $member = $adminMenu->createItem('payout', array(
            // 'label' => 'e-Wallet',
            // 'icon' => 'credit-card',
            // 'url' => 'member/payout'
        // ));
        // $adminMenu->addItem('payout', $member);
	// }
	public function registerMemberRoute() {
		Route::resource ( '/payout', 'IIMemberPayout\Controllers\MemberPayoutController' );
		Route::post ( '/payout/withdraw', 'IIMemberPayout\Controllers\MemberPayoutController:requestPayout' ) -> name('encashment');
	}
}
