<?php

namespace Geneology;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'Geneology';
	}

	public function getModuleAccessor() {
		return 'geneology';
	}

	// public function registerMemberMenu() {
		// $user = Sentry::getUser();
// 		
		// if ($user -> account_type != 4) {
			// $adminMenu = Menu::get('member_sidebar');
			// $member = $adminMenu -> createItem('geneology', 
					// array('label' => 'Genealogy', 
						  // 'icon' => 'sitemap', 
						  // 'url' => sprintf("member/%s/geneology", $user -> id)
						 // )
			// );
// 	
			// $adminMenu -> addItem('geneology', $member);
		// }
	// }

	public function registerMemberRoute() {
		Route::resource('/geneology', 'Geneology\Controllers\GeneologyController');
		
		Route::post('/geneology/register/member', 'Geneology\Controllers\GeneologyController:registerNewMember') -> name('register_new_member');
		
		Route::get('/geneology/graph/pairing', 'Geneology\Controllers\GeneologyController:daily_pairing_stats');
	}

}
