<?php

namespace Geneology\Controllers;

use \User;
use \Users;
use \HierarchySiblings;
use \UsersHasHierarchySiblings;
use \DirectReferrals;
use \Codes;
use \UserBalance;
use \Constants;
use \OverrideHistory;
use \CodeGenerator;
use \BonusManager;
use \CrosslineValidator;
use \AdminControl;
use \raw;

use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Points;
use \GenericHelper;
use \PairBonus;

class GeneologyController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = 'geneology';
    }

    public function index() {
        $this -> data['title'] = 'Genealogy';

        $user = Sentry::getUser();
        $this -> data['user'] = $user;

        $toggleRegistration = AdminControl::where('name', '=', 'member_registration') -> first();
        $this -> data['toggle_registration'] = $toggleRegistration -> value;

        App::render('@geneology/index.twig', $this -> data);
    }

    private function countBAN() {
        $codeGen = new CodeGenerator();
        $uuid = $codeGen -> getToken(12);
        $refid = Users::where('ban', '=', $uuid) -> first();
        if (!$refid) {
            return $uuid;
        } else {
            return $this -> countBAN();
        }
    }

    public function registerNewMember() {
        $currUser = Sentry::getUser();
        $username = Input::post('username');
        $email = Input::post('email');
        $password = Input::post('password');
        $confirm_password = Input::post('confirm_password');
        $firstname = Input::post('first_name');
        $middlename = Input::post('middle_name');
        $lastname = Input::post('last_name');
        $mobile_number = Input::post('mobile_number');
        $country_code = Input::post("country_code");

        $firstIndex = substr($mobile_number, 0, 1);
        if ($firstIndex === "0") {
            $mobile_number = substr_replace($mobile_number, "", 0, 1);
        }

        if ($password !== $confirm_password) {
            App::flash('message_status', false);
            App::flash('message', "Password Does not matched.");
            Response::redirect($this -> siteUrl("member/geneology"));
            return;
        }

        $codeGen = new CodeGenerator();
        $ban = $this -> countBAN();
        $activation_code = $codeGen -> getToken(5);

        $credential = array('ban' => $ban, 'username' => trim($username), 'email' => trim($email), 'password' => $password, 'canonical_hash' => base64_encode($password), 'first_name' => trim($firstname), 'middle_name' => trim($middlename), 'last_name' => trim($lastname), 'mobile_number' => $mobile_number, 'country_code' => $country_code, 'activated' => 1, 'user_type' => 3, 'is_mobile_verified' => 1, 'is_registered' => 1, 'permissions' => array('member' => 1));

        $activationCode = Input::post('activation_code');
        $placementPosition = Input::post('placement');
        $placementHead = Input::post("placement_head_ban");

        $_code = Codes::where('generated_code', '=', $activationCode) -> first();
        if ($_code) {
            if ($_code -> is_used == 0) {
                // try {
                $placementHead_id = Users::where("ban", "=", $placementHead) -> first();
                if (!$placementHead_id) {
                    App::flash('message_status', false);
                    App::flash('message', "Invalid Geneology Head!");
                    Response::redirect($this -> siteUrl("member/geneology"));
                    return;
                }

                $head = Sentry::findUserById($placementHead_id -> id);
                $DR = Sentry::findUserById($currUser -> id);

                if (!CrosslineValidator::isCrosslinedRegistration($DR -> id, $head -> id)) {
                    App::flash('message_status', false);
                    App::flash('message', "Crosslining is not permitted!");
                    Response::redirect($this -> siteUrl("member/geneology"));
                    return;
                }

                $user = Sentry::register($credential, false);
                $user -> addGroup(Sentry::getGroupProvider() -> findByName('Member'));

                $user -> activation_code = $activation_code;
                $user -> save();

                if ($user) {
                    $hierarchy = new HierarchySiblings();
                    $hierarchy -> recruitee_id = $user -> id;
                    $hierarchy -> position = (strcasecmp($placementPosition, "Left") == 0) ? 0 : 1;
                    if ($hierarchy -> save()) {
                        $hasSiblings = new UsersHasHierarchySiblings();
                        $hasSiblings -> user_id = $head -> id;
                        $hasSiblings -> hierarchy_sibling_id = $hierarchy -> id;
                        $hasSiblings -> save();

                        $_code -> is_used = 1;
                        $_code -> user_id = $user -> id;
                        $_code -> save();

                        $process_cmd = sprintf("php %s/bonus_worker.php %s %s &", WORKERS_PATH, $user -> id, $DR -> id);
                        pclose(popen($process_cmd, "w"));

                        // BonusManager::updateDirectReferralBalance($DR -> id, $user -> id);
                        // BonusManager::processLogicTransaction($user -> id);

                        App::flash('message_status', true);
                        App::flash('message', "Member Successfully Saved!");

                        if ($this -> sendSMSVerificationCode($user)) {
                            App::flash('message_status', true);
                            App::flash('message', "Member Successfully Saved!");
                        } else {
                            App::flash('message_status', false);
                            App::flash('message', "Member Successfully Saved, but unable to send SMS notification.");
                        }
                    } else {
                        App::flash('message_status', false);
                        App::flash('message', "Unable to save member, please try again..");
                    }
                }
                // } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
                // App::flash('message_status', false);
                // App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
                // } catch ( \Exception $e ) {
                // App::flash('message_status', false);
                // App::flash('message', $e -> getMessage());
                // }
            } else {
                App::flash('act_code', $activationCode);
                App::flash('message_status', false);
                App::flash('message', "Activation code is already in used!");
            }
        } else {
            App::flash('act_code', $activationCode);
            App::flash('message_status', false);
            App::flash('message', "Activation code is invalid!");
        }
        Response::redirect($this -> siteUrl("member/geneology"));
    }

    private function sendSMSVerificationCode($user) {
        $mobile = sprintf("%s%s", $user -> country_code, $user -> mobile_number);
        echo "[SENDTO]|--> $mobile\n";
        $message = "[JCA International Corporation]\n";
        $message .= sprintf("Your account Username is : %s\n", $user -> username);
        $message .= sprintf("Your account Password is : %s\n", base64_decode($user -> canonical_hash));
        $message .= sprintf("Your verification code is : %s\n", $user -> activation_code);
        $message .= sprintf("You can login at %slogin\n", GenericHelper::baseUrl());
        echo $message;
        // return GenericHelper::sendSMS($mobile, $message);

        $sms_cmd = sprintf("php %s/sms_notification.php %s %s &", WORKERS_PATH, $mobile, base64_encode($message));
        pclose(popen($sms_cmd, "w"));

        return true;
    }

    /*
     * API for member contributions
     */
    public function daily_pairing_stats() {
        $user = Sentry::getUser();

        $pair_day = PairBonus::where("user_id", "=", $user -> id) -> whereRaw(sprintf("MONTH(created_at) = '%s' and YEAR(created_at) = '%s'", date("m"), date("Y"))) -> select(new raw("DATE(created_at) as day")) -> groupBy("day") -> get();
        foreach ($pair_day as $key => $day) {
            // convert day in word
            $fDate = new \DateTime($day -> day);
            $day -> day_word = $fDate -> format('M d, Y');
            // -------------------------------------------

            // paired count
            $paired_count = PairBonus::where("user_id", "=", $user -> id) -> where("is_valid", "=", 1) -> where("is_paired", "=", 1) -> whereRaw(sprintf("DATE(created_at) = '%s'", $day -> day)) -> count();
            $day -> paired_count = $paired_count;

            // unpaired count
            $unpaired_count = PairBonus::where("user_id", "=", $user -> id) -> where("is_valid", "=", 1) -> where("is_paired", "=", 0) -> whereRaw(sprintf("DATE(created_at) = '%s'", $day -> day)) -> count();
            $day -> unpaired_count = $unpaired_count;

            // encode count
            $encode_count = PairBonus::where("user_id", "=", $user -> id) -> whereRaw(sprintf("DATE(created_at) = '%s'", $day -> day)) -> count();
            $day -> encoded_count = $encode_count;
        }
        Response::setBody(json_encode($pair_day));
    }

}
