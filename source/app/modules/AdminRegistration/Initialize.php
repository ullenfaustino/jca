<?php

namespace AdminRegistration;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'AdminRegistration';
	}
	
	public function getModuleAccessor() {
		return 'adminregistration';
	}
	
	public function registerAdminMenu() {
		$adminMenu = Menu::get ( 'admin_sidebar' );
		
		$admin = $adminMenu->createItem ( 'admin_registration', array (
				'label' => 'Registration',
				'icon' => 'database',
				'url' => 'admin/registration' 
		));
		$admin->setAttribute('class', 'nav nav-second-level');
		
		$registrationMenu = $adminMenu->createItem('registration', array(
            'label' => 'Registration',
            'icon'  => 'user-plus',
            'url'   => 'admin/registration'
        ));
		
		$preRegisteredMenu = $adminMenu->createItem('pre-registered', array(
            'label' => 'Pre-Registered',
            'icon'  => 'users',
            'url'   => 'admin/pre-registered'
        ));
        
        $RegisteredMenu = $adminMenu->createItem('registered', array(
            'label' => 'Registered',
            'icon'  => 'users',
            'url'   => 'admin/registered'
        ));
        
        $addCashierMenu = $adminMenu->createItem('add_cashier', array(
            'label' => 'Add Cashier',
            'icon'  => 'user-plus',
            'url'   => 'admin/add/cashier'
        ));
		
		$admin->addChildren($registrationMenu);
		$admin->addChildren($preRegisteredMenu);
		$admin->addChildren($RegisteredMenu);
		$admin->addChildren($addCashierMenu);
		
		$adminMenu->addItem ( 'admin_registration', $admin);
	}
	
	public function registerAdminRoute() {
		Route::resource ( '/registration', 'AdminRegistration\Controllers\AdminRegistrationController' );
		
		Route::post ( '/add/cashier/new', 'AdminRegistration\Controllers\AdminRegistrationController:cashier_registration' ) -> name('cashier_registration');
		
		Route::get('/pre-registered', 'AdminRegistration\Controllers\AdminRegistrationController:pre_registered_view' );
		Route::get('/registered', 'AdminRegistration\Controllers\AdminRegistrationController:registered_view' );
		Route::get('/add/cashier', 'AdminRegistration\Controllers\AdminRegistrationController:cashier_registration_view' );
	}
}
