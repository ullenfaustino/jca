<?php

namespace AdminRegistration\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \AdminControl;
use \Users;

class AdminRegistrationController extends BaseController {

	public function __construct() {
		parent::__construct();
		Menu::get('admin_sidebar') -> setActiveMenu('admin_registration');
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Registration';
		$this -> data['user'] = $user;

		$toggleRegistration = AdminControl::where('name', '=', 'member_registration') -> first();
		$this -> data['toggle_registration'] = $toggleRegistration -> value;

		/** render the template */
		View::display('@adminregistration/index.twig', $this -> data);
	}

	public function pre_registered_view() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Pre-Registered List';
		$this -> data['user'] = $user;

		$this -> data['users'] = Users::where('user_type', '=', 3) -> where('username', '<>', 'HEAD') -> where('is_registered', '=', 0) -> get();

		/** render the template */
		View::display('@adminregistration/pre_registered.twig', $this -> data);
	}

	public function registered_view() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Registered List';
		$this -> data['user'] = $user;

		$this -> data['users'] = Users::where('user_type', '=', 3) -> where('username', '<>', 'HEAD') -> where('is_registered', '=', 1) -> get();

		/** render the template */
		View::display('@adminregistration/registered.twig', $this -> data);
	}

	public function cashier_registration_view() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Cashier Registration';
		$this -> data['user'] = $user;
		
		/** render the template */
		View::display('@adminregistration/cashier_registration.twig', $this -> data);
	}
	
	public function cashier_registration() {
		$username    = Input::post('username');
		$email       = Input::post('email');
		$firstname   = Input::post('first_name');
		$middlename  = Input::post('middle_name');
		$lastname    = Input::post('last_name');
		try {
			$credential = array('username' => trim($username), 
								'email' => trim($email),
								'password' => 'password',
								'first_name' => trim($firstname),
								'middle_name' => trim($middlename),
								'last_name' => trim($lastname),
								'activated'   => 1,
								'user_type'   => 2,
								'permissions' => array(
				                    'cashier'   => 1
				                ));
			
			$user = Sentry::register($credential, false);
			$user->addGroup(Sentry::getGroupProvider()->findByName('Cashier'));
			
			App::flash('message_status', true);
			App::flash('message', "Cashier Successfully Created");
		} catch (\Exception $e ) {
			App::flash('message_status', false);
			App::flash('message', $e -> getMessage());
		}
		Response::redirect($this -> siteUrl("admin/add/cashier"));
	}
}
