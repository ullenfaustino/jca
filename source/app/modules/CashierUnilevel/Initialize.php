<?php

namespace CashierUnilevel;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'CashierUnilevel';
	}

	public function getModuleAccessor() {
		return 'cashierunilevel';
	}

	public function registerCashierRoute() {
		$this -> setSalesRoutes();
	}

	private function setSalesRoutes() {
		Route::resource('/unilevel/sales', 'CashierUnilevel\Controllers\CashierUnilevelSalesController');
		
		Route::get('/unilevel/pos/print/receipt/:control_number/:receipt_no', 'CashierUnilevel\Controllers\CashierUnilevelSalesController:printReceipt');
		Route::get('/unilevel/pos/download/receipt/:control_number/:receipt_no', 'CashierUnilevel\Controllers\CashierUnilevelSalesController:downloadReceiptPOS_PDF');

		Route::post('/unilevel/sales/purchase', 'CashierUnilevel\Controllers\CashierUnilevelSalesController:purchaseProduct') -> name('cashier_purchaseProduct');
	}

}
