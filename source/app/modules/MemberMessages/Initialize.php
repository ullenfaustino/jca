<?php

namespace MemberMessages;

use \App;
use \Menu;
use \Route;
use \Sentry;
use \Messages;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'MemberMessages';
	}
	public function getModuleAccessor() {
		return 'membermessages';
	}
	// public function registerMemberMenu() {
		// $user = Sentry::getUser();
		// $adminMenu = Menu::get ( 'member_sidebar' );
// 		
		// // Create Account Details Menu
		// $accountMenu = $adminMenu->createItem ( 'membermessages', array (
				// 'label' => 'Messages',
				// 'icon' => 'pe-7s-mail',
				// 'url' => 'member/messages' ,
				// 'notif_count' => $this -> countUnreadMessage()
		// ) );
		// // add Parent Menu to Navigation Menu
		// $adminMenu->addItem ( 'membermessages', $accountMenu );
	// }
	public function registerMemberRoute() {
		Route::resource ( '/messages', 'MemberMessages\Controllers\MemberMessagesController' );
		
		Route::post('/send/message', 'MemberMessages\Controllers\MemberMessagesController:sendMessage') -> name('send_message');
		
		Route::get('/thread/replies/:msg_ref_id', 'MemberMessages\Controllers\MemberMessagesController:msgrepliesview');
	}
	
	private function countUnreadMessage() {
		$user = Sentry::getUser();
		return Messages::where('to_ref_id', '=', $user -> id) 
						-> where('last_sender_id', '<>', $user -> id) 
						-> where('is_read','=',0) -> count();
	}
}
