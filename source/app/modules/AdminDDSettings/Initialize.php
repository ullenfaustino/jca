<?php

namespace AdminDDSettings;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'AdminDDSettings';
    }

    public function getModuleAccessor(){
        return 'adminsettings';
    }

    public function registerAdminRoute(){
        Route::resource('/settings', 'AdminDDSettings\Controllers\AdminSettingsController');
        
        Route::post('/settings/set/wallet', 'AdminDDSettings\Controllers\AdminSettingsController:setAdminWallet')->name('set_admin_wallet');
        Route::post('/settings/set/socailmedia', 'AdminDDSettings\Controllers\AdminSettingsController:setSocialMedia')->name('set_admin_socialmedia');
        Route::post('/settings/set/login', 'AdminDDSettings\Controllers\AdminSettingsController:setLoginPage')->name('set_admin_login');
        Route::post('/settings/set/login/background', 'AdminDDSettings\Controllers\AdminSettingsController:setLoginBackground')->name('set_admin_login_background');
        Route::post('/settings/set/login/gallery', 'AdminDDSettings\Controllers\AdminSettingsController:setLoginGallery')->name('set_admin_login_gallery');
        Route::post('/settings/set/donations', 'AdminDDSettings\Controllers\AdminSettingsController:setDonations')->name('set_admin_donations');
    	
    	Route::post('/settings/reset/password', 'AdminDDSettings\Controllers\AdminSettingsController:resetPassword')->name('reset_password');
    	Route::post('/settings/general', 'AdminDDSettings\Controllers\AdminSettingsController:setGeneralSettings')->name('set_general_settings');
	}
}
