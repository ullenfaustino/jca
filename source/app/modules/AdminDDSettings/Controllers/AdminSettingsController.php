<?php

namespace AdminDDSettings\Controllers;

use \User;
use \Users;
use \raw;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \GeneralSettings;
use \AdminControl;

class AdminSettingsController extends BaseController {
	
	public function __construct() {
		parent::__construct();
		$this -> data["active_menu"] = "admin_settings";
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'SETTINGS';
		$this -> data['user'] = $user;

		// login attribute
		$loginAttr = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
		if ($loginAttr) {
			$this -> data['login_attribute'] = json_decode($loginAttr -> content);
		}
		
		$payout_status = AdminControl::where("name","=","member_payout") -> first();
		$this -> data["payout_enabled"] = ($payout_status) ? (($payout_status -> value == 1) ? "checked" : "") : "";
		
		$registration_status = AdminControl::where("name","=","member_registration") -> first();
		$this -> data["registration_enabled"] = ($registration_status) ? (($registration_status -> value == 1) ? "checked" : "") : "";
		
		 /** load the user.js app */
        $this->loadCss('summernote/summernote.css');
        $this->loadJs('summernote/summernote.min.js');
		
		View::display('@adminsettings/index.twig', $this -> data);
	}

	public function setLoginPage() {
		$disclaimer = Input::post('disclaimer');
		$dealersPolicy = Input::post('dealers_policy');
		$productPolicy = Input::post('product_policy');
		$privacy_security = Input::post('privacy_security');
		$company_policy = Input::post('company_policy');
		$terms_condition = Input::post('terms_condition');
		$announcements = Input::post('announcements');

		$content['disclaimer'] = $disclaimer;
		$content['dealers_policy'] = $dealersPolicy;
		$content['product_policy'] = $productPolicy;
		$content['privacy_security'] = $privacy_security;
		$content['company_policy'] = $company_policy;
		$content['terms_condition'] = $terms_condition;
		$content['announcements'] = $announcements;

		$loginAttribue = GeneralSettings::where('module_name', '=', 'login_attribute') -> first();
		if (!$loginAttribue) {
			$loginAttribue = new GeneralSettings();
			$loginAttribue -> module_name = "login_attribute";
		}
		$loginAttribue -> content = json_encode($content, JSON_UNESCAPED_UNICODE);
		$loginAttribue -> save();

		App::flash('message_status', true);
		App::flash('message', "Login Attribute successfully saved");
		Response::redirect($this -> siteUrl('admin/settings'));
	}
	
	public function setGeneralSettings() {
		$payout_switch = Input::post("payout_switch");
		$payout_switch = ($payout_switch === "on") ? 1 : 0;
		$registration_switch = Input::post("registration_switch");
		$registration_switch = ($registration_switch === "on") ? 1 : 0;
		
		$payout_status = AdminControl::where("name","=","member_payout") -> first();
		$payout_status -> value = $payout_switch;
		$payout_status -> save();
		
		$registration_status = AdminControl::where("name","=","member_registration") -> first();
		$registration_status -> value = $registration_switch;
		$registration_status -> save();
		
		App::flash('message_status', true);
		App::flash('message', "Settings successfully saved.");
		Response::redirect($this -> siteUrl('admin/settings'));
	}
}
