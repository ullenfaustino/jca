<?php

namespace DDGenerateCodes\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \Codes;
use \Constants;
use \CodeGenerator;
use \CodesTransaction;
use \CodesTransactionHasCodes;
use \GenericHelper;
use \Receipt;

include __DIR__ . "/../../../lib/Classes/PHPExcel/IOFactory.php";

class GenerateCodesController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = 'admin_generatecode';
    }

    /**
     * display list of resource
     */
    public function index() {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Generate Codes';
        $this -> data['users'] = User::groupByCreatedAt($user -> id) -> get() -> toArray();
        $this -> data['user'] = $user;

        $this -> data['entry_payin'] = Constants::getEntryPayin();
        $this -> data['transactions'] = CodesTransactionHasCodes::userTransactions() -> get() -> toArray();

        /** render the template */
        View::display('@generatecodes/index.twig', $this -> data);
    }

    public function viewGeneratedCodes($transaction_id) {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Generated Codes';
        $this -> data['user'] = $user;

        $this -> data['unused_generated_codes'] = CodesTransactionHasCodes::leftJoin('codes_transaction as CT', 'CT.id', '=', 'codes_transaction_has_codes.codes_transaction_id') -> leftJoin('codes as C', 'C.id', '=', 'codes_transaction_has_codes.codes_id') -> leftJoin('users as User', 'User.id', '=', 'C.user_id') -> where('C.is_used', '=', 0) -> where('codes_transaction_has_codes.codes_transaction_id', '=', $transaction_id) -> select(array('C.id as id', 'C.generated_code as code', 'User.username as user_username', 'CT.account_type as account_type', 'C.created_at')) -> get();

        $this -> data['used_generated_codes'] = CodesTransactionHasCodes::leftJoin('codes_transaction as CT', 'CT.id', '=', 'codes_transaction_has_codes.codes_transaction_id') -> leftJoin('codes as C', 'C.id', '=', 'codes_transaction_has_codes.codes_id') 
                                                -> leftJoin('users as User', 'User.id', '=', 'C.user_id') 
                                                -> where('C.is_used', '=', 1) 
                                                -> where('codes_transaction_has_codes.codes_transaction_id', '=', $transaction_id) 
                                                -> select(array('C.id as id', 
                                                                'C.generated_code as code', 
                                                                'User.username as user_username', 
                                                                'CT.account_type as account_type',
                                                                'C.created_at')) 
                                                -> get();

        /** render the template */
        View::display('@generatecodes/generated_codes.twig', $this -> data);
    }

    public function generateCodes() {
        try {
            $codeCount = Input::post('no_of_codes');

            $codeGenerator = new CodeGenerator();

            $issuer = Sentry::getUser();

            $codeTransaction = new CodesTransaction();
            $codeTransaction -> generated_by_id = $issuer -> id;
            $codeTransaction -> issued_to_id = $issuer -> id;

            if ($codeTransaction -> save()) {
                for ($i = 0; $i < $codeCount; $i++) {
                    $token = $codeGenerator -> getCodes();

                    $code = new Codes();
                    $code -> generated_code = $token;
                    $code -> user_id = 0;

                    if ($code -> save()) {
                        $transactionCodes = new CodesTransactionHasCodes();
                        $transactionCodes -> codes_transaction_id = $codeTransaction -> id;
                        $transactionCodes -> codes_id = $code -> id;
                        $transactionCodes -> save();
                    }
                }
            }
            App::flash('message_status', true);
            App::flash('message', "Code successfully generated!");
        } catch(UserNotFoundException $e) {
            App::flash('message_status', false);
            App::flash('message', "Username does not exist!");
        } catch(\Exception $e) {
            App::flash('message_status', false);
            App::flash('message', $e -> getMessage());
        }
        Response::redirect($this -> siteUrl('/') . 'admin/generatecodes');
    }

    public function uploadCSVGenCodes() {
        $file = Input::file('upload_csv');
        $tempName = $file["tmp_name"];
        $filename = $file["name"];

        if (!empty($file)) {
            $allowed = ' ,csv,CSV';
            $extension_allowed = explode(',', $allowed);
            $file_extension = pathinfo($filename, PATHINFO_EXTENSION);

            if (!array_search($file_extension, $extension_allowed)) {
                App::flash('message', "Invalid File Type!" . $filename);
                App::flash('message_status', false);
                Response::redirect($this -> siteUrl('/') . 'admin/generatecodes');
                return;
            }

            $this -> parseCSV($tempName);

            App::flash('message_status', true);
            App::flash('message', "Successfully Saved");
        } else {
            App::flash('message_status', false);
            App::flash('message', "File is Empty");
        }
        Response::redirect($this -> siteUrl('/') . 'admin/generatecodes');
    }

    private function parseCSV($filePath) {
        $inputFileType = 'CSV';
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader -> load($filePath);
        $worksheet = $objPHPExcel -> getActiveSheet() -> toArray(null, true, true, true);

        $issuer = Sentry::getUser();
        $codeTransaction = new CodesTransaction();
        $codeTransaction -> generated_by_id = $issuer -> id;
        // $codeTransaction -> issued_to_id = $recipientUser -> id;
        $codeTransaction -> issued_to_id = $issuer -> id;
        if ($codeTransaction -> save()) {
            for ($x = 2; $x <= count($worksheet); $x++) {
                foreach ($worksheet[$x] as $data) {
                    $code_gen = $worksheet[$x]['A'];
                    $code = Codes::where('generated_code', '=', $code_gen) -> first();
                    if (!$code) {
                        $code = new Codes();
                        $code -> generated_code = $code_gen;
                        if ($code -> save()) {
                            $transactionCodes = new CodesTransactionHasCodes();
                            $transactionCodes -> codes_transaction_id = $codeTransaction -> id;
                            $transactionCodes -> codes_id = $code -> id;
                            $transactionCodes -> save();
                        }
                    }
                }
            }
        }
    }

    public function downloadGeneratedCodesPDF($transaction_id) {
        $source_url = sprintf("%sreports/generated_codes/%s", GenericHelper::baseUrl(), $transaction_id);

        $file_path = __DIR__ . "/../../../../public/assets/downloads/";
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777, TRUE);
        }

        $filename = base64_encode($transaction_id) . ".pdf";
        $pdfFile = sprintf("%s%s.pdf", $file_path, base64_encode($transaction_id));

        $wkhtml_path = __DIR__ . "/../../../lib/wkhtml/bin/";
        $cmd = sprintf("%swkhtmltopdf %s %s", $wkhtml_path, $source_url, $pdfFile);
        echo "wkHTMLtoPDF_COMMAND-> $cmd \n";
        exec($cmd);

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($pdfFile) . "\"");

        readfile($pdfFile);

        unlink($pdfFile);
    }
    
    public function printReceipt($code_id, $receipt_no) {
        $codeGen = new CodeGenerator();
        
        $user = Sentry::getUser();
        $this -> data['title'] = 'Generate Codes';
        $this -> data['user'] = $user;
        
        $code = Codes::find($code_id);
        $this -> data['code'] = $code;
        
        $payin_amount = Constants::where("name","=","entry_amount")->first();
        $this -> data['code_amount'] = $payin_amount -> value;
        
        $receipt = Receipt::where("or_num","=",$receipt_no) 
                        -> where("module_type","=","payin")
                        -> first();
        if (!$receipt) {
            $receipt = new Receipt();
            $receipt -> trans_code = $codeGen -> generateControlNumber(4);
            $receipt -> or_num = $receipt_no;
            $receipt -> ref_id = $code -> id;
            $receipt -> module_type = "payin";
            $receipt -> total_amount = $payin_amount -> value;
            $receipt -> created_by = $user -> id;
            $receipt -> issued_to = $code -> user_id;
            $receipt -> is_issued = 1;
            $receipt -> save();
        }
        
        // receipt data content
        $receipt_data["date_today"] = date("F j, Y");
        $receipt_data["customer_id"] = $code -> user_id;
        $receipt_data["receipt_no"] = $receipt -> or_num;
        $receipt_data["reference_code"] = $receipt -> trans_code;
        $receipt_data["sub_total"] = 0;
        $receipt_data["total"] = $payin_amount -> value;
        $receipt_data["amount_paid"] = $payin_amount -> value;
        $receipt_data["particulars"][] = array("item" => "Activation Code",
                                                "description" => $code -> generated_code,
                                                "unit_cost" => $payin_amount -> value,
                                                "quantity" => 1,
                                                "amount" => $payin_amount -> value);
        $this -> data['receipt_data'] = $receipt_data;
        
        /** render the template */
        View::display('@generatecodes/receipt.twig', $this -> data);
    }
    
    public function downloadReceiptPDF($code_id, $receipt_no) {
        $source_url = sprintf("%sprint/receipt/pdf/%s/%s", GenericHelper::baseUrl(), $code_id, $receipt_no);
        
        $file_path = __DIR__ . "/../../../../public/assets/downloads/";
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777, TRUE);
        }

        $pdfFile = sprintf("%s%s.pdf", $file_path, $receipt_no);

        $wkhtml_path = __DIR__ . "/../../../lib/wkhtml/bin/";
        $cmd = sprintf("%swkhtmltopdf %s %s", $wkhtml_path, $source_url, $pdfFile);
        echo "wkHTMLtoPDF_COMMAND-> $cmd \n";
        exec($cmd);

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($pdfFile) . "\"");

        readfile($pdfFile);

        unlink($pdfFile);
    }

}
