<?php

namespace DDGenerateCodes;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'DDGenerateCodes';
	}
	
	public function getModuleAccessor() {
		return 'generatecodes';
	}
	
	public function registerAdminRoute() {
		Route::resource ( '/generatecodes', 'DDGenerateCodes\Controllers\GenerateCodesController' );
		
		Route::get ( '/generatecodes/list/:user_id', 'DDGenerateCodes\Controllers\GenerateCodesController:viewGeneratedCodes' );
		Route::get ( '/generatecodes/download/:transaction_id', 'DDGenerateCodes\Controllers\GenerateCodesController:downloadGeneratedCodesPDF' );
		Route::get ( '/generatecodes/download/receipt/:code_id/:receipt_no', 'DDGenerateCodes\Controllers\GenerateCodesController:downloadReceiptPDF' );
		Route::get ( '/generatecodes/print/receipt/:code_id/:receipt_no', 'DDGenerateCodes\Controllers\GenerateCodesController:printReceipt' );
		
		Route::post ( '/generatecodes/generateCodes', 'DDGenerateCodes\Controllers\GenerateCodesController:generateCodes' );
		Route::post ( '/generatecodes/upload', 'DDGenerateCodes\Controllers\GenerateCodesController:uploadCSVGenCodes' ) -> name('uploadCSV');
	}
}
