<?php

namespace CashierProfile\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Cashier\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Users;

class CashierProfileController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data["active_menu"] = "cashier_profile";
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'My Profile';
		$this -> data['user'] = $user;

		/** render the template */
		View::display('@cashierprofile/index.twig', $this -> data);
	}

	public function updateProfileDetails() {
		$user = Sentry::getUser();

		$username = Input::post('username');
		$email = Input::post('email');
		$firstname = Input::post('first_name');
		$middlename = Input::post('middle_name');
		$lastname = Input::post('last_name');

		$user = Users::find($user -> id);
		$user -> username = trim($username);
		$user -> email = trim($email);
		$user -> first_name = trim($firstname);
		$user -> middle_name = trim($middlename);
		$user -> last_name = trim($lastname);
		$user -> save();

		App::flash('message_status', true);
		App::flash('message', "Profile Successfully Updated.");
		Response::redirect($this -> siteUrl('cashier/profile'));
	}
	
	public function changePassword() {
		$user = Sentry::getUser();
		$password = Input::post("password");
		
		$user = Sentry::findUserById($user -> id);
		$user -> password = $password;
		$user -> save();
		
		App::flash('message_status', true);
		App::flash('message', "Password Successfully Changed.");
		Response::Redirect($this -> siteUrl('cashier/profile'));
	}
	
	public function uploadAvatar() {
		$user = Sentry::getUser();
		$_USER = Users::find($user -> id);

		$file = Input::file('img_avatar_upload');
		$tempName = $file["tmp_name"];
		$filename = $file["name"];

		$allowed = ' ,png,jpg,JPG,PNG';
		$extension_allowed = explode(',', $allowed);

		$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
		if (!array_search($file_extension, $extension_allowed)) {
			App::flash('message', "Invalid Image File Type!" . $filename);
			App::flash('message_status', false);
			Response::redirect($this -> siteUrl('cashier/profile'));
			return;
		}

		if (!file_exists(AVATAR_PATH)) {
			mkdir(AVATAR_PATH, 0777, TRUE);
		}

		$type = pathinfo($filename, PATHINFO_EXTENSION);
		$img = AVATAR_PATH . '/' . $filename;

		if (move_uploaded_file($tempName, $img)) {
			$_USER -> img_file_name = $filename;
			$_USER -> save();

			App::flash('message_status', true);
			App::flash('message', "Avatar Successfully Updated.");
			Response::redirect($this -> siteUrl('cashier/profile'));
		} else {
			App::flash('message_status', false);
			App::flash('message', "Unable to Update Avatar");
			Response::redirect($this -> siteUrl('cashier/profile'));
		}
	}

}
