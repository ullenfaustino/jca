<?php

namespace CashierProfile;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'CashierProfile';
	}
	
	public function getModuleAccessor() {
		return 'cashierprofile';
	}
	
	public function registerCashierMenu() {
		$cashierMenu = Menu::get ( 'cashier_sidebar' );
		
		$cashier = $cashierMenu->createItem ( 'cashier_profile', array (
				'label' => 'Profile',
				'icon' => 'user',
				'url' => 'cashier/profile' 
		));
		
		$cashierMenu->addItem ( 'cashier_profile', $cashier);
	}
	
	public function registerCashierRoute() {
		Route::resource ( '/profile', 'CashierProfile\Controllers\CashierProfileController' );
		
		Route::post ( '/profile/update', 'CashierProfile\Controllers\CashierProfileController:updateProfileDetails' ) -> name('cashier_update_profile');
		Route::post ( '/profile/avatar/upload', 'CashierProfile\Controllers\CashierProfileController:uploadAvatar' ) -> name('cashier_upload_avatar');
		Route::post ( '/profile/password/change', 'CashierProfile\Controllers\CashierProfileController:changePassword' ) -> name('cashier_change_password');
	}
}
