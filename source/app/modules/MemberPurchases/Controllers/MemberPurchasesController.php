<?php

namespace MemberPurchases\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \UnilevelPurchaseLogs;
use \PMLogs;

class MemberPurchasesController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = "member_purchases";
	}
	
	/**
	 * display list of resource
	 */
	public function index($page = 1) {
		$user = Sentry::getUser();

		$this -> data['title'] = 'Purchase History';
		$this -> data['user'] = $user;
		
		$this -> data['purchase_history'] = UnilevelPurchaseLogs::join('unilevel_products as UP','UP.product_code','=','unilevel_purchase_logs.product_code')
													-> select(array('*','unilevel_purchase_logs.created_at as created_at'))
													-> where('member_id','=',$user->id) -> get();
		
		$this -> data["maintenances"] = PMLogs::where("user_id","=",$user->id) -> get();
		
		View::display('@memberpurchases/index.twig', $this -> data);
	}
	
}
