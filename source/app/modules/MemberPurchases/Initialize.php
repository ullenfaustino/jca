<?php

namespace MemberPurchases;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'MemberPurchases';
	}

	public function getModuleAccessor() {
		return 'memberpurchases';
	}

	// public function registerMemberMenu() {
	// $user = Sentry::getUser();
	// $adminMenu = Menu::get ( 'member_sidebar' );
	//
	// // Create Account Details Menu
	// $incomeMenu = $adminMenu->createItem ( 'memberpurchases', array (
	// 'label' => 'Purchase History',
	// 'icon' => 'shopping-cart',
	// 'url' => 'member/purchases'
	// ) );
	// $adminMenu->addItem ( 'memberpurchases', $incomeMenu );
	// }
	public function registerMemberRoute() {
		Route::resource('/purchases', 'MemberPurchases\Controllers\MemberPurchasesController');
	}

}
