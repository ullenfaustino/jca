<?php

namespace MemberDownlines\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \BonusManager;
use \DirectReferrals;
use \HierarchySiblings;
use \UsersHasHierarchySiblings;
use \GenericHelper;

class MemberDownlinesController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'member_downlines';
	}
	
	/**
	 * display list of resource
	 */
	public function index($page = 1) {
		$user = Sentry::getUser();

		$this -> data['title'] = 'Downlines Table';
		$this -> data['user'] = $user;
		
		View::display('@memberdownlines/index.twig', $this -> data);
	}
	
	public function getHierarchyMembersPaginated() {
		$userId = Input::post ( "user_id" );
		$limit = Input::post ( "length" );
		$start = Input::post("start");
		$searchValue = trim(Input::post("search")['value'],'"');
		$order = Input::post("order")[0]['column'];
		$dir = Input::post("order")[0]['dir'];
		
		$leftCount = 0;
		$rightCount = 0;

		$left_members = array();
		$right_members = array();

		$h = UsersHasHierarchySiblings::groupBy('user_id') -> where("user_id", "=", $userId) -> first();
		if ($h) {
			$siblings = UsersHasHierarchySiblings::siblingsPaginated($h -> user_id, $start, $limit) -> get();
			foreach ($siblings as $key => $sibling) {
				if ($sibling -> position == 0) {
					$child = BonusManager::getLineMembersPaginated($sibling -> recruitee_id, $start, $limit);

					array_push($left_members, $sibling -> recruitee_id);
					foreach ($child['members'] as $key => $member) {
						array_push($left_members, $member);
					}
					sort($left_members);
					
					$leftCount++;
					$leftCount += $child['count'];
				} else if ($sibling -> position == 1) {
					$child = BonusManager::getLineMembersPaginated($sibling -> recruitee_id, $start, $limit);

					array_push($right_members, $sibling -> recruitee_id);
					foreach ($child['members'] as $key => $member) {
						array_push($right_members, $member);
					}
					sort($right_members);
					
					$rightCount++;
					$rightCount += $child['count'];
				}

			}
		}
		$heirarchy = array('LEFT' => array('count' => $leftCount, 'members' => $left_members), 'RIGHT' => array('count' => $rightCount, 'members' => $right_members));
		
		$arrData = array();
		$maxCount = max(array($heirarchy['LEFT']['count'],$heirarchy['RIGHT']['count']));
		for ($i=0; $i < $maxCount; $i++) { 
			if (isset($heirarchy['LEFT']['members'][$i])) {
				$u = Users::find($heirarchy['LEFT']['members'][$i]);
				$tmp[] = $u -> username;
				$tmp[] = $this -> getDateEncoded($u -> id);
				$tmp[] = $this -> getDirect($u -> id);
			} else {
				$tmp[] = "";
				$tmp[] = "";
				$tmp[] = "";
			}
			$tmp[] = "";
			if (isset($heirarchy['RIGHT']['members'][$i])) {
				$u = Users::find($heirarchy['RIGHT']['members'][$i]);
				$tmp[] = $u -> username;
				$tmp[] = $this -> getDateEncoded($u -> id);
				$tmp[] = $this -> getDirect($u -> id);
			} else {
				$tmp[] = "";
				$tmp[] = "";
				$tmp[] = "";
			}
			array_push($arrData, $tmp);
			unset($tmp);
		}
		
		$response ['draw'] = Input::post ( "draw" );
		$response ['recordsTotal'] = $maxCount;
		$response ['recordsFiltered'] = $maxCount;
		$response ['data'] = $arrData;
		Response::setBody ( json_encode ( $response ) );
	}
	
	private function getDirect($userId) {
		$direct = DirectReferrals::getDirect($userId)->first();
		return ($direct) ? $direct -> username : "";
	}
	
	private function getDateEncoded($userId) {
		$heirarchy = HierarchySiblings::join('users as User','User.id','=','hierarchy_siblings.recruitee_id')
									-> where('recruitee_id','=',$userId) -> first();
		if ($heirarchy) {
			$created_at = $heirarchy -> created_at;
			$phpdate = strtotime($created_at);
			$mysqldate = date('M d, Y h:i:s a', $phpdate);
			return $mysqldate;
		} else {
			return "";
		}
	}
	
	public function listDownlines() {
		$user = Sentry::getUser();
		$months = GenericHelper::monthList();
		foreach ($months as $key => $month) {
			$heirarchy = BonusManager::getHierarchyCount($user -> id, $month -> month);
			
			$month -> left_count = $heirarchy['LEFT'];
			$month -> right_count = $heirarchy['RIGHT'];
		}
		Response::setBody(json_encode($months));
	}
}
