<?php

namespace MemberDownlines;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
	public function getModuleName() {
		return 'MemberDownlines';
	}

	public function getModuleAccessor() {
		return 'memberdownlines';
	}

	// public function registerMemberMenu() {
	// $user = Sentry::getUser();
	// $adminMenu = Menu::get ( 'member_sidebar' );
	//
	// // Create Account Details Menu
	// $incomeMenu = $adminMenu->createItem ( 'memberdownlines', array (
	// 'label' => 'Downline Report',
	// 'icon' => 'users',
	// 'url' => 'member/downlines'
	// ) );
	// $adminMenu->addItem ( 'memberdownlines', $incomeMenu );
	// }
	
	public function registerMemberRoute() {
		Route::resource('/downlines', 'MemberDownlines\Controllers\MemberDownlinesController');
		Route::post('/downlines/members/paginated', 'MemberDownlines\Controllers\MemberDownlinesController:getHierarchyMembersPaginated') -> name('members_paginated');
	
		Route::get('/graph/downlines/list', 'MemberDownlines\Controllers\MemberDownlinesController:listDownlines') -> name("list_downlines");
	}

}
