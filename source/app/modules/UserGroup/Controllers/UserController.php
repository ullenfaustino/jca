<?php

namespace UserGroup\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \GenericHelper;

class UserController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data['active_menu'] = "admin_members";
    }

    public function index() {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Users List';
        $this -> data['user'] = $user;
        $this -> data['users'] = User::where('id', '<>', $user -> id) -> where("user_type", "=", 3) -> get();

        View::display('@usergroup/user/index.twig', $this -> data);
    }

    public function downloadMemberList() {
        $source_url = sprintf("%sreports/list/members", GenericHelper::baseUrl());

        $file_path = __DIR__ . "/../../../../public/assets/downloads/";
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777, TRUE);
        }

        $filename = md5(time()) . ".pdf";
        $pdfFile = sprintf("%s%s", $file_path, $filename);

        $wkhtml_path = __DIR__ . "/../../../lib/wkhtml/bin/";
        $cmd = sprintf("%swkhtmltopdf -O landscape %s %s", $wkhtml_path, $source_url, $pdfFile);
        echo "wkHTMLtoPDF_COMMAND-> $cmd \n";
        exec($cmd);

        header('Content-Type: application/pdf');
        header(sprintf('Content-disposition: attachment; filename=%s', $filename));

        readfile($pdfFile);

        unlink($pdfFile);
    }

}
