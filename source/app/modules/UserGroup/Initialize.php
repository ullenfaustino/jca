<?php

namespace UserGroup;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'UserGroup';
	}

	public function getModuleAccessor() {
		return 'usergroup';
	}

	public function registerAdminRoute() {
		Route::resource('/user', 'UserGroup\Controllers\UserController');
		
		Route::get('/download/pdf/member/list', 'UserGroup\Controllers\UserController:downloadMemberList');

		Route::post('/user/add/ecl', 'UserGroup\Controllers\UserController:addECL') -> name('add_ecl');
	}

}
