<?php

namespace AdminUnilevel\Controllers;

use \Illuminate\Database\Capsule\Manager as DB;
use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \CodeGenerator;

use \Users;
use \UnilevelProducts;
use \UnilevelPurchaseLogs;
use \DirectReferrals;
use \BonusManager;
use \PMLogs;

class AdminUnilevelSalesController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = "admin_unilevel";
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$randomCode = new CodeGenerator();

		$this -> data['title'] = 'Point Of Sales';
		$this -> data['sub_active_menu'] = "admin_unilevel_sales";
		$this -> data['user'] = $user;

		$allUsers = User::where('user_type', '<>', 1) 
						-> where('user_type', '<>', 2) 
						-> where('is_registered', '=', 1) 
						-> where('id', '<>', $user -> id) 
						-> where('id', '<>', 3) 
						-> get();
		$this -> data['all_users'] = $allUsers;

		$this -> data['purchaseLogs'] = $this -> getCurrentMonthPurchaseLogs();
		$this -> data['products'] = UnilevelProducts::all();
		$this -> data['control_number'] = $randomCode -> generateControlNumber(6);

		/** render the template */
		View::display('@adminunilevel/sales/index.twig', $this -> data);
	}

	public function getCurrentMonthPurchaseLogs() {
		$StartDate = new \DateTime('now');
		$StartDate -> modify('first day of this month');
		$startDay = $StartDate -> format('Y-m-d');

		$EndDate = new \DateTime('now');
		$EndDate -> modify('last day of this month');
		$endDay = $EndDate -> format('Y-m-d');

		return DB::select(sprintf("SELECT * FROM unilevel_purchase_logs WHERE (DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay));
	}

	public function purchaseProduct() {
		$user = Sentry::getUser();

		$CN = Input::post('control_number');
		$user_id = Input::post('user_id');
		$product_id = Input::post('product_id');
		$quantity = Input::post('quantity');
		$paymentType = Input::post('payment_type');
		$pm_code = Input::post("pm_code");
		$quantity = ($paymentType == 3) ? 1 : $quantity;

		$product = UnilevelProducts::find($product_id);
		$member = Users::find($user_id);
		
		if (!$member) {
			App::flash('message_status', false);
			App::flash('message', "Member Not Defined");
			Response::redirect($this -> siteUrl("admin/unilevel/sales"));
			return;
		}

		if (!$product) {
			App::flash('message_status', false);
			App::flash('message', "No Selected Product");
			Response::redirect($this -> siteUrl("admin/unilevel/sales"));
			return;
		}

		$totalPV = $product -> pv * $quantity;
		$totalAmount = ($paymentType == 3) ? ($product -> srp * $quantity) : ($product -> price * $quantity);
		
		if ($paymentType == 2) {
			$GC_balance = BonusManager::getCurrentTotalGC($member -> id);
			if ($totalAmount > $GC_balance) {
				App::flash('message_status', false);
				App::flash('message', "Insufficient Amount of GC");
				Response::redirect($this -> siteUrl("admin/unilevel/sales"));
				return;
			}
		}
		
		if ($paymentType == 3) {
			$pmlog = PMLogs::where("user_id","=",$user_id)
						-> where("verification_code", "=", $pm_code)
						-> first();
			if ($pmlog) {
				if ($pmlog -> is_used == 0) {
					$pmlog -> is_used = 1;
					$pmlog -> save();
				} else {
					App::flash('message_status', false);
					App::flash('message', "Product Maintenance already in used.");
					Response::redirect($this -> siteUrl("admin/unilevel/sales"));
					return;
				}
			} else {
				App::flash('message_status', false);
				App::flash('message', "Invalid Product Maintenance Code");
				Response::redirect($this -> siteUrl("admin/unilevel/sales"));
				return;
			}
		}

		$purchase = new UnilevelPurchaseLogs();
		$purchase -> member_id = $member -> id;
		$purchase -> control_number = $CN;
		$purchase -> product_code = $product -> product_code;
		$purchase -> quantity = $quantity;
		$purchase -> total_amount = $totalAmount;
		$purchase -> total_pv = $totalPV;
		$purchase -> payment_type = $paymentType;
		$purchase -> created_by = $user -> id;
		$purchase -> save();
		
		if ($paymentType == 1) {
			try {
				$unilevel = $this -> unilevels($member -> id);
				foreach ($unilevel as $key => $u) {
					$credit = $u['percentage'] * $totalPV;
					if (!is_null($u['user_id'])) {
						$direct = Users::find($u['user_id']);
						BonusManager::add_UPV_PPV_Points($direct -> id, $member -> id, $credit);
					}
				}
			} catch(\Exception $e) {
				// echo $e -> getMessage();
				throw new \Exception($e -> getMessage());
			}
		}

		App::flash('message_status', true);
		App::flash('message', "Transaction Successfully Completed.");
		Response::redirect($this -> siteUrl("admin/unilevel/sales"));
	}
	
	private function unilevels($member_id) {
		$ulvl[0]['percentage'] = 0.03;
		$ulvl[0]['user_id'] = null;

		$ulvl[1]['percentage'] = 0.03;
		$ulvl[1]['user_id'] = null;

		$ulvl[2]['percentage'] = 0.06;
		$ulvl[2]['user_id'] = null;

		$ulvl[3]['percentage'] = 0.03;
		$ulvl[3]['user_id'] = null;

		$ulvl[4]['percentage'] = 0.03;
		$ulvl[4]['user_id'] = null;

		$ulvl[5]['percentage'] = 0.06;
		$ulvl[5]['user_id'] = null;

		$ulvl[6]['percentage'] = 0.03;
		$ulvl[6]['user_id'] = null;

		$ulvl[7]['percentage'] = 0.03;
		$ulvl[7]['user_id'] = null;

		$ulvl[8]['percentage'] = 0.06;
		$ulvl[8]['user_id'] = null;

		$ulvl[9]['percentage'] = 0.09;
		$ulvl[9]['user_id'] = null;

		// 1st level
		$first_level = DirectReferrals::where('recruitee_id', '=', $member_id) -> first();
		if ($first_level) {
			$ulvl[0]['user_id'] = $first_level -> recruiter_id;

			// 2nd level
			$second_level = DirectReferrals::where('recruitee_id', '=', $first_level -> recruiter_id) -> first();
			if ($second_level) {
				$ulvl[1]['user_id'] = $second_level -> recruiter_id;

				// 3rd level
				$third_level = DirectReferrals::where('recruitee_id', '=', $second_level -> recruiter_id) -> first();
				if ($third_level) {
					$ulvl[2]['user_id'] = $third_level -> recruiter_id;

					// 4th level
					$fourth_level = DirectReferrals::where('recruitee_id', '=', $third_level -> recruiter_id) -> first();
					if ($fourth_level) {
						$ulvl[3]['user_id'] = $fourth_level -> recruiter_id;

						// 5th level
						$fifth_level = DirectReferrals::where('recruitee_id', '=', $fourth_level -> recruiter_id) -> first();
						if ($fifth_level) {
							$ulvl[4]['user_id'] = $fifth_level -> recruiter_id;

							//6th level
							$sixth_level = DirectReferrals::where('recruitee_id', '=', $fifth_level -> recruiter_id) -> first();
							if ($sixth_level) {
								$ulvl[5]['user_id'] = $sixth_level -> recruiter_id;

								//7th level
								$seventh_level = DirectReferrals::where('recruitee_id', '=', $sixth_level -> recruiter_id) -> first();
								if ($seventh_level) {
									$ulvl[6]['user_id'] = $seventh_level -> recruiter_id;

									//8th level
									$eight_level = DirectReferrals::where('recruitee_id', '=', $seventh_level -> recruiter_id) -> first();
									if ($eight_level) {
										$ulvl[7]['user_id'] = $eight_level -> recruiter_id;

										//9th level
										$nineth_level = DirectReferrals::where('recruitee_id', '=', $eight_level -> recruiter_id) -> first();
										if ($nineth_level) {
											$ulvl[8]['user_id'] = $nineth_level -> recruiter_id;

											//10th level
											$tenth_level = DirectReferrals::where('recruitee_id', '=', $nineth_level -> recruiter_id) -> first();
											if ($tenth_level) {
												$ulvl[9]['user_id'] = $tenth_level -> recruiter_id;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $ulvl;
	}
	
}
