<?php

namespace AdminUnilevel\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \UnilevelProducts;

class AdminUnilevelProductsController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = "admin_unilevel";
	}

	/**
	 * display list of resource
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['sub_active_menu'] = "admin_unilevel_products";
		$this -> data['title'] = 'Products';
		$this -> data['user'] = $user;

		$this -> data['products'] = UnilevelProducts::all();
		
		 /** load the user.js app */
        $this->loadCss('summernote/summernote.css');
        $this->loadJs('summernote/summernote.min.js');
		
		/** render the template */
		View::display('@adminunilevel/products/index.twig', $this -> data);
	}

	public function addProduct() {
		$product_codeA = Input::post('product_code_A');
		$product_codeB = Input::post('product_code_B');
		$product_code = sprintf("%s-%s", $product_codeA, $product_codeB);
		$product_name = Input::post('product_name');
		$product_description = Input::post('product_description');
		$introduction = Input::post('introduction');
		$packing = Input::post('packing');
		$price = Input::post('price');
		$srp = Input::post('srp');
		$pv = Input::post('pv');
		// $upv = Input::post('upv');

		$file = Input::file('item_image');
		$tempName = $file["tmp_name"];
		$filename = $file["name"];

		$item_image = null;
		if (!empty($file)) {
			$allowed = ' ,png,jpg,JPG,PNG';
			$extension_allowed = explode(',', $allowed);
			$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
			if (!array_search($file_extension, $extension_allowed)) {
				App::flash('message', "Invalid Image File Type!" . $filename);
				App::flash('message_status', false);
				Response::redirect($this -> siteUrl('admin/unilevel/products'));
				return;
			}

			if (!file_exists(PRODUCT_IMG_PATH)) {
				mkdir(PRODUCT_IMG_PATH, 0777, TRUE);
			}

			$type = pathinfo($filename, PATHINFO_EXTENSION);
			$img = PRODUCT_IMG_PATH . '/' . $filename;

			if (move_uploaded_file($tempName, $img)) {
				$item_image = $filename;
			}
		}

		$product = UnilevelProducts::where('product_code', '=', $product_code) -> first();
		if (!$product) {
			$product = new UnilevelProducts();
			$product -> product_code = $product_code;
			$product -> product_name = $product_name;
			$product -> product_description = $product_description;
			$product -> introduction = $introduction;
			$product -> packing = $packing;
			$product -> price = $price;
			$product -> srp = $srp;
			$product -> pv = $pv;
			// $product -> upv = $upv;
			$product -> image_filename = $item_image;
			$product -> save();

			App::flash('message_status', true);
			App::flash('message', 'Product successfully saved.');
		} else {
			App::flash('message_status', false);
			App::flash('message', 'Product already exists.');
		}
		Response::redirect($this -> siteUrl('admin/unilevel/products'));
	}

	public function editProduct() {
		$product_id = Input::post('product_id');
		$product_name = Input::post('product_name');
		$product_description = Input::post('product_description');
		$introduction = Input::post('introduction');
		$packing = Input::post('packing');
		$price = Input::post('price');
		$srp = Input::post('srp');
		$pv = Input::post('pv');
		// $upv = Input::post('upv');

		$file = Input::file('item_image');
		$tempName = $file["tmp_name"];
		$filename = $file["name"];

		$item_image = null;
		if (!empty($file)) {
			$allowed = ' ,png,jpg,JPG,PNG';
			$extension_allowed = explode(',', $allowed);
			$file_extension = pathinfo($filename, PATHINFO_EXTENSION);
			if (!array_search($file_extension, $extension_allowed)) {
				App::flash('message', "Invalid Image File Type!" . $filename);
				App::flash('message_status', false);
				Response::redirect($this -> siteUrl('admin/unilevel/products'));
				return;
			}

			if (!file_exists(PRODUCT_IMG_PATH)) {
				mkdir(PRODUCT_IMG_PATH, 0777, TRUE);
			}

			$type = pathinfo($filename, PATHINFO_EXTENSION);
			$img = PRODUCT_IMG_PATH . '/' . $filename;

			if (move_uploaded_file($tempName, $img)) {
				$item_image = $filename;
			}
		}

		$product = UnilevelProducts::find($product_id);
		if ($product) {
			$product -> product_name = $product_name;
			$product -> product_description = $product_description;
			$product -> introduction = $introduction;
			$product -> packing = $packing;
			$product -> price = $price;
			$product -> srp = $srp;
			$product -> pv = $pv;
			// $product -> upv = $upv;
			if (!is_null($item_image)) {
				$curr_image = PRODUCT_IMG_PATH . '/' . $product -> image_filename;
				if (file_exists($curr_image) && !is_null($product -> image_filename)) {
					unlink($curr_image);
				}
				$product -> image_filename = $item_image;
			}
			$product -> save();

			App::flash('message_status', true);
			App::flash('message', 'Product successfully updated.');
		} else {
			App::flash('message_status', false);
			App::flash('message', 'Product is Invalid');
		}
		Response::redirect($this -> siteUrl('admin/unilevel/products'));
	}

	public function deleteProduct() {
		$product_id = Input::post('product_id');
		UnilevelProducts::find($product_id) -> delete();

		App::flash('message_status', true);
		App::flash('message', 'Product successfully deleted.');
		Response::redirect($this -> siteUrl('admin/unilevel/products'));
	}

}
