<?php

namespace AdminUnilevel;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminUnilevel';
	}

	public function getModuleAccessor() {
		return 'adminunilevel';
	}

	public function registerAdminRoute() {
		$this -> setProductRoutes();
		// $this -> setSalesRoutes();
		// $this -> setSummaryLogs();
	}

	private function setProductRoutes() {
		Route::resource('/unilevel/products', 'AdminUnilevel\Controllers\AdminUnilevelProductsController');

		Route::post('/unilevel/products/add', 'AdminUnilevel\Controllers\AdminUnilevelProductsController:addProduct') -> name('addProduct');
		Route::post('/unilevel/products/edit', 'AdminUnilevel\Controllers\AdminUnilevelProductsController:editProduct') -> name('editProduct');
		Route::post('/unilevel/products/delete', 'AdminUnilevel\Controllers\AdminUnilevelProductsController:deleteProduct') -> name('deleteProduct');
	}
	
	// private function setSalesRoutes() {
        // Route::resource('/unilevel/sales', 'AdminUnilevel\Controllers\AdminUnilevelSalesController');
// 
        // Route::post('/unilevel/sales/purchase', 'AdminUnilevel\Controllers\AdminUnilevelSalesController:purchaseProduct') -> name('purchaseProduct');
    // }

	// private function setSummaryLogs() {
		// Route::resource('/unilevel/summarylogs', 'AdminUnilevel\Controllers\AdminUnilevelSummarylogsController');
// 
		// Route::get('/unilevel/summarylogs/view/:monthly', 'AdminUnilevel\Controllers\AdminUnilevelSummarylogsController:ViewPurchaseLogsHistory');
	// }

}
