<?php

namespace AdminDashboard\Controllers;

use \raw;
use \App;
use \View;
use \Menu;
use \User;
use \Users;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Codes;
use \PairBonus;
use \DirectReferrals;
use \GenericHelper;
use \BonusManager;
use \Constants;
use \MemberPayoutRequests;

class AdminDashboardController extends BaseController {
	
	public function __construct() {
		parent::__construct();
		$this -> data["active_menu"] = "admin_dashboard";
		$a = [];
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['user'] = $user;
		
		$this -> data["months"] = GenericHelper::monthList();
		$this -> data["now"] = date("m");
		
		$this -> stats();
		
		View::display('@admindashboard/index.twig', $this -> data);
	}
	
	private function stats() {
		$this -> data["generated_codes"] = Codes::all() -> count();
		$this -> data["unused_codes"] = Codes::where("is_used","=",0)->count();
		$this -> data["used_codes"] = Codes::where("is_used","=",1)->count();
		
		// total payins
		$payin = Constants::where('name', '=', 'entry_amount') -> first();
		$payin_amount = 0;
		if ($payin) {
			$payin_amount = $payin -> value;
		}
		$this -> data["total_payins"] = $payin_amount * $this -> data["used_codes"];
		
		// total payouts
		$pairing_payouts = PairBonus::where("payout_status","=",2) -> sum("amount");
		$dr_payouts = DirectReferrals::where("payout_status","=",2) -> sum("amount");
		$this -> data["total_payouts"] = $pairing_payouts + $dr_payouts;
		
		$this -> data["total_amount_generated_codes"] = $payin_amount * $this -> data["generated_codes"];
		
		$pending_pair_bonus = PairBonus::where("is_paired","=",1)
										-> where("is_valid","=",1)
										-> where("is_gc","=",0)
										-> where("payout_status","=",0)
										-> sum("amount");
		$pending_dr_bonus = DirectReferrals::where("payout_status","=",0)
											-> sum("amount");
		$this -> data["pending_payouts"] = $pending_pair_bonus + $pending_dr_bonus;
		
		// total vouchers
		$pending_pair_gc = PairBonus::where("is_paired","=",1)
									-> where("is_valid","=",1)
									-> where("is_gc","=",1)
									-> where("payout_status","=",0)
									-> sum("amount");
		$pending_dr_gc = DirectReferrals::where("payout_status","=",0)
											-> sum("gc");
		$this -> data["pending_gc"] = $pending_pair_gc + $pending_dr_gc;
	}
	
	public function monthlyStats() {
		$payin = Constants::where('name', '=', 'entry_amount') -> first();
		$payin_amount = 0;
		if ($payin) {
			$payin_amount = $payin -> value;
		}
		
		$months = GenericHelper::monthList();
		foreach ($months as $key => $month) {
			// total payins
			$payins = Codes::where("is_used","=",1)
									-> whereRaw(sprintf("MONTH(updated_at) = '%s' and YEAR(updated_at)", $month -> month, date("Y")))
									-> count();
			$month -> total_payins = $payins * $payin_amount;
			
			// total payouts
			$pairBonus = PairBonus::where("payout_status","=",2)
								-> whereRaw(sprintf("MONTH(updated_at) = '%s' and YEAR(updated_at)", $month -> month, date("Y")))
								-> sum("amount");
			$drBonus = DirectReferrals::where("payout_status","=",2)
								-> whereRaw(sprintf("MONTH(updated_at) = '%s' and YEAR(updated_at)", $month -> month, date("Y")))
								-> sum("amount");
			$month -> total_payouts = $pairBonus + $drBonus;
		}
		Response::setBody(json_encode($months));
	}
	
	public function incomeStats($month) {
		$payin = Constants::where('name', '=', 'entry_amount') -> first();
		$payin_amount = 0;
		if ($payin) {
			$payin_amount = $payin -> value;
		}
		$total_payins = 0;
		
		$days = Codes::where("is_used","=",1)
						-> select(new raw("DATE(updated_at) as day"))
						-> whereRaw(sprintf("MONTH(updated_at) = '%s' and YEAR(updated_at) = '%s'", $month, date("Y")))
						-> groupBy("day")
						-> get();
		foreach ($days as $key => $day) {
			// convert day in word
			$fDate = new \DateTime($day -> day);
			$day -> day_word = $fDate -> format('M d, Y');
			// -------------------------------------------
			
			$used_count = Codes::where("is_used","=",1)
						-> whereRaw(sprintf("DATE(updated_at) = '%s'", $day -> day))
						-> count();
			$day -> used_count = $used_count;
			$day -> total_payin = $used_count * $payin_amount;
			
			$total_payins += $day -> total_payin;
		}
		
		$data["total_payins"] = $total_payins;
		$data["graph_data"] = $days;
		Response::setBody(json_encode($data));
	}
	
	public function payoutStats($month) {
		$days = DirectReferrals::where("payout_status","=",2)
						-> select(new raw("DATE(updated_at) as day"))
						-> whereRaw(sprintf("MONTH(updated_at) = '%s' and YEAR(updated_at) = '%s'", $month, date("Y")))
						-> groupBy("day")
						-> get();
						
		foreach ($days as $key => $day) {
			// convert day in word
			$fDate = new \DateTime($day -> day);
			$day -> day_word = $fDate -> format('M d, Y');
			// -------------------------------------------
			
			// pair bonus payout
			$pairBonus = PairBonus::where("payout_status","=",2)
								-> whereRaw(sprintf("DATE(updated_at) = '%s'", $day -> day))
								-> sum("amount");
			$day -> pair_bonus = $pairBonus;
			
			// pair bonus payout
			$drBonus = DirectReferrals::where("payout_status","=",2)
								-> whereRaw(sprintf("DATE(updated_at) = '%s'", $day -> day))
								-> sum("amount");
			$day -> dr_bonus = $drBonus;
		}
					
		Response::setBody(json_encode($days));
	}
}
