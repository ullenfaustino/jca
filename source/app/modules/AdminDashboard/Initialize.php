<?php

namespace AdminDashboard;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'AdminDashboard';
    }

    public function getModuleAccessor(){
        return 'admindashboard';
    }

    public function registerAdminRoute(){
        Route::resource('/dashboard', 'AdminDashboard\Controllers\AdminDashboardController');
        
        Route::get('/dashboard/graph/income_stats/:month', 'AdminDashboard\Controllers\AdminDashboardController:incomeStats');
        Route::get('/dashboard/graph/payouts/:month', 'AdminDashboard\Controllers\AdminDashboardController:payoutStats');
        Route::get('/dashboard/graph/stats', 'AdminDashboard\Controllers\AdminDashboardController:monthlyStats') -> name("admin_stats");
    }
}
