<?php

namespace CashierCheckGeneration\Controllers;

use \App;
use \View;
use \Menu;
use \User;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Cashier\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Users;
use \MemberPayoutRequests;
use \Receipt;
use \CodeGenerator;
use \GenericHelper;

class CashierCheckGenerationController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this -> data["active_menu"] = "cashier_check_generation";
    }

    /**
     * display list of resource
     */
    public function index() {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Check Generation';
        $this -> data['user'] = $user;
        $this -> data["now"] = date("Y-m-d");

        $pendingChecks = MemberPayoutRequests::where("status", "=", 1) -> where("check_issued", "=", 0) -> get();
        $this -> data["pending_checks"] = $pendingChecks;
        
        $generatedChecks = MemberPayoutRequests::where("status", "=", 1) -> where("check_issued", "=", 1) -> get();
        $this -> data["generated_checks"] = $generatedChecks;

        /** render the template */
        View::display('@cashiercheckgeneration/index.twig', $this -> data);
    }

    public function updateCheckDetails() {
        $user = Sentry::getUser();

        $trans_code = Input::post("trans_code");
        
        $check_date = Input::post("check_date");
        $check_date = strtr($check_date, '/', '-');
        $check_date = date("Y-m-d", strtotime($check_date));
        
        $check_number = Input::post("check_number");
        $bank_name = Input::post("bank_name");
        $bank_account_no = Input::post("bank_account_no");

        $request = MemberPayoutRequests::where("transaction_number", "=", $trans_code) -> first();
        if ($request) {
            $request -> check_issued = 1;
            $request -> check_issued_by = $user -> id;
            $request -> check_no = $check_number;
            $request -> check_date = $check_date;
            $request -> bank_name = $bank_name;
            $request -> bank_account_no = $bank_account_no;
            $request -> save();
            
            App::flash("message_status", True);
            App::flash("message", "Check Details Successfully Saved.");
        } else {
            App::flash("message_status", false);
            App::flash("message", "Invalid Transaction Number");
        }
        Response::redirect($this -> siteUrl("cashier/check_generation"));
    }
    
    public function printReceipt($trans_no, $receipt_no) {
        $transaction = MemberPayoutRequests::where("transaction_number", "=", $trans_no) -> first();
        
        $codeGen = new CodeGenerator();
        
        $user = Sentry::getUser();
        $this -> data['title'] = 'Print Receipt';
        $this -> data['user'] = $user;
        $this -> data['trans_no'] = $trans_no;
        
        $receipt = Receipt::where("or_num","=",$receipt_no) 
                        -> where("module_type","=","payout")
                        -> first();
        if (!$receipt) {
            $receipt = new Receipt();
            $receipt -> trans_code = $codeGen -> generateControlNumber(4);
            $receipt -> or_num = $receipt_no;
            $receipt -> ref_id = $transaction -> id;
            $receipt -> module_type = "payout";
            $receipt -> total_amount = $transaction -> amount_encash;
            $receipt -> created_by = $user -> id;
            $receipt -> issued_to = $transaction -> user_id;
            $receipt -> is_issued = 1;
            $receipt -> save();
        }
        
        // receipt data content
        $receipt_data["date_today"] = date("F j, Y");
        $receipt_data["customer_id"] = $transaction -> user_id;
        $receipt_data["receipt_no"] = $receipt -> or_num;
        $receipt_data["reference_code"] = $receipt -> trans_code;
        $receipt_data["sub_total"] = 0;
        $receipt_data["total"] = $transaction -> amount_encash;
        $receipt_data["amount_paid"] = $transaction -> amount_encash;
        $receipt_data["particulars"][] = array("item" => "Payout",
                                                "description" => "Bonus Encashment",
                                                "unit_cost" => $transaction -> amount_encash,
                                                "quantity" => 1,
                                                "amount" => $transaction -> amount_encash);
        $this -> data['receipt_data'] = $receipt_data;
        
        /** render the template */
        View::display('@cashiercheckgeneration/receipt.twig', $this -> data);
    }
    
    public function downloadReceiptPayoutPDF($trans_no, $receipt_no) {
        $source_url = sprintf("%sprint/receipt/payout/pdf/%s/%s", GenericHelper::baseUrl(), $trans_no, $receipt_no);
        
        $file_path = __DIR__ . "/../../../../public/assets/downloads/";
        if (!file_exists($file_path)) {
            mkdir($file_path, 0777, TRUE);
        }

        $pdfFile = sprintf("%s%s.pdf", $file_path, $receipt_no);

        $wkhtml_path = __DIR__ . "/../../../lib/wkhtml/bin/";
        $cmd = sprintf("%swkhtmltopdf %s %s", $wkhtml_path, $source_url, $pdfFile);
        echo "wkHTMLtoPDF_COMMAND-> $cmd \n";
        exec($cmd);

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($pdfFile) . "\"");

        readfile($pdfFile);

        unlink($pdfFile);
    }

}
