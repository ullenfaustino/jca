<?php

namespace CashierCheckGeneration;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'CashierCheckGeneration';
	}
	
	public function getModuleAccessor() {
		return 'cashiercheckgeneration';
	}
	
	public function registerCashierRoute() {
		Route::resource ( '/check_generation', 'CashierCheckGeneration\Controllers\CashierCheckGenerationController' );
		
		Route::post ( '/check_generation/update/details', 'CashierCheckGeneration\Controllers\CashierCheckGenerationController:updateCheckDetails' )-> name("update_check_details");
	
        Route::get ( '/check_generation/print/receipt/:trans_no/:receipt_no', 'CashierCheckGeneration\Controllers\CashierCheckGenerationController:printReceipt' );
        Route::get ( '/check_generation/download/receipt/:trans_no/:receipt_no', 'CashierCheckGeneration\Controllers\CashierCheckGenerationController:downloadReceiptPayoutPDF' );
    }
}
