<?php

namespace CashierGeneratedCodes;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer {
		
	public function getModuleName() {
		return 'CashierGeneratedCodes';
	}
	
	public function getModuleAccessor() {
		return 'cashiergeneratedcodes';
	}
	
	// public function registerAdminMenu() {
		// $adminMenu = Menu::get ( 'admin_sidebar' );
		// $member = $adminMenu->createItem ( 'generatecodes', array (
				// 'label' => 'Codes',
				// 'icon' => 'ticket',
				// 'url' => 'admin/generatecodes' 
		// ) );
		// $adminMenu->addItem ( 'generatecodes', $member );
	// }
	
	public function registerCashierRoute() {
		Route::resource ( '/generatecodes', 'CashierGeneratedCodes\Controllers\CashierGeneratedCodesController' );
		
		Route::get ( '/generatecodes/list/:user_id', 'CashierGeneratedCodes\Controllers\CashierGeneratedCodesController:viewGeneratedCodes' );
		Route::get ( '/generatecodes/download/:transaction_id', 'CashierGeneratedCodes\Controllers\CashierGeneratedCodesController:downloadGeneratedCodesPDF' );
		Route::get ( '/generatecodes/download/receipt/:code_id/:receipt_no', 'CashierGeneratedCodes\Controllers\CashierGeneratedCodesController:downloadReceiptPDF' );
		Route::get ( '/generatecodes/print/receipt/:code_id/:receipt_no', 'CashierGeneratedCodes\Controllers\CashierGeneratedCodesController:printReceipt' );
		
		Route::post ( '/generatecodes/generateCodes', 'CashierGeneratedCodes\Controllers\CashierGeneratedCodesController:generateCodes' );
	}
}
