<?php
try {
	$app = require '../app/bootstrap/start.php';
	$app -> run();
} catch(\Exception $e) {
	header('Location: /error.html');
}
