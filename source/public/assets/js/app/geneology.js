$(document).ready(function() {

	$('.first').on('click', function(e) {
		var id = $('#userid').val();
		var url = $("#base_url").val();
		window.location = url + "/member/" + id + "/geneology";
	});

	$(".second").click(function(e) {
		e.preventDefault();
		var id = $(e.currentTarget).attr("attr-uid");
		var url = $("#base_url").val();

		var headBan = $(e.currentTarget).attr("attr-head-ban");
		var headUsername = $(e.currentTarget).attr("attr-head");
		var position = $(e.currentTarget).attr("attr-position");

		if (id != "") {
			window.location = url + "/member/" + id + "/geneology";
		} else {
			onModalRegistration(headUsername, headBan, position);
		}
	});

	$('.third').on('click', function(e) {
		e.preventDefault();
		var parent = $(this).parent().parent().parent().find('a');
		if ($(parent).hasClass('active-geneology-container')) {
			var id = $(e.currentTarget).attr("attr-uid");
			var url = $("#base_url").val();

			var headBan = $(e.currentTarget).attr("attr-head-ban");
			var headUsername = $(e.currentTarget).attr("attr-head");
			var position = $(e.currentTarget).attr("attr-position");

			if (id != "") {
				window.location = url + "/member/" + id + "/geneology";
			} else {
				onModalRegistration(headUsername, headBan, position);
			}
		} else {
			// alert("Unable to add in this slot!");
			alertUnable(this);
		}
	});

	$('.fourth').on('click', function(e) {
		e.preventDefault();
		var parent = $(this).parent().parent().parent().find('a');
		if ($(parent).hasClass('active-geneology-container')) {
			var id = $(e.currentTarget).attr("attr-uid");
			var url = $("#base_url").val();

			var headBan = $(e.currentTarget).attr("attr-head-ban");
			var headUsername = $(e.currentTarget).attr("attr-head");
			var position = $(e.currentTarget).attr("attr-position");

			if (id != "") {
				window.location = url + "/member/" + id + "/geneology";
			} else {
				onModalRegistration(headUsername, headBan, position);
			}
		} else {
			// alert("Unable to add in this slot!");
			alertUnable(this);
		}
	});

});

function alertUnable(elem) {
	$.notify("Unable to Add to this Slot, No Head Available", "error");
	// $(elem).notify("Unable to Add to this Slot, No Head Available");
}
