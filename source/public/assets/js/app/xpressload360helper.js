// var Banks = ["BDO Unibank|Banco de Oro Universal Bank (BDO)", "Metropolitan Bank and Trust Company", "Bank of the Philippine Islands (BPI)", "Land Bank of the Philippines", "Philippine National Bank (PNB)", "Development Bank of the Philippines (DBP)", "Chinabank|China Banking Corporation", "Rizal Commercial Banking Corporation (RCBC)", "Security Bank Corporation (Security Bank)", "UnionBank of the Philippines (Union Bank)", "Citibank Philippines|Citibank, N.A. (CITIBANK)", "United Coconut Planters Bank (UCPB)", "Hongkong and Shanghai Banking Corporation", "East West Bank (Philippines)|East West Bank", "Bank of Commerce", "Philtrust Bank]] (Philippine Trust Company", "Asia United Bank (AUB)", "Standard Chartered Bank", "Maybank", "Philippine Bank of Communications (PBCOM)", "Australia and New Zealand Banking Group (ANZ)", "JPMorgan Chase", "The Bank of Tokyo-Mitsubishi UFJ, Ltd.", "Philippine Veterans Bank", "Deutsche Bank AG", "Robinsons Bank Corporation (Robinsons Bank)", "BDO Private Bank (subsidiary of BDO Unibank)", "ING Group", "Mizuho Corporate Bank", "CTBC Bank", "Bank of America, N.A.", "Korea Exchange Bank", "Mega International Commercial Bank", "Bank of China", "Bangkok Bank", "Al-Amanah Islamic Investment Bank of the Philippines"];

$(document).ready(function() {
	try {
		bankdetailsValidation();
		onAccountViewShow();
		onGeneologyViewShow();
		purchaseOrderValidation();
		removePayoutDecimalInput();
		validatePhoneNumberLength();
	} catch(e) {
		console.log(e);
	}
});

function bankdetailsValidation() {
	$('[name=option_bank]').on('change', function() {
		var _val = $(this).val();
		if (_val === "0") {
			$("#bank-bank-name").attr("readonly", false);
			$("#bank-account-name").attr("readonly", false);
			$("#bank-account-number").attr("readonly", false);
			$("#remittance-name").attr("readonly", true);
			$("#remittance-account-name").attr("readonly", true);
			$("#remittance-account-number").attr("readonly", true);
		} else {
			$("#bank-bank-name").attr("readonly", true);
			$("#bank-account-name").attr("readonly", true);
			$("#bank-account-number").attr("readonly", true);
			$("#remittance-name").attr("readonly", false);
			$("#remittance-account-name").attr("readonly", false);
			$("#remittance-account-number").attr("readonly", false);
		}
	});
}

function onAccountViewShow() {
	$('[name=user_accounts]').on('change', function() {
		var _val = $(this).val();
		var url = $("#base_url").val();
		if (_val !== "0") {
			window.location = url + "/member/myaccount/page/" + _val;
		}
	});
}

function onGeneologyViewShow() {
	$('[name=user_geneology_accounts]').on('change', function() {
		var _val = $(this).val();
		var url = $("#base_url").val();
		if (_val !== "0") {
			window.location = url + "/member/geneology/page/" + _val;
		}
	});
}

function purchaseOrderValidation() {
	$('[name=opt_payment]').on('change', function() {
		var _val = $(this).val();
		if (_val === "0") {
			$("#gateway-name").attr("required", false);
			$("#ref-number").attr("required", false);
			$("#img-upload").attr("required", false);
		} else {
			$("#gateway-name").removeAttr("required");
			$("#ref-number").removeAttr("required");
			$("#img-upload").removeAttr("required");

			$('[name=gateway]').on('change', function() {
				var _val = $(this).val();
				if (_val == 0 || _val == 1) {
					$("#bank-remit").attr("required", true);
				} else {
					$("#bank-remit").removeAttr("required");
				}
			});
		}
	});
}

function removePayoutDecimalInput() {
	$("#load_amount").bind('input propertychange', function() {
		var val = this.value;
		var amount = val - (val * 0.05);
		$("#discounted").val(amount);
	});
}

function validatePhoneNumberLength() {
	$('[name=load_to]').bind('input propertychange', function(e) {
		var _val = $(this).val();
		if (_val.length > 7) {
			$(this).val(_val.substring(0, 7));
		}
	});
}

function commaSeparateNumber(val) {
	while (/(\d+)(\d{3})/.test(val.toString())) {
		val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
	}
	return val;
}

function submitForm() {
	$("#btn-submit-form").attr('disabled', true);
	$("#form-submit").submit();
}

function submitForm(form, id) {
	$("#" + id).attr('disabled', true);
	form.submit();
	console.log(">>>>>>>>>>>>>>>>>>>>>> submit button clicked");
}

