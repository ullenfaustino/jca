$(document).ready(function() {
	// validate bank details
	try {
		allowMemberToUpdateBankDetails();
		removeNumericFromInput("register-username");
		removeNumericFromInput("user-matrix-username");
		removeSpacesInReferenceNumber();
//		validateBankDetailFields();
	} catch (err) {
		console.log(err);
	}
});

function removeNumericFromInput(id) {
	$("#" + id).bind('input propertychange', function() {
		var val = this.value;
		val = val.replace(/\d+/g, '');
		this.value = val;
	});
}

function removeSpacesInReferenceNumber() {
	$("#ref-number").bind('input propertychange', function() {
		var val = this.value;
		val = val.replace(/\s/g, "");
		this.value = val;
	});
}

function validateBankDetailFields() {
	var bankNameCnt = $("#user-bank-name").val().length;
	var accountNameCnt = $("#user-account-name").val().length;
	var accountNumberCnt = $("#user-account-number").val().length;

	if (bankNameCnt > 0) {
		$("#user-bank-name").attr('readonly', true);
	}
	if (accountNameCnt > 0) {
		$("#user-account-name").attr('readonly', true);
	}
	if (accountNumberCnt > 0) {
		$("#user-account-number").attr('readonly', true);
	}
}

function allowMemberToUpdateBankDetails() {
	$(".bank-details").click(function(e) {
		var parent = $(this).parent();
		var content = $(parent).find("input");
		$(content).val('');
	});
}
