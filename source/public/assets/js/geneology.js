function alertUnable(elem) {
	// $.notify("Unable to Add to this Slot, No Head Available", "error");
	alert("Unable to Add to this Slot, No Head Available");
	// $(elem).notify("Unable to Add to this Slot, No Head Available");
}

function loadGeneology(user_id, callback, callback2) {
	$.get("/api/geneology/" + user_id, function(data) {
		if (callback2 !== null || callback2 !== undefined) {
			callback2(data);
		}
		console.log(data);

		$(".tree").empty();
		var concat = [];
		concat.push("<ul>");
		concat.push("<li>");
		concat.push('<a href="#" class="active-geneology-container first a" attr-uid="' + data.parent.id + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + data.parent.username + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");

		concat.push("<!-- level 2 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="second left b" attr-position="Left" attr-head="' + data.parent.username + '" attr-head-ban="' + data.parent.ban + '" attr-uid="' + ((data.child_left) ? data.child_left.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left) ? data.child_left.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");

		concat.push("<!-- level 3 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="third left c" attr-position="Left" attr-head="' + ((data.child_left) ? data.child_left.username : null) + '" attr-head-ban="' + ((data.child_left) ? data.child_left.ban : null) + '" attr-uid="' + ((data.child_left00) ? data.child_left00.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left00) ? data.child_left00.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");

		concat.push("<!-- level 4 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth d" attr-position="Left" attr-head="' + ((data.child_left00) ? data.child_left00.username : null) + '" attr-head-ban="' + ((data.child_left00) ? data.child_left00.ban : null) + '" attr-uid="' + ((data.child_left000) ? data.child_left000.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left000) ? data.child_left000.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("<!-- level 4 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth e" attr-position="Right" attr-head="' + ((data.child_left00) ? data.child_left00.username : null) + '" attr-head-ban="' + ((data.child_left00) ? data.child_left00.ban : null) + '" attr-uid="' + ((data.child_left001) ? data.child_left001.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left001) ? data.child_left001.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");

		concat.push("<!-- level 3 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="third right f" attr-position="Right" attr-head="' + ((data.child_left) ? data.child_left.username : null) + '" attr-head-ban="' + ((data.child_left) ? data.child_left.ban : null) + '" attr-uid="' + ((data.child_left01) ? data.child_left01.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left01) ? data.child_left01.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");
		concat.push("<!-- level 4 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth g" attr-position="Left" attr-head="' + ((data.child_left01) ? data.child_left01.username : null) + '" attr-head-ban="' + ((data.child_left01) ? data.child_left01.ban : null) + '" attr-uid="' + ((data.child_left010) ? data.child_left010.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left010) ? data.child_left010.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("<!-- level 4 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth h" attr-position="Right" attr-head="' + ((data.child_left01) ? data.child_left01.username : null) + '" attr-head-ban="' + ((data.child_left01) ? data.child_left01.ban : null) + '" attr-uid="' + ((data.child_left011) ? data.child_left011.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_left011) ? data.child_left011.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");

		concat.push("<!-- level 2 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="second right i" attr-position="Right" attr-head="' + ((data.parent) ? data.parent.username : null) + '" attr-head-ban="' + ((data.parent) ? data.parent.ban : null) + '" attr-uid="' + ((data.child_right) ? data.child_right.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right) ? data.child_right.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");

		concat.push("<!-- level 3 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="third left j" attr-position="Left" attr-head="' + ((data.child_right) ? data.child_right.username : null) + '" attr-head-ban="' + ((data.child_right) ? data.child_right.ban : null) + '" attr-uid="' + ((data.child_right00) ? data.child_right00.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right00) ? data.child_right00.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");

		concat.push("<!-- level 4 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth k" attr-position="Left" attr-head="' + ((data.child_right00) ? data.child_right00.username : null) + '" attr-head-ban="' + ((data.child_right00) ? data.child_right00.ban : null) + '" attr-uid="' + ((data.child_right000) ? data.child_right000.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right000) ? data.child_right000.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("<!-- level 4 Left -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth l" attr-position="Right" attr-head="' + ((data.child_right00) ? data.child_right00.username : null) + '" attr-head-ban="' + ((data.child_right00) ? data.child_right00.ban : null) + '" attr-uid="' + ((data.child_right001) ? data.child_right001.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right001) ? data.child_right001.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");

		concat.push("<!-- level 3 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="third right m" attr-position="Right" attr-head="' + ((data.child_right) ? data.child_right.username : null) + '" attr-head-ban="' + ((data.child_right) ? data.child_right.ban : null) + '" attr-uid="' + ((data.child_right01) ? data.child_right01.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right01) ? data.child_right01.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("<ul>");

		concat.push("<!-- level 4 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth n" attr-position="Left" attr-head="' + ((data.child_right01) ? data.child_right01.username : null) + '" attr-head-ban="' + ((data.child_right01) ? data.child_right01.ban : null) + '" attr-uid="' + ((data.child_right010) ? data.child_right010.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right010) ? data.child_right010.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("<!-- level 4 Right -->");
		concat.push("<li>");
		concat.push('<a href="#" class="fourth o" attr-position="Right" attr-head="' + ((data.child_right01) ? data.child_right01.username : null) + '" attr-head-ban="' + ((data.child_right01) ? data.child_right01.ban : null) + '" attr-uid="' + ((data.child_right011) ? data.child_right011.id : null) + '" data-toggle="tooltip" data-placement="top"> <span class="geneology-label">' + ((data.child_right011) ? data.child_right011.username : "Available") + '</span> <img class="geneology-image"/> </a>');
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");
		concat.push("</ul>");
		concat.push("</li>");
		concat.push("</ul>");

		var tree = $(concat.join(""));
		$(".tree").append(tree);

		getUserDetail(data.parent.id, function(response) {
			$(".a > img").attr("src", "/assets/images/" + response.color);
			$(".a").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
		});
		getUserDetail(((data.child_left) ? data.child_left.id : -1), function(response) {
			$(".b > img").attr("src", "/assets/images/" + response.color);
			$(".b").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".b").addClass((data.child_left) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_left00) ? data.child_left00.id : -1), function(response) {
			$(".c > img").attr("src", "/assets/images/" + response.color);
			$(".c").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".c").addClass((data.child_left00) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_left000) ? data.child_left000.id : -1), function(response) {
			$(".d > img").attr("src", "/assets/images/" + response.color);
			$(".d").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".d").addClass((data.child_left000) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_left001) ? data.child_left001.id : -1), function(response) {
			$(".e > img").attr("src", "/assets/images/" + response.color);
			$(".e").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".e").addClass((data.child_left001) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_left01) ? data.child_left01.id : -1), function(response) {
			$(".f > img").attr("src", "/assets/images/" + response.color);
			$(".f").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".f").addClass((data.child_left01) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_left010) ? data.child_left010.id : -1), function(response) {
			$(".g > img").attr("src", "/assets/images/" + response.color);
			$(".g").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".g").addClass((data.child_left010) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_left011) ? data.child_left011.id : -1), function(response) {
			$(".h > img").attr("src", "/assets/images/" + response.color);
			$(".h").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".h").addClass((data.child_left011) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right) ? data.child_right.id : -1), function(response) {
			$(".i > img").attr("src", "/assets/images/" + response.color);
			$(".i").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".i").addClass((data.child_right) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right00) ? data.child_right00.id : -1), function(response) {
			$(".j > img").attr("src", "/assets/images/" + response.color);
			$(".j").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".j").addClass((data.child_right00) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right000) ? data.child_right000.id : -1), function(response) {
			$(".k > img").attr("src", "/assets/images/" + response.color);
			$(".k").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".k").addClass((data.child_right000) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right001) ? data.child_right001.id : -1), function(response) {
			$(".l > img").attr("src", "/assets/images/" + response.color);
			$(".l").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".l").addClass((data.child_right001) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right01) ? data.child_right01.id : -1), function(response) {
			$(".m > img").attr("src", "/assets/images/" + response.color);
			$(".m").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".m").addClass((data.child_right01) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right010) ? data.child_right010.id : -1), function(response) {
			$(".n > img").attr("src", "/assets/images/" + response.color);
			$(".n").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".n").addClass((data.child_right010) ? "active-geneology-container" : "inactive-geneology-container");
		});
		getUserDetail(((data.child_right011) ? data.child_right011.id : -1), function(response) {
			$(".o > img").attr("src", "/assets/images/" + response.color);
			$(".o").attr("title", (response.dr) ? response.dr.username : "No Direct Referral");
			$(".o").addClass((data.child_right011) ? "active-geneology-container" : "inactive-geneology-container");
		});

		// EVENTS LISTENER
		// $('.first').on('click', function(e) {
		// var id = $('#userid').val();
		// var url = $("#base_url").val();
		// window.location = url + "/member/" + id + "/geneology";
		// });

		$(".second").click(function(e) {
			e.preventDefault();
			var headBan = $(e.currentTarget).attr("attr-head-ban");
			var headUsername = $(e.currentTarget).attr("attr-head");
			var position = $(e.currentTarget).attr("attr-position");
			var uid = $(e.currentTarget).attr("attr-uid");

			callback({
				"head_ref_code" : headBan,
				"head_username" : headUsername,
				"position" : position,
				"uid" : uid
			});
		});

		$('.third').on('click', function(e) {
			e.preventDefault();
			var parent = $(this).parent().parent().parent().find('a');
			if ($(parent).hasClass('active-geneology-container')) {
				var headBan = $(e.currentTarget).attr("attr-head-ban");
				var headUsername = $(e.currentTarget).attr("attr-head");
				var position = $(e.currentTarget).attr("attr-position");
				var uid = $(e.currentTarget).attr("attr-uid");

				callback({
					"head_ref_code" : headBan,
					"head_username" : headUsername,
					"position" : position,
					"uid" : uid
				});
			} else {
				// alert("Unable to add in this slot!");
				var mod_name = $("[name=module_name]").val();
				if (mod_name !== "geneology") {
					alertUnable(this);
				}
			}
		});

		$('.fourth').on('click', function(e) {
			e.preventDefault();
			var parent = $(this).parent().parent().parent().find('a');
			if ($(parent).hasClass('active-geneology-container')) {
				var headBan = $(e.currentTarget).attr("attr-head-ban");
				var headUsername = $(e.currentTarget).attr("attr-head");
				var position = $(e.currentTarget).attr("attr-position");
				var uid = $(e.currentTarget).attr("attr-uid");

				callback({
					"head_ref_code" : headBan,
					"head_username" : headUsername,
					"position" : position,
					"uid" : uid
				});
			} else {
				// alert("Unable to add in this slot!");
				var mod_name = $("[name=module_name]").val();
				if (mod_name !== "geneology") {
					alertUnable(this);
				}
			}
		});
	}).fail(function(error) {
		console.log("[error - geneology]", error);
		$(".tree").empty();
	});
}

function getUserDetail(user_id, callback) {
	$.get("/api/geneology/detail/user/" + user_id, function(response) {
		callback(response);
	}).fail(function(error) {
		console.log("[error - getIconColor]", error);
	});
}