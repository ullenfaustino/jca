<?php
namespace Helper;

use \MemberPayoutRequests;
use \UsersHasHierarchySiblings;
use \CodeGenerator;
use \Codes;
use \DirectReferrals;
use \OverrideHistory;
use \PairBonus;
use \Users;
use \DirectMatchingBonus;
use \Points;
use \Input;
use \Commissions;
use \UnilevelPurchaseLogs;
use \Constants;
use \PMLogs;
use \raw;

define("log_path", __DIR__ . "/pairing_logs.txt");

class BonusManager {
	
	public function index() {
		$a = [];
	}	
	
	public function processLogicTransaction($new_user) {
		$hierarchies = UsersHasHierarchySiblings::groupBy('user_id') -> get();
		foreach ($hierarchies as $key => $h) {
			// echo "\n" . $h -> user_id;

			// set PV to each head
			BonusManager::addPVPoints($h -> user_id, $new_user);

			BonusManager::updateUserPairBonus($h -> user_id);
		}
	}

	public function updateUserPairBonus($userId) {
		$pv_points = 100;

		// pairing bonus
		$pair_bonus = Constants::where('name', '=', 'pairing_bonus') -> first();
		$balToAdd = 0;
		if ($pair_bonus) {
			$balToAdd = $pair_bonus -> value;
		}

		$points = Points::where('user_id', '=', $userId) -> first();
		if ($points) {
			$is_paired = false;
			
			$leftPoints = $points -> left_ppv;
			$rightPoints = $points -> right_ppv;

			
			$heirarchy = BonusManager::getHierarchyMembers($userId);
			if (BonusManager::isPairingValid($userId)) {
				if (($leftPoints >= $pv_points) && ($rightPoints >= $pv_points)) {
					$is_paired = true;
	
					// updates remaining points
					$points -> left_ppv -= $pv_points;
					$points -> right_ppv -= $pv_points;
					$points -> save();
				}
			}

			//check current pair for 5th cycle
			$is_gc = 0;
			$currPairsCount = PairBonus::where('user_id', '=', $userId) 
			                         -> where('is_valid', '=', 1) 
			                         -> where("is_paired", "=", 1) 
			                         -> count() + 1;
			if ($currPairsCount % 5 == 0) {// 5th Cycle
				$is_gc = 1;
			}
			
			if (!BonusManager::isPairingValid($userId)) {
				$points -> left_ppv = 0;
				$points -> right_ppv = 0;
				$points -> save();
			}

			$pairBonus = new PairBonus();
			$pairBonus -> user_id = $userId;
			$pairBonus -> amount = $balToAdd;
			$pairBonus -> used_pv = $pv_points;
			$pairBonus -> is_gc = $is_gc;
			$pairBonus -> is_valid = BonusManager::isPairingValid($userId);
			$pairBonus -> is_paired = ($is_paired) ? 1 : 0;
			$pairBonus -> left_count = $heirarchy['LEFT']['count'];
			$pairBonus -> right_count = $heirarchy['RIGHT']['count'];
			$pairBonus -> save();
		}
	}

	public function addPVPoints($curr_user, $new_user, $pv = 100) {
		if (!BonusManager::isPairingValid($curr_user)) {
			return;
		}
		
		$heirarchy = BonusManager::getHierarchyMembers($curr_user);

		$leftMembers = $heirarchy['LEFT']['members'];
		$rightMembers = $heirarchy['RIGHT']['members'];

		$position = -1;
		if (in_array($new_user, $leftMembers)) {
			$position = 0;
		}
		if (in_array($new_user, $rightMembers)) {
			$position = 1;
		}

		if ($position > -1) {
			$points = Points::where('user_id', '=', $curr_user) -> first();
			if (!$points) {
				$points = new Points();
				$points -> user_id = $curr_user;
			}
			if ($position == 0) {
				$points -> left_ppv += $pv;
			}
			if ($position == 1) {
				$points -> right_ppv += $pv;
			}
			$points -> save();
		}
	}

	public function add_UPV_PPV_Points($curr_user, $new_user, $pv) {
		if (!BonusManager::isPairingValid($curr_user)) {
			return;
		}
		
		$heirarchy = BonusManager::getHierarchyMembersAll($curr_user);

		$leftMembers = $heirarchy['LEFT']['members'];
		$rightMembers = $heirarchy['RIGHT']['members'];

		$position = -1;
		if (in_array($new_user, $leftMembers)) {
			$position = 0;
		}
		if (in_array($new_user, $rightMembers)) {
			$position = 1;
		}

		if ($position > -1) {
			$points = Points::where('user_id', '=', $curr_user) -> first();
			if (!$points) {
				$points = new Points();
				$points -> user_id = $curr_user;
			}
			if ($position == 0) {
				$points -> left_ppv += $pv;
				$points -> left_upv += $pv;
			}
			if ($position == 1) {
				$points -> right_ppv += $pv;
				$points -> right_upv += $pv;
			}
			$points -> save();
		}
		
		BonusManager::updateUserPairBonus($curr_user);
	}

	public function updateDirectReferralBalance($referrer_id, $referral_id) {
		// pairing bonus
		$dr_bonus = Constants::where('name', '=', 'dr_bonus') -> first();
		$balToAdd = 0;
		if ($dr_bonus) {
			$balToAdd = $dr_bonus -> value;
		}

		$dr_gc = Constants::where('name', '=', 'dr_gc') -> first();
		$balToAddGC = 0;
		if ($dr_gc) {
			$balToAddGC = $dr_gc -> value;
		}

		// create referral
		$newDR = new DirectReferrals();
		$newDR -> recruiter_id = $referrer_id;
		$newDR -> recruitee_id = $referral_id;
		$newDR -> amount = $balToAdd;
		$newDR -> gc = $balToAddGC;
		$newDR -> save();
	}

	public function getCurrentEarnings($userId) {
		$totalEarnings = PairBonus::where('user_id', '=', $userId) 
							-> where("is_paired","=",1)
							-> where("is_valid","=",1)
							-> where("is_gc", "=", 0) 
							-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
							-> sum('amount');
		return $totalEarnings;
	}

	public function getCurrent_GC_Earnings($userId) {
		$totalEarnings = PairBonus::where('user_id', '=', $userId) 
							-> where("is_paired","=",1)
							-> where("is_valid","=",1)
							-> where("is_gc", "=", 1) 
							-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
							-> sum('amount');
		return $totalEarnings;
	}

	public function getCurrentDRBonus($userId) {
		$income = DirectReferrals::where('recruiter_id', '=', $userId) 
							-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
							-> sum("amount");
		return $income;
	}

	public function getCurrentDR_GC_Bonus($userId) {
		$income = DirectReferrals::where('recruiter_id', '=', $userId) 
							-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
							-> sum("gc");
		return $income;
	}

	public function getTotalPairingCount($userId) {
		return PairBonus::where('user_id', '=', $userId) 
					-> where('is_paired', '=', 1) 
					-> where("is_valid", "=", 1) 
					-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
					-> count();
	}

	public function getPV_Points($userId) {
		$points_left = Points::where("user_id", "=", $userId) 
							-> whereRaw(sprintf("(DATE(`updated_at`) >= '%s' AND DATE(`updated_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
							-> sum("left_ppv");
		$points_right = Points::where("user_id", "=", $userId) 
							-> whereRaw(sprintf("(DATE(`updated_at`) >= '%s' AND DATE(`updated_at`) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
							-> sum("right_ppv");
		return array("LEFT_PV_POINTS" => $points_left, "RIGHT_PV_POINTS" => $points_right);
	}

	public function isPairingValid($user_id, $limitPerDay = 10) {
		if ((strtolower(date('D', time())) === 'sat') || (strtolower(date('D', time())) === 'sun')) {
			if (strtolower(date('D', time())) === 'sat') {
				$startDate = new \DateTime('now');
				$startDay = $startDate -> format('Y-m-d');
				
				$EndDate = new \DateTime('now');
				$EndDate -> modify('+1 day');
				$endDay = $EndDate -> format('Y-m-d');
			}
			if (strtolower(date('D', time())) === 'sun') {
				$startDate = new \DateTime('now');
				$startDate -> modify('-1 day');
				$startDay = $startDate -> format('Y-m-d');
				
				$EndDate = new \DateTime('now');
				$endDay = $EndDate -> format('Y-m-d');
			}
			$pairBonus = PairBonus::where("user_id", "=", $user_id) 
								-> where("is_paired", "=", 1) 
								-> whereRaw(sprintf("(DATE(created_at) >= '%s' AND DATE(created_at) <= '%s')", $startDay, $endDay)) 
								-> get();
		} else {
			$pairBonus = PairBonus::where("user_id", "=", $user_id) 
								-> where("is_paired", "=", 1) 
								-> whereRaw(sprintf("(DATE(created_at) >= '%s' AND DATE(created_at) <= '%s')", date('Y-m-d'), date('Y-m-d'))) 
								-> get();
		}
		return ((count($pairBonus) + 1) <= $limitPerDay) ? 1 : 0;
	}
	
	public function currentPVMaintenance($user_id) {
		$StartDate = new \DateTime('now');
		$StartDate -> modify('first day of this month');
		$startDay = $StartDate -> format('Y-m-d');

		$EndDate = new \DateTime('now');
		$EndDate -> modify('last day of this month');
		$endDay = $EndDate -> format('Y-m-d');
		
		return UnilevelPurchaseLogs::where("member_id", "=", $user_id)
								-> where("payment_type","=",1)
								-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay))
								-> sum("total_pv");
	}

	public function hasPVMaintenance($user_id) {
		$StartDate = new \DateTime('now');
		$StartDate -> modify('first day of last month');
		$startDay = $StartDate -> format('Y-m-d');

		$EndDate = new \DateTime('now');
		$EndDate -> modify('last day of last month');
		$endDay = $EndDate -> format('Y-m-d');

		$sum_pv = UnilevelPurchaseLogs::where("member_id", "=", $user_id)
								-> where("payment_type","=",1)
								-> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay))
								-> sum("total_pv");
		return ($sum_pv >= 1500);
	}
	
	public function hasCurrentMonthPM($user_id) {
	    $StartDate = new \DateTime('now');
        $StartDate -> modify('first day of last month');
        $startDay = $StartDate -> format('Y-m-d');

        $EndDate = new \DateTime('now');
        $EndDate -> modify('last day of this month');
        $endDay = $EndDate -> format('Y-m-d');
        
        $pm = PMLogs::where("user_id","=",$user_id)
                    -> whereRaw(sprintf("(DATE(`created_at`) >= '%s' AND DATE(`created_at`) <= '%s')", $startDay, $endDay))
                    -> get();
        return (count($pm) > 0);
	}
	
	public function getCurrentTotalGC($userid) {
        $drGC = DirectReferrals::where('recruiter_id', '=', $userid) 
                            -> sum("gc");
        $pairGC = PairBonus::where('user_id','=',$userid)
                        -> where('is_gc','=',1)
                        -> where('is_valid','=',1)
                        -> where('is_paired','=',1)
                        -> sum('amount');
        $totalGC = $drGC + $pairGC;
        
        $purchases = UnilevelPurchaseLogs::where('member_id','=',$userid)
                                        -> where('payment_type','=',2)
                                        -> sum('total_amount');
        return $totalGC - $purchases;
    }
	
	public function getHierarchyMembers($userId) {
		$leftCount = 0;
		$rightCount = 0;

		$left_members = array();
		$right_members = array();

		$h = UsersHasHierarchySiblings::groupBy('user_id') -> where("user_id", "=", $userId) -> first();
		if ($h) {
			$siblings = UsersHasHierarchySiblings::siblings($h -> user_id) -> get();
			foreach ($siblings as $key => $sibling) {
				if ($sibling -> position == 0) {
					$child = BonusManager::getLineMembers($sibling -> recruitee_id);

					$u = Users::find($sibling -> recruitee_id);
					if ($u -> account_type != 3) {
						array_push($left_members, $sibling -> recruitee_id);
						$leftCount++;
					}
					foreach ($child['members'] as $key => $member) {
						array_push($left_members, $member);
					}
					sort($left_members);

					$leftCount += $child['count'];
				} else if ($sibling -> position == 1) {
					$child = BonusManager::getLineMembers($sibling -> recruitee_id);

					$u = Users::find($sibling -> recruitee_id);
					if ($u -> account_type != 3) {
						array_push($right_members, $sibling -> recruitee_id);
						$rightCount++;
					}
					foreach ($child['members'] as $key => $member) {
						array_push($right_members, $member);
					}
					sort($right_members);

					$rightCount += $child['count'];
				}

			}
		}
		return array('LEFT' => array('count' => $leftCount, 'members' => $left_members), 'RIGHT' => array('count' => $rightCount, 'members' => $right_members));
	}

	public static function getLineMembers($userId) {
		$count = 0;
		$members = array();

		$siblings = UsersHasHierarchySiblings::siblings($userId) -> get();
		foreach ($siblings as $key => $sibling) {
			$child = BonusManager::getLineMembers($sibling -> recruitee_id);

			$u = Users::find($sibling -> recruitee_id);
			if ($u -> account_type != 3) {
				array_push($members, $sibling -> recruitee_id);
				$count++;
			}
			foreach ($child['members'] as $key => $member) {
				array_push($members, $member);
			}
			sort($members);

			$count += $child['count'];
		}
		return array('count' => $count, 'members' => $members);
	}

	public function getHierarchyMembersAll($userId) {
		$leftCount = 0;
		$rightCount = 0;

		$left_members = array();
		$right_members = array();

		$h = UsersHasHierarchySiblings::groupBy('user_id') -> where("user_id", "=", $userId) -> first();
		if ($h) {
			$siblings = UsersHasHierarchySiblings::siblings($h -> user_id) -> get();
			foreach ($siblings as $key => $sibling) {
				if ($sibling -> position == 0) {
					$child = BonusManager::getLineMembersAll($sibling -> recruitee_id);

					array_push($left_members, $sibling -> recruitee_id);
					foreach ($child['members'] as $key => $member) {
						array_push($left_members, $member);
					}
					sort($left_members);

					$leftCount++;
					$leftCount += $child['count'];
				} else if ($sibling -> position == 1) {
					$child = BonusManager::getLineMembersAll($sibling -> recruitee_id);

					array_push($right_members, $sibling -> recruitee_id);
					foreach ($child['members'] as $key => $member) {
						array_push($right_members, $member);
					}
					sort($right_members);

					$rightCount++;
					$rightCount += $child['count'];
				}

			}
		}
		return array('LEFT' => array('count' => $leftCount, 'members' => $left_members), 'RIGHT' => array('count' => $rightCount, 'members' => $right_members));
	}

	public function getLineMembersAll($userId) {
		$count = 0;
		$members = array();

		$siblings = UsersHasHierarchySiblings::siblings($userId) -> get();
		foreach ($siblings as $key => $sibling) {
			$child = BonusManager::getLineMembersAll($sibling -> recruitee_id);

			array_push($members, $sibling -> recruitee_id);
			foreach ($child['members'] as $key => $member) {
				array_push($members, $member);
			}
			sort($members);

			$count++;
			$count += $child['count'];
		}
		return array('count' => $count, 'members' => $members);
	}

	public function getLineMembersPaginated($userId, $start, $limit) {
		$count = 0;
		$members = array();

		$siblings = UsersHasHierarchySiblings::siblings($userId) -> get();
		foreach ($siblings as $key => $sibling) {
			$child = BonusManager::getLineMembersPaginated($sibling -> recruitee_id, $start, $limit);

			array_push($members, $sibling -> recruitee_id);
			foreach ($child['members'] as $key => $member) {
				array_push($members, $member);
			}
			sort($members);

			$count++;
			$count += $child['count'];
		}
		return array('count' => $count, 'members' => $members);
	}

	public function getHierarchyCount($userId, $month = null) {
		$leftCount = 0;
		$rightCount = 0;

		$h = UsersHasHierarchySiblings::groupBy('user_id') -> where("user_id", "=", $userId) -> first();
		if ($h) {
			$siblings = UsersHasHierarchySiblings::siblings($h -> user_id, $month) -> get();
			foreach ($siblings as $key => $sibling) {
				if ($sibling -> position == 0) {
					$leftCount++;
					$leftCount += BonusManager::getLineCount($sibling -> recruitee_id, $month);
				} else if ($sibling -> position == 1) {
					$rightCount++;
					$rightCount += BonusManager::getLineCount($sibling -> recruitee_id, $month);
				}
			}
		}
		return array('LEFT' => $leftCount, 'RIGHT' => $rightCount);
	}

	public function getLineCount($userId, $month = null) {
		$count = 0;
		$siblings = UsersHasHierarchySiblings::siblings($userId, $month) -> get();
		foreach ($siblings as $key => $sibling) {
			$count++;
			$count += BonusManager::getLineCount($sibling -> recruitee_id, $month);
		}
		return $count;
	}

}
