<?php

namespace Helper;

use \UsersHasHierarchySiblings;

class CrosslineValidator {
	public static function isCrosslinedRegistration($sponsorId, $headId) {
		if ($sponsorId == $headId) {
			return true;
		}

		$h = UsersHasHierarchySiblings::groupBy('user_id') -> where("user_id", "=", $sponsorId) -> get() -> first();
		if ($h == null) {
			return false;
		}

		$u = CrosslineValidator::getHierarchyUser($h -> user_id, $headId);
		if ($u) {
			return true;
		}
		return false;
	}

	public static function getHierarchyUser($userId, $headId) {
		$siblings = UsersHasHierarchySiblings::siblings($userId) -> get();
		foreach ($siblings as $key => $sibling) {
			$u = CrosslineValidator::getHierarchyUser($sibling -> recruitee_id, $headId);

			if ($u) {
				return true;
			}

			if ($sibling -> recruitee_id == $headId) {
				return true;
			}
		}
		return false;
	}

}
