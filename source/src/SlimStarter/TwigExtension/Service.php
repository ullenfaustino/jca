<?php

namespace SlimStarter\TwigExtension;
use \Slim;

use \Sentry;
use \User;
use \Users;
use \DirectReferrals;
use \UsersHasHierarchySiblings;
use \HierarchySiblings;
use \UserBalance;
use \UnilevelProducts;
use \UnilevelBalance;
use \UnilevelPurchaseLogs;
use \Teams;
use \PairBonus;
use \DirectMatchingBonus;
use \Points;
use \BonusManager;
use \Commissions;
use \GenericHelper;

class Service extends \Twig_Extension
{
    public function getName()
    {
        return 'service';
    }

    public function getFunctions()
    {
        return array(
        	new \Twig_SimpleFunction('decodeBase64', array($this, 'decodeBase64')),
        	new \Twig_SimpleFunction('getUserIconColor', array($this, 'getUserIconColor')),
            new \Twig_SimpleFunction('getUserdetails', array($this, 'getUserdetails')),
        	new \Twig_SimpleFunction('getDirect', array($this, 'getDirect')),
            new \Twig_SimpleFunction('getUsername', array($this, 'username')),
        	new \Twig_SimpleFunction('hasEntry', array($this, 'hasEntry')),
        	new \Twig_SimpleFunction('getAvatarPath', array($this, 'getAvatarPath')),
        	new \Twig_SimpleFunction('weekGroupToBase64', array($this, 'weekGroupToBase64')),     
        	new \Twig_SimpleFunction('isSunday', array($this, 'isSunday')),
        	new \Twig_SimpleFunction('getPosition', array($this, 'getPosition')),
        	new \Twig_SimpleFunction('getProductDetail', array($this, 'getProductDetail')),
        	new \Twig_SimpleFunction('getProduct', array($this, 'getProduct')),
        	new \Twig_SimpleFunction('getPMBbonus', array($this, 'getPMBbonus')),
        	new \Twig_SimpleFunction('getPMBGCbonus', array($this, 'getPMBGCbonus')),
        	new \Twig_SimpleFunction('getTotalDRBonus', array($this, 'getTotalDRBonus')),
        	new \Twig_SimpleFunction('getTotalDR_GC_Bonus', array($this, 'getTotalDR_GC_Bonus')),
        	new \Twig_SimpleFunction('getRemainingPairingIncome', array($this, 'getRemainingPairingIncome')),
        	new \Twig_SimpleFunction('getRemainingDRIncome', array($this, 'getRemainingDRIncome')),
        	new \Twig_SimpleFunction('timeAgo', array($this, 'timeAgo')),
		);
    }
    
    public function timeAgo($date) {
        return GenericHelper::timeAgo($date);
    }
    
    public function getRemainingDRIncome($user_id) {
    	$dr_payout = DirectReferrals::where("recruiter_id","=",$user_id)
    								-> where("payout_status","=",2)
    								-> sum("amount");
    	$total_dr = DirectReferrals::where("recruiter_id", "=", $user_id)
    								-> sum("amount");
    	return $total_dr - $dr_payout;
    }
    
    public function getRemainingPairingIncome($user_id) {
    	$pairing_payout = PairBonus::where("user_id","=",$user_id)
    								-> where("payout_status","=",2)
    								-> sum("amount");
    	$total_income = PairBonus::where('user_id','=',$user_id)
		    					-> where('is_paired','=',1)
		    					-> where('is_valid','=',1)
								-> where('is_gc','=',0)
							 	-> sum('amount');
		return $total_income - $pairing_payout;
    }
    
    public function decodeBase64($str) {
    	return base64_decode($str);
    }
    
    public function getTotalDRBonus($userId) {
    	return DirectReferrals::where("recruiter_id", "=", $userId)
    						-> sum("amount");
    }
    
    public function getTotalDR_GC_Bonus($userId) {
    	return DirectReferrals::where("recruiter_id", "=", $userId)
    						-> sum("gc");
    }
    
    public function getPMBbonus($userId) {
    	return PairBonus::where('user_id','=',$userId)
    					-> where('is_paired','=',1)
    					-> where('is_valid','=',1)
						-> where('is_gc','=',0)
					 	-> sum('amount');
    }
    
    public function getPMBGCbonus($userId) {
    	return PairBonus::where('user_id','=',$userId)
    					-> where('is_paired','=',1)
    					-> where('is_valid','=',1)
    					-> where('is_gc','=',1)
    					-> sum('amount');
    }
    
    public function getUserIconColor($userId) {
    	$user = Users::find($userId);
    	
    	$color = "geneology/black-gene.png";
    	switch ((int) $user -> account_type) {
			case 1:
				$color = "geneology/silver-gene.png";
				break;
			case 2:
				$color = "geneology/gold-gene.png";
				break;
			case 3:
				$color = "geneology/free-gene.png";
				break;
		}
		
		return $color;
    }
    
    public function getProduct($product_code) {
    	$product = UnilevelProducts::where('product_code','=',$product_code) -> first();
    	return ($product) ? $product : new UnilevelProducts();
    }
    
    public function getProductDetail($id) {
    	$product = UnilevelProducts::find($id);
    	return base64_encode(json_encode($product, JSON_UNESCAPED_UNICODE));
    }
	
	public function getPosition($userid) {
		$heirarchy = HierarchySiblings::where('recruitee_id','=',$userid) -> first();
		return ($heirarchy) ? (($heirarchy -> position == 0) ? "Left" : "Right") : "n/a";
	}
	
	public function isSunday() {
		return (strtolower(date('D', time())) === 'sun') ? TRUE : FALSE;
		// return FALSE;
	}
	
	public function weekGroupToBase64($str) {
		$week = explode("~", $str);
		return base64_encode(json_encode($week));
	}
	
	public function getAvatarPath() {
		return AVATAR_PATH;
	}

    public function username($id) {
    	$user = Sentry::findUserById($id);
    	return $user->username;
    }
    
    public function getUserdetails($id) {
    	return Users::find($id);
    }
    
    public function getDirect($id) {
    	return DirectReferrals::getDirect($id)->first();
    }
    
    public function hasEntry($userid, $table) {
    	return UsersHasHierarchySiblings::hasEntry($userid, $table);
    }
    
}
