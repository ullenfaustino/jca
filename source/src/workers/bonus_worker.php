<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class BONUSWORKER extends BaseController {

    function __construct() {}

    public function processManager($user_id, $dr_id) {
    	BonusManager::updateDirectReferralBalance($dr_id, $user_id);
        BonusManager::processLogicTransaction($user_id);
	}

}

$worker = new BONUSWORKER();
$worker -> processManager($argv[1], $argv[2]);
?>