<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class BackupDB extends BaseController {

    function __construct() {
        $current_time = date('d M Y H:i:s');
        $current_hour = date('H', strtotime($current_time));

        echo "[BACKUP_DB][CURRENT_TIME]|--> " . $current_time . "\n";
        echo "[BACKUP_DB][CURRENT_HOUR]|--> " . $current_hour . "\n";
        if ($current_hour < 1) {
            $this -> do_backup();
        }
    }

    public function do_backup() {
        $db_backup_dir = __DIR__ . "/backups/";
        if (!file_exists($db_backup_dir)) {
            mkdir($db_backup_dir, 0777, TRUE);
        }

        $backup_name = str_replace(" ", "_", date('d M Y')) . ".sql";
        $backup_file = $db_backup_dir . $backup_name;

        if (!file_exists($backup_file)) {
            $cmd = sprintf("mysqldump -u root jca > %s", $backup_file);
            exec($cmd);

            echo "[BACKUP_DB][Backup Successfully] \n";
        }
    }

}

new BackupDB();
?>