<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class Worker extends BaseController {

    function __construct() {
        $current_time = date('d M Y H:i:s');
        $current_hour = date('H', strtotime($current_time));

        echo "[CURRENT_TIME]|--> " . $current_time . "\n";
        echo "[CURRENT_HOUR]|--> " . $current_hour . "\n";
        if ($current_hour < 1) {
            $this -> reset_points();
        }
    }

    public function reset_points() {
        if (!$this -> isReset()) {
            //reset tag
            $reset_check = new FlushoutChecker();
            $reset_check -> user_id = -1;
            $reset_check -> is_paired = 0;
            $reset_check -> save();

            $points = Points::all();
            foreach ($points as $key => $point) {
                if ($this -> isPreviouslyFlushedOut($point -> user_id)) {
                    echo "[USER_ID] |-> " . $point -> user_id . "\n";
                    $point -> left_ppv = 0;
                    $point -> right_ppv = 0;
                    $point -> save();
                }
            }
            echo "[Points successfully reset] \n";
        } else {
            echo "[Points was already reset] \n";
        }
    }

    private function isReset() {
        $reset = FlushoutChecker::whereRaw(sprintf("DATE(created_at) = '%s'", date("Y-m-d"))) -> count();
        return ($reset > 0);
    }

    private function isPreviouslyFlushedOut($user_id) {
    	if (strtolower(date('D', time())) === 'sun') {
			return false;
		} else {
			if (strtolower(date('D', time())) === 'mon') {
				$startDate = new \DateTime('now');
				$startDate -> modify('-2 day');
				$startDay = $StartDate -> format('Y-m-d');

				$EndDate = new \DateTime('now');
				$EndDate -> modify('-1 day');
				$endDay = $EndDate -> format('Y-m-d');

				$pairs = PairBonus::where('user_id', '=', $user_id)
		        				-> where('is_paired', '=', 1)
		        				-> where("is_valid", "=", 1)
		        				-> where("payout_status", "=", 0)
		        				-> whereRaw(sprintf("(DATE(created_at) >= '%s' AND DATE(created_at) <= '%s')", $startDay, $endDay))
		        				-> count();
	    	} else {
	    		$now = new \DateTime('now');
		        $now -> modify('-1 day');
		        $kahapon = $now -> format('Y-m-d');

		        $pairs = PairBonus::where('user_id', '=', $user_id)
		        				-> where('is_paired', '=', 1)
		        				-> where("is_valid", "=", 1)
		        				-> where("payout_status", "=", 0)
		        				-> whereRaw(sprintf("DATE(`created_at`) >= '%s'", $kahapon))
		        				-> count();
	    	}
	        return ($pairs >= 10);
		}
    }

}

new Worker();
?>
