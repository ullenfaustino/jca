<?php
include __DIR__ . "/../../app/bootstrap/start.php";

use \Illuminate\Database\Capsule\Manager as DB;

class Emergency extends BaseController {

    var $sablay = [];

    function __construct() {
      // // $qry = DB::select("SELECT DATE(created_at) as created_at FROM `pair_bonus` WHERE user_id=24 and (DATE(created_at) >= '2017-08-26' and DATE(created_at) <= '2017-09-07') GROUP BY DATE(created_at)");
      // $qry = DB::select("SELECT user_id FROM `pair_bonus` WHERE DATE(created_at) >= '2017-01-01' and DATE(created_at) <= '2017-09-07' GROUP BY user_id");
      // foreach (json_decode(json_encode($qry)) as $key => $a) {
      //   // $curr_date = new \DateTime($_date -> created_at);
      //   // $frm_curr_date = $curr_date -> format('D');
      //   //
      //   // if ((strtolower($frm_curr_date) === 'sat') || strtolower($frm_curr_date) === 'sun') {
      //   //   echo $curr_date -> format('Y-m-d') . " -> it is $frm_curr_date boy.... \n";
      //   //   continue;
      //   // }
      //
      //   // $qry_per_day = DB::select(sprintf("SELECT * from pair_bonus where user_id=24 and DATE(created_at) = '%s'", $curr_date -> format('Y-m-d')));
      //   // print_r($qry_per_day);
      //   // break;
      //
      //   $this -> checkErrorEntries($a -> user_id);
      // }
      // // $this -> processFixData();
      //
      // // $this -> checkErrorEntries(5);
      // print_r($this -> sablay);
      $this -> generateReferenceEntries();
    }

    private function generateReferenceEntries() {
      $qry = DB::select("SELECT * FROM `users` WHERE DATE(created_at) >= '2017-08-01'");
      foreach (json_decode(json_encode($qry)) as $key => $user) {
        $dt_created = new \DateTime($user -> created_at);
        echo "==================================================================================================================\n\n";
        echo sprintf("[%s.][%s] [id]=%s - %s - %s - %s - %s(%s) \n\n", ($key + 1), $user -> ban, $user -> id, $user -> first_name . " " . $user -> middle_name . " " . $user -> last_name, $user -> username, (strlen($user -> email) == 0) ? "no email" : $user -> email, $dt_created -> format("Y-m-d"), $dt_created -> format('F j, Y D'));

        $dr = DirectReferrals::where("recruitee_id", "=", $user -> id) -> first();
        if ($dr) {
          $dr_user = Users::find($dr -> recruiter_id);
          echo sprintf("----> [Sponsor:] %s - %s -> %s \n\n", $dr_user -> id, $dr_user -> username, $dr_user -> first_name . " " . $dr_user -> middle_name . " " . $dr_user -> last_name);
        }

        $position = HierarchySiblings::join("users_has_hierarchy_siblings as UHHS", "UHHS.hierarchy_sibling_id", "=", "hierarchy_siblings.id")
                                    -> where("hierarchy_siblings.recruitee_id", "=", $user -> id) -> first();
        if ($position) {
          $pos_user = Users::find($position -> user_id);
          echo sprintf("----> [MATRIX POSITION:] [HEAD ID]: %s - [HEAD USERNAME: ] %s - [POSITION]: %s \n\n", $position -> user_id, $pos_user -> username, ($position -> position == 0) ? "Left" : "Right");
        } else {
          echo "[Not Activated (Pre-Registered)]\n";
        }

        echo "******************************************************************************************************************=\n";
      }
    }

    private function checkErrorEntries($user_id) {
      $qry_init = DB::select("SELECT * FROM `pair_bonus` WHERE user_id=$user_id and (DATE(created_at) >= '2017-01-01' and DATE(created_at) <= '2017-09-07') ORDER BY created_at ASC");

      echo "User ID: " . $user_id . "\n";

      $arr_hey = [];
      foreach (json_decode(json_encode($qry_init)) as $key => $a) {
        if ($a -> left_count == $a -> right_count) {
          array_push($arr_hey, "same");
        } else {
          array_push($arr_hey, "not same");
        }
      }

      // print_r($arr_hey);

      if ($this -> same($arr_hey)) {
        array_push($this -> sablay, $user_id);
      //   echo "pasok lahat";
      // } else {
      //   echo "sablay";
      }
    }

    private function same($arrs) {
      $is_same = true;
      foreach ($arrs as $key => $arr) {
        if ($arr !== "same") {
          $is_same = false;
          break;
        }
      }
      return $is_same;
        // return array_filter($arr, function ($element) use ($arr) {
        //     return ($element === "same");
        // });
    }

    private function processFixData() {
      $qry_init = DB::select("SELECT * FROM `pair_bonus` WHERE user_id=998 and (DATE(created_at) >= '2017-08-25' and DATE(created_at) <= '2017-09-07') ORDER BY created_at ASC");
      // $qry_init = DB::select("SELECT * FROM `pair_bonus` WHERE user_id=24 ORDER BY created_at ASC");
      $previous_l_count = -1;
      $previous_r_count = -1;

      $ctr_paired = 0;
      foreach (json_decode(json_encode($qry_init)) as $key => $a) {
        $curr_date = new \DateTime($a -> created_at);
        $frm_curr_date = $curr_date -> format('D');

        $curr_l_count = $a -> left_count;
        $curr_r_count = $a -> right_count;

        $is_paired = false;

        if ($previous_r_count == -1 || $previous_l_count == -1 && $key != 0) {
          $previous_l_count = $curr_l_count;
          $previous_r_count = $curr_r_count;
          continue;
        }

        if ((strtolower($frm_curr_date) === 'sat') || strtolower($frm_curr_date) === 'sun') {
          echo  $a -> id . " -> it is $frm_curr_date boy.... \n";
          $previous_l_count = $curr_l_count;
          $previous_r_count = $curr_r_count;
          continue;
        }

        if ($curr_l_count == $previous_l_count && $curr_r_count == $previous_r_count) {
          $is_paired = false;
        }

        if ($curr_l_count != $previous_l_count && $curr_r_count != $previous_r_count && $curr_l_count == $curr_r_count) {
          $is_paired = true;
        }

        if ($curr_l_count != $previous_l_count && $curr_r_count == $previous_r_count && $curr_r_count > $curr_l_count) {
          $is_paired = true;
        }

        if ($curr_l_count != $previous_l_count && $curr_r_count == $previous_r_count && $curr_l_count > $curr_r_count) {
          $is_paired = false;
        }

        if ($curr_r_count != $previous_r_count && $curr_l_count == $previous_l_count && $curr_l_count > $curr_r_count) {
          $is_paired = true;
        }

        if ($curr_r_count != $previous_r_count && $curr_l_count == $previous_l_count && $curr_r_count > $curr_l_count) {
          $is_paired = false;
        }

        if ($is_paired) {
          // echo $a -> id . " -> is_paired " . " \n";
          $ctr_paired++;
        }

        $previous_l_count = $curr_l_count;
        $previous_r_count = $curr_r_count;

        // echo $a -> id . " - " . $curr_l_count . " - " . $curr_r_count . " - " . (($is_paired) ? "pares" : "hindi pares") . " \n";
      }
      echo "paired count = " . $ctr_paired;
    }
}

new Emergency();
?>
