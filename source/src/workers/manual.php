<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class Worker extends BaseController {

    var $curr_dt;

    function __construct($current_date) {
        $this -> curr_dt = $current_date;
        $this -> reset_points();
    }

    public function reset_points() {
        if (!$this -> isReset()) {
            //reset tag
            $reset_check = new FlushoutChecker();
            $reset_check -> user_id = -1;
            $reset_check -> is_paired = 0;
            $reset_check -> save();

            $points = Points::all();
            foreach ($points as $key => $point) {
                echo "*********************************************" . "\n";
                if ($this -> isPreviouslyFlushedOut($point -> user_id)) {
                    $point -> left_ppv = 0;
                    $point -> right_ppv = 0;
                    $point -> save();
                }
                echo "[USER_ID] |-> " . $point -> user_id . "\n";
                echo "+++++++++++++++++++++++++++++++++++++++++++++" . "\n";
            }
            echo "[Points successfully reset] \n";
        } else {
            echo "[Points was already reset] \n";
        }
    }

    private function isReset() {
        $reset = FlushoutChecker::whereRaw(sprintf("DATE(created_at) = '%s'", date("Y-m-d"))) -> count();
        return ($reset > 0);
    }

    private function isPreviouslyFlushedOut($user_id) {
      $dt_dtCurr = new \DateTime($this -> curr_dt);
      $str_curr_date = $dt_dtCurr -> format('D');
    	if (strtolower($str_curr_date) === 'sun') {
        echo "Your date set is sunday....." . "\n";
			return false;
		} else {
			if (strtolower($str_curr_date) === 'mon') {
        echo "Your date set is monday....." . "\n";
				$startDate = new \DateTime($this -> curr_dt);
				$startDate -> modify('-2 day');
				$startDay = $StartDate -> format('Y-m-d');

				$EndDate = new \DateTime($this -> curr_dt);
				$EndDate -> modify('-1 day');
				$endDay = $EndDate -> format('Y-m-d');

				$pairs = PairBonus::where('user_id', '=', $user_id)
		        				-> where('is_paired', '=', 1)
		        				-> where("is_valid", "=", 1)
		        				-> where("payout_status", "=", 0)
		        				-> whereRaw(sprintf("(DATE(created_at) >= '%s' AND DATE(created_at) <= '%s')", $startDay, $endDay))
		        				-> count();
	    	} else {
          echo "Your date set is regular day....." . "\n";
	    		$now = new \DateTime($this -> curr_dt);
		        $now -> modify('-1 day');
		        $kahapon = $now -> format('Y-m-d');

		        $pairs = PairBonus::where('user_id', '=', $user_id)
		        				-> where('is_paired', '=', 1)
		        				-> where("is_valid", "=", 1)
		        				-> where("payout_status", "=", 0)
		        				-> whereRaw(sprintf("DATE(`created_at`) >= '%s'", $kahapon))
		        				-> count();
	    	}
        echo "Current pair count --> " . $pairs . "\n";
        echo "isPreviouslyFlushedOut --> " . (($pairs >= 10) ? "true" : "false") . "\n";
	        return ($pairs >= 10);
		}
    }

}

new Worker($argv[1]);
?>
