var cmd = require('node-cmd');

setInterval(function() {
	points_reset();
	backup_db();
}, 30000);

var points_reset = function() {
	var command = "php workers.php";
	cmd.get(command, function(data) {
		console.log("[Command Response]|---> ", data);
	});
};

var backup_db = function() {
	var command = "php backup_db.php";
	cmd.get(command, function(data) {
		console.log("[Command Response]|---> ", data);
	});
};
