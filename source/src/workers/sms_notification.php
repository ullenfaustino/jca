<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class SMSNOTIFICATION extends BaseController {

    function __construct() {}

    public function SendSMS($mobile, $message) {
    	$message = base64_decode($message);
        GenericHelper::sendSMS($mobile, $message);
	}

}

$notif = new SMSNOTIFICATION();
$notif -> SendSMS($argv[1], $argv[2]);
?>