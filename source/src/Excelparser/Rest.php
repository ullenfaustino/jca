<?php
namespace ExcelParser;

ob_start();
class REST {

	function REST() {
		//constructor
	}

	public function delivery_callback($callback, $isArray, $status) {
		if ($isArray) {
			echo json_encode($callback);
		} else {
			$response['status'] = $callback;
			echo json_encode($response);
		}
		header('Content-Type : application/json; charset=UTF-8');
		header('HTTP/1.1 ' . $status . " " . $this -> get_status_message($status), true, $status);
	}

	public function get_status_message($code) {
		$status = array(100 => 'Continue', 101 => 'Switching Protocols', 200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content', 300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not Modified', 305 => 'Use Proxy', 306 => '(Unused)', 307 => 'Temporary Redirect', 400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy Authentication Required', 408 => 'Request Timeout', 409 => 'Conflict', 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Long', 415 => 'Unsupported Media Type', 416 => 'Requested Range Not Satisfiable', 417 => 'Expectation Failed', 500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 => 'Service Unavailable', 504 => 'Gateway Timeout', 505 => 'HTTP Version Not Supported');
		// return ($status[$this->_code])?$status[$this->_code]:$status[500];
		return ($code) ? $status[$code] : $status[304];
	}

}
?>