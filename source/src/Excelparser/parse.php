<?php
ob_start();
header("Content-type : application/json; charset=UTF-8");

require $_SERVER['DOCUMENT_ROOT'] . '/excelparser/Rest.php';
require $_SERVER['DOCUMENT_ROOT'] . '/excelparser/php-excel-reader/excel_reader2.php';
require $_SERVER['DOCUMENT_ROOT'] . '/excelparser/SpreadsheetReader.php';
require $_SERVER['DOCUMENT_ROOT'] . '/excelparser/DBConnect.php';
define("FILE_PATH", $_SERVER['DOCUMENT_ROOT'] . '/excelparser/uploaded_files/');

$target_file = FILE_PATH . basename($_FILES['file']['name']);

$allowed = array("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
if (!in_array($_FILES['file']['type'], $allowed)) {
	$rest = new REST();
	$rest -> delivery_callback("Unable to upload file, wrong file format!.", false, 500);
	return;
}

if (file_exists(FILE_PATH . $_FILES['file']['name'])) { 
	$rest = new REST();
	$rest -> delivery_callback("Sorry, File is already Existing.", false, 500);
	return;
}

if (isset($_POST["submit"])) {
	if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
		parseData($target_file);
	} else {
		$rest = new REST();
		$rest -> delivery_callback("Sorry, there was an error uploading your file.", false, 500);
	}
}

function parseData($file_path) {
	$rest = new REST();
	$db = new DbConnect();
	$db -> connect();
	try {
		$arrData = array();
		$ctr = 0;

		$Spreadsheet = new SpreadsheetReader($file_path);
		$Sheets = $Spreadsheet -> Sheets();

		foreach ($Sheets as $Index => $Name) {
			$Spreadsheet -> ChangeSheet($Index);
			foreach ($Spreadsheet as $Key => $Row) {
				if ($Key != 0) {
					/*
					 *ADD TO ARRAY TO RETURN AS JSON OBJECT
					 **/
					$temp["account_number"] = $Row[0];
					$temp["account_name"] = $Row[1];
					$temp["meter_number"] = $Row[2];
					$temp["present_reading"] = $Row[3];
					$temp["previous_reading"] = $Row[4];
					$temp["average_consumption"] = $Row[5];
					if (isset($Row[6])) {
						$temp["remarks"] = $Row[6];
						$remarks = $Row[6];
					} else {
						$temp["remarks"] = "n/a";
						$remarks = "n/a";
					}

					/*
					 *SAVE TO DB
					 **/
					$account_number = $Row[0];
					$account_name = str_replace("'", "~", $Row[1]);
					$meter_number = $Row[2];

					$qry_find = mysql_query("select * from account where account_no = '$account_number'");
					$numrows = mysql_num_rows($qry_find);
					if ($numrows == 0) {
						$qry = mysql_query("INSERT INTO account (account_no,account_name,meter_no,status,remarks) VALUES ('$account_number','$account_name','$meter_number','$remarks','$remarks')");
						if ($qry) {
							$temp["save_status"] = "Saved To DB!";
						} else {
							$temp["save_status"] = "Failed To Save!";
						}
					} else {
						$qry_update = mysql_query("UPDATE account set account_name='$account_name',meter_no='$meter_number',remarks='$remarks' WHERE account_no='$account_number'");
						if ($qry_update) {
							$temp["save_status"] = "Database Updated!";
						} else {
							$temp["save_status"] = "Failed To Update DB!";
						}
					}

					/*
					 *PUSH TO ARRAY $temp
					 **/
					array_push($arrData, $temp);
				}
			}
		}
		$status["status"] = "Success";
		$status["total"] = (int)count($arrData);
		$status["data"] = $arrData;
		$rest -> delivery_callback($status, true, 200);
	} catch (Exception $E) {
		$rest -> delivery_callback($E -> getMessage(), false, 400);
	}
}
?>