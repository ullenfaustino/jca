<?php

define('APP_PATH'   , __DIR__.'/app/');

$config = array();
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/app/config/database.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Cartalyst\Sentry\Facades\Native\Sentry;
    
class Migrator{

    protected $config;

    public function __construct($config){
        $this->config = $config;
        $this->makeConnection($config);
    }

    /**
     * create connection to the database based on given configuration
     */
    private function makeConnection($config){
        try{
            $capsule = new Capsule;

            $capsule->addConnection($config);
            $capsule->setAsGlobal();
            $capsule->bootEloquent();

            Sentry::setupDatabaseResolver($capsule->connection()->getPdo());

        }catch(Exception $e){
            throw $e;
        }
    }

    /**
     * migrate the database schema
     */
    public function migrate(){
        /**
         * create table for sentry user
         */
        if (!Capsule::schema()->hasTable('users')){
            Capsule::schema()->create('users', function($table)
            {
                $table->increments('id');
                $table->string('email');
                $table->string('password');
                $table->text('permissions')->nullable();
                $table->boolean('activated')->default(0);
                $table->string('activation_code')->nullable();
                $table->timestamp('activated_at')->nullable();
                $table->timestamp('last_login')->nullable();
                $table->string('persist_code')->nullable();
                $table->string('reset_password_code')->nullable();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->timestamps();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->unique('email');
                $table->index('activation_code');
                $table->index('reset_password_code');
            });
        }

        /**
         * create table for sentry group
         */
        if (!Capsule::schema()->hasTable('groups')){
            Capsule::schema()->create('groups', function($table)
            {
                $table->increments('id');
                $table->string('name');
                $table->text('permissions')->nullable();
                $table->timestamps();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->unique('name');
            });
        }

        /**
         * create user-group relation
         */
        if (!Capsule::schema()->hasTable('users_groups')){
            Capsule::schema()->create('users_groups', function($table)
            {
                $table->integer('user_id')->unsigned();
                $table->integer('group_id')->unsigned();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->primary(array('user_id', 'group_id'));
            });
        }

        /**
         * create throttle table
         */
        if (!Capsule::schema()->hasTable('throttle')){
            Capsule::schema()->create('throttle', function($table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('ip_address')->nullable();
                $table->integer('attempts')->default(0);
                $table->boolean('suspended')->default(0);
                $table->boolean('banned')->default(0);
                $table->timestamp('last_attempt_at')->nullable();
                $table->timestamp('suspended_at')->nullable();
                $table->timestamp('banned_at')->nullable();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->index('user_id');
            });
        }
    }

    /**
     * seed the database with initial value
     */
    public function seed(){
    	$this -> seedAdmin();
    	$this -> seedCashier();
    	$this -> seedMember();
    }
    
    private function seedAdmin() {
    	try {
            Sentry::getGroupProvider()->create(array(
                'name'        => 'Administrators',
                'permissions' => array(
                    'admin' => 1,
                ),
            ));
            
            $admin = Sentry::createUser(array(
                'username'    => 'admin',
                'email'       => 'admin@jca.org',
                'password'    => 'password',
                'canonical_hash'    => base64_encode('password'),
                'first_name'  => 'Fname',
                'last_name'   => 'Lname',
                'activated'   => 1,
                'user_type'   => 1,
                'permissions' => array(
                    'admin'   => 1
                )
            ));
            $admin->addGroup( Sentry::getGroupProvider()->findByName('Administrators') );
            
            $adminACS = Sentry::createUser(array(
                'username'    => 'acsolution',
                'email'       => 'acsolution@jca.org',
                'password'    => 'acsolution1234',
                'canonical_hash'    => base64_encode('acsolution1234'),
                'first_name'  => 'AC',
                'last_name'   => 'Solution',
                'activated'   => 1,
                'user_type'   => 1,
                'permissions' => array(
                    'admin'   => 1
                )
            ));
            $adminACS->addGroup( Sentry::getGroupProvider()->findByName('Administrators') );
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }	
    }
    
    private function seedCashier() {
    	try {
            Sentry::getGroupProvider()->create(array(
                'name'        => 'Cashier',
                'permissions' => array(
                    'cashier' => 1,
                ),
            ));
            
            $cashier = Sentry::createUser(array(
                'username'    => 'cashier1',
                'email'       => 'cashier1@jca.org',
                'password'    => 'password',
                'canonical_hash'    => base64_encode('password'),
                'first_name'  => 'Fname',
                'last_name'   => 'Lname',
                'activated'   => 1,
                'user_type'   => 2,
                'permissions' => array(
                    'cashier'   => 1
                )
            ));
            $cashier->addGroup( Sentry::getGroupProvider()->findByName('Cashier') );
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }	
    }
    
    private function seedMember() {
    	try {
            Sentry::getGroupProvider()->create(array(
                'name'        => 'Member',
                'permissions' => array(
                    'member' => 1,
                ),
            ));
            
            $member = Sentry::createUser(array(
                'username'    => 'HEAD',
                'email'       => 'HEAD@jca.org',
                'password'    => 'password12345678910',
                'canonical_hash'    => base64_encode('password12345678910'),
                'first_name'  => 'Company',
                'last_name'   => 'Head',
                'activated'   => 1,
                'user_type'   => 3,
                'permissions' => array(
                    'member'   => 1
                )
            ));
            $member->addGroup( Sentry::getGroupProvider()->findByName('Member') );
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }	
    }
}

$migrator = new Migrator($config['database']['connections'][$config['database']['default']]);

// $migrator->migrate();
$migrator->seed();
?>