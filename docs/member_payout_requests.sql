-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 16, 2017 at 02:36 AM
-- Server version: 5.7.12-1~exp1+deb.sury.org~trusty+1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jca`
--

-- --------------------------------------------------------

--
-- Table structure for table `member_payout_requests`
--

CREATE TABLE `member_payout_requests` (
  `id` int(11) NOT NULL,
  `transaction_number` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `amount_encash` decimal(10,2) DEFAULT '0.00',
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `status` tinyint(1) DEFAULT '0',
  `approved_by` int(11) DEFAULT NULL,
  `week_range` varchar(500) DEFAULT NULL,
  `is_pm_deducted` tinyint(1) DEFAULT '0',
  `payout_type` tinyint(1) DEFAULT '0',
  `check_issued` tinyint(1) DEFAULT '0',
  `check_no` varchar(250) DEFAULT NULL,
  `check_date` date DEFAULT NULL,
  `bank_name` varchar(500) DEFAULT NULL,
  `bank_account_no` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_payout_requests`
--

INSERT INTO `member_payout_requests` (`id`, `transaction_number`, `user_id`, `amount_encash`, `total_amount`, `status`, `approved_by`, `week_range`, `is_pm_deducted`, `payout_type`, `check_issued`, `check_no`, `check_date`, `bank_name`, `bank_account_no`, `created_at`, `updated_at`) VALUES
(1, 'U9aJGuMPqg9i', 5, '2550.00', '4500.00', 1, 1, '2017-03-13 | 2017-03-19', 1, 1, 0, NULL, NULL, NULL, NULL, '2017-04-15 17:59:23', '2017-04-15 17:59:23'),
(3, 'RGk68XhFl3TF', 5, '3150.00', '3500.00', 1, 1, '2017-03-13 | 2017-03-19', 0, 2, 0, NULL, NULL, NULL, NULL, '2017-04-15 17:59:15', '2017-04-15 17:59:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
