-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2017 at 05:57 PM
-- Server version: 5.7.8-rc
-- PHP Version: 5.6.23-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jca`
--

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE IF NOT EXISTS `receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_code` varchar(250) DEFAULT NULL,
  `or_num` varchar(250) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `module_type` varchar(250) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `created_by` int(11) DEFAULT NULL,
  `issued_to` int(11) DEFAULT NULL,
  `is_issued` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `receipts`
--

INSERT INTO `receipts` (`id`, `trans_code`, `or_num`, `ref_id`, `module_type`, `total_amount`, `created_by`, `issued_to`, `is_issued`, `created_at`, `updated_at`) VALUES
(1, '1F1S-2Q6V-H1MM', '4321', 7, 'payin', 8000.00, 1, 11, 1, '2017-04-12 06:22:53', '2017-04-12 06:22:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
